image: docker:latest

services:
  - docker:dind

variables:
  DOCKER_HOST: tcp://localhost:2375



build:
  stage: build
  variables:
    RUN_TEST: "0"
  script:
    - if [ $CI_COMMIT_REF_SLUG == "master" ]; then export RUN_TEST="1"; fi
#    - export
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    # pull the latest build on master
    - docker pull "$CI_REGISTRY_IMAGE:latest" || true
    # pull the latest build on this branch
    - docker pull "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" || true
    # build the image while passing commit SHA and tagging the image with it
    - docker build
      --build-arg CI_COMMIT_REF_SLUG
      --build-arg CI_COMMIT_SHA
      --build-arg CI_COMMIT_SHORT_SHA
      --cache-from "$CI_REGISTRY_IMAGE:latest"
      --cache-from "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
      --tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
      --tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
      --tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
      ./src/viralhostrange/
    # Test if the build image is passing all the tests
    - mkdir persistent_volume && chmod 777 persistent_volume
    - if [ $RUN_TEST == "1" ]; then docker run -v $(pwd)/persistent_volume:/code/persistent_volume -e "USE_SQLITE_AS_DB=True" "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" test; fi
    - if [ $RUN_TEST == "1" ]; then mv persistent_volume/htmlcov ./htmlcov; fi
    # Tests have been passed, so pushing
    # if we are on master branch, tag the build with latest and push it
    - if [ $CI_COMMIT_REF_SLUG == "master" ]; then docker tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA" "$CI_REGISTRY_IMAGE:latest"; docker push "$CI_REGISTRY_IMAGE:latest"; fi
    # push with the tag of the branch name
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
      # push with the tag of the commit sha
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
      # push with the tag of the commit short sha
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
  tags:
    - k8s
  artifacts:
    paths:
    - htmlcov
    expire_in: 1 week



deploy_prod:
  only:
    - master
  stage: deploy
  image: registry-gitlab.pasteur.fr/dsi-tools/docker-images:docker_kubernetes_image
  variables:
    CI_DEBUG_TRACE: "false"
    NAMESPACE: "viralhostrange-prod"
    PUBLIC_URL: "viralhostrangedb.pasteur.cloud"
    STORAGE_SUFFIX: "-${CI_COMMIT_REF_SLUG}"
    DEBUG: "True"
    DISPOSABLE: "Never"
  environment:
    name: "viralhostrange-prod/$CI_COMMIT_REF_SLUG"
    url: "https://viralhostrangedb.pasteur.cloud"
  script:
    - echo $NAMESPACE
    - echo $CI_COMMIT_REF_SLUG
    - echo $STORAGE_SUFFIX
    - kubectl get pods -n $NAMESPACE
    - yum install gettext -y
    - kubectl delete secret registry-gitlab -n $NAMESPACE --ignore-not-found=true
    - kubectl create secret docker-registry -n $NAMESPACE registry-gitlab --docker-server=$CI_REGISTRY --docker-username=$DEPLOY_USER --docker-password=$DEPLOY_TOKEN --docker-email=$GITLAB_USER_EMAIL
    - envsubst < k8s/config-map.yaml | kubectl apply -n $NAMESPACE -f -
    - envsubst < k8s/kubernetes-storage.yaml | kubectl apply -n $NAMESPACE -f -
    - envsubst < k8s/manifest-postgres.yaml | kubectl apply -n $NAMESPACE -f -
    - envsubst < k8s/manifest.yaml | kubectl apply -n $NAMESPACE -f -
    # - envsubst < k8s/manifest-webhost-autoscale.yaml | kubectl apply -n $NAMESPACE -f -
    - envsubst < k8s/kubernetes-cronjob.yaml | kubectl apply -n $NAMESPACE -f -
  tags:
    - k8s



deploy_dev:
  except:
    - master
  stage: deploy
  image: registry-gitlab.pasteur.fr/dsi-tools/docker-images:docker_kubernetes_image
  variables:
    CI_DEBUG_TRACE: "false"
    NAMESPACE: "viralhostrange-dev"
    PUBLIC_URL: "viralhostrangedb-${CI_COMMIT_REF_SLUG}.pasteur.cloud"
    STORAGE_SUFFIX: "-${CI_COMMIT_REF_SLUG}"
    DEBUG: "True"
    DISPOSABLE: "Always"
  environment:
    name: "viralhostrange-dev/$CI_COMMIT_REF_SLUG"
    url: "https://viralhostrangedb-${CI_COMMIT_REF_SLUG}.pasteur.cloud"
    on_stop: stop_and_delete_in_dev
  script:
    - echo $NAMESPACE
    - echo $CI_COMMIT_REF_SLUG
    - echo $STORAGE_SUFFIX
    - kubectl get pods -n $NAMESPACE
    - yum install gettext -y
    - kubectl delete secret registry-gitlab -n $NAMESPACE --ignore-not-found=true
    - kubectl create secret docker-registry -n $NAMESPACE registry-gitlab --docker-server=$CI_REGISTRY --docker-username=$DEPLOY_USER --docker-password=$DEPLOY_TOKEN --docker-email=$GITLAB_USER_EMAIL
    - . ./k8s/init_db_from_prod.sh
    - envsubst < k8s/config-map.yaml | kubectl apply -n $NAMESPACE -f -
    - envsubst < k8s/kubernetes-storage.yaml | kubectl apply -n $NAMESPACE -f -
    - envsubst < k8s/manifest-postgres.yaml | kubectl apply -n $NAMESPACE -f -
    - envsubst < k8s/manifest.yaml | kubectl apply -n $NAMESPACE -f -
    # - envsubst < k8s/manifest-webhost-autoscale.yaml | kubectl apply -n $NAMESPACE -f -
    - envsubst < k8s/kubernetes-cronjob.yaml | kubectl apply -n $NAMESPACE -f -
  tags:
    - k8s



stop_and_delete_in_dev:
  except:
    - master
  stage: deploy
  when: manual
  image: registry-gitlab.pasteur.fr/dsi-tools/docker-images:docker_kubernetes_image
  variables:
    GIT_STRATEGY: none # important to not checkout source when branch is deleted
    NAMESPACE: "viralhostrange-dev"
  environment:
    name: "viralhostrange-dev/$CI_COMMIT_REF_SLUG"
    action: stop
  script:
    - echo "Removing $CI_COMMIT_REF_SLUG"
    - kubectl --namespace=$NAMESPACE --wait=true delete hpa -l branch=branch-$CI_COMMIT_REF_SLUG
    - kubectl --namespace=$NAMESPACE --wait=true delete deploy,replicasets -l branch=branch-$CI_COMMIT_REF_SLUG -l role=front
    - kubectl --namespace=$NAMESPACE --wait=true delete deploy,replicasets -l branch=branch-$CI_COMMIT_REF_SLUG
    - kubectl --namespace=$NAMESPACE --wait=true delete cronjob -l branch=branch-$CI_COMMIT_REF_SLUG
    - kubectl --namespace=$NAMESPACE --wait=true delete ing,svc -l branch=branch-$CI_COMMIT_REF_SLUG
    - kubectl --namespace=$NAMESPACE --wait=true delete po -l branch=branch-$CI_COMMIT_REF_SLUG
    - kubectl --namespace=$NAMESPACE --wait=true delete pvc -l branch=branch-$CI_COMMIT_REF_SLUG
    - kubectl --namespace=$NAMESPACE --wait=true delete secrets -l branch=branch-$CI_COMMIT_REF_SLUG
    - kubectl --namespace=$NAMESPACE --wait=true delete configmaps -l branch=branch-$CI_COMMIT_REF_SLUG
  tags:
    - k8s


  
pages:
  image: python:3.5
  stage: deploy
  script:
  - pip install -r doc/requirements.txt
  - cd doc
  - make html
  - mv _build/html/ ../public
  artifacts:
    paths:
    - public
  only:
  - master

