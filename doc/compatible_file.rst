What is a compatible file?
==========================


.. contents:: Table of Contents
   :depth: 3

TL;DR
-----
It as a 2D array in an xlsx spreadsheet where columns are hosts, and rows are viruses.
Each cell is the response of the infection of the host of the current column by the virus of the current row, the response is a float value.


In Long
-------
Here is a basic example, you can also download its `xlsx version <https://gitlab.pasteur.fr/hub/12765-viralhostrangedb/blob/master/src/viralhostrange/viralhostrangedb/static/media/example.xlsx>`_.


+-------------------+-------------------------------+------------------------------+
|                   | E. coli MG1655 (NC_000913.3)  | E. coli O157:H7 (AE005174.2) |
+-------------------+-------------------------------+------------------------------+
| T4 (NC_000866.4)  | 2                             | 0                            |
+-------------------+-------------------------------+------------------------------+
| T7 (NC_001604.1)  | 1                             | 2                            |
+-------------------+-------------------------------+------------------------------+

Values of the responses
~~~~~~~~~~~~~~~~~~~~~~~
Each experiment can have its own way of measuring if there was an infection or not, and sometime that the level of infection is intermediate. In the file you can use whatever values to define the level of infection, the only constraints is that **responses are float**. Though we also recommend that the responses are ordinated i.e: if 0 is *no infection* and 4 is *infection*, the response for *not conclusive infection*  should not be 6.

Providing the identifier of a virus or a host
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The virus/host name is the whole heading cell. If available, you can add NCBI identifiers between parenthesis after the name of the host or of the virus. You can also add them after importing the table on the data source page by editing hosts/viruses.

Example :
    ``T4 (NC_000866.4)`` stands for a virus named ``T4`` where the NCBI identifier is ``NC_000866.4``. In the app, link to the NCBI will be provided such as https://www.ncbi.nlm.nih.gov/nuccore/NC_000866.4


How the data of a file can be compared to the other data sources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Once your file imported, you will be asked to map it to a global three-state scheme (*no infection*, *intermediate [infection]*, *infection*). Thanks to this mapping, data sources using a three-state scheme can be compared to continuous scale data sources.


Color in the file
~~~~~~~~~~~~~~~~~
Cell color in the file are not taken into account, so fell free to use whatever color scheme you want to help read your raw files. Note that also use colors in exported file when possible, but again it is only informative.


Variants in the file disposition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1. The header of a row (resp. column) in the spreadsheet can be preceded by one or many columns (resp. rows). In the following example, only the cell in bold will be interpreted,

+-------+-----------------------+------------------------+------------------------+
|       |                       | Obtained from John doe | Obtained from Jane doe |
+-------+-----------------------+------------------------+------------------------+
|       |                       | On 2009-02-21          | On 2012-03-21          |
+-------+-----------------------+------------------------+------------------------+
|       |                       | **E. coli MG1655**     | **E. coli O157:H7**    |
+-------+-----------------------+------------------------+------------------------+
| Virus | **T4 (NC_000866.4)**  | **2**                  | **0**                  |
+-------+-----------------------+------------------------+------------------------+
| Virus | **T7 (NC_001604.1)**  | **1**                  | **2**                  |
+-------+-----------------------+------------------------+------------------------+

2. The data must be on the first sheet of the file.

3. There can be additional rows after the responses, they will not be imported as long as there is nothing written where the virus name is expected.

+-----------------+-----------------------+--------------------+---------------------+
|                 |                       | **E. coli MG1655** | **E. coli O157:H7** |
+-----------------+-----------------------+--------------------+---------------------+
| Virus           | **T4 (NC_000866.4)**  | **2**              | **0**               |
+-----------------+-----------------------+--------------------+---------------------+
| Virus           | **T7 (NC_001604.1)**  | **1**              | **2**               |
+-----------------+-----------------------+--------------------+---------------------+
| Infection Ratio |  *<Must be blank>*    | 100%               | 50%                 |
+-----------------+-----------------------+--------------------+---------------------+

Robustness of file import
~~~~~~~~~~~~~~~~~~~~~~~~~
The resilience of the importation module to read and interpret the file is of a paramount importance, we generated multiple configuration in which a file could be written and how we should read it. At each change in the programme we test that each file is still read as expected. The file collection can be browsed at https://gitlab.pasteur.fr/hub/12765-viralhostrangedb/tree/master/src/viralhostrange/test_data, where for an input file ``<filename>.xlsx`` the data we extract from it is ``<filename>.xlsx.json``.

If you tried to import a file which should work but did not, please to not hesitate to submit an issue at https://gitlab.pasteur.fr/hub/12765-viralhostrangedb/issues with the file cleaned of its private data. You can also e-mail the file and steps to reproduce the bug at viralhostrangedb@pasteur.fr. We will either correct the importation module so that the file can now be imported, or return a error message clear enough so users understand what is wrong with their file.
