Importing data into Viral Host Range database
=============================================


.. contents:: Table of Contents
   :depth: 3

Mapping responses to the global scheme.
------------------------------------------
Mapping the original responses of a data source to a global scheme allows to compare data source which have differente standard in recording the response of the infection of an host by a virus. For example one can use a two-state scheme (0/1), while the other one would use a continous scale from 0 to 4 (as in the following example).

Example
~~~~~~~

.. image:: _static/mapping-float.png
  :width: 100%
  :alt: Mapping example with float responses

In this example, original responses goes from 0 to 4. The global scheme has three responses `No infection`,  `Intermediate`, and  `Infection`. All responses of a data source have to be associated to one and only one of the global responses. To distribute the original responses, we propose to set thresholds to :math:`0.58` and  \\(2.3\\). The thresholds do not need to have a scientific meaning, they are juste values that help dispatching the original responses to one of the state of the global scheme. In the example, setting the first threshold to :math:`0.58` or :math:`0.9` would results in the same mapping: \\(0\\), \\(0.5\\) and \\(0.1\\) are associated to `No infection` while \\(1\\) and \\(1.2\\) are associated with `Intermediate`.

Implementation details
~~~~~~~~~~~~~~~~~~~~~~
To compute the thresholds, we run a clustering algorithm on the original responses of the data source with three clusters :math:`c_1`, :math:`c_2` and :math:`c_3`, then the threshold between :math:`c_k` and :math:`c_{k+1}` is the mean of the centroide of the these two clusters. Implementation can be seen `here <https://gitlab.pasteur.fr/hub/12765-viralhostrangedb/blob/master/src/viralhostrange/viralhostrangedb/forms.py#L252-261>`_.



Citation example
----------------

See :cite:`Strunk1979` for an introduction to stylish blah, blah...

.. bibliography:: references.bib

