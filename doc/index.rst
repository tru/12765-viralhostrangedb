.. Viral Host Range database documentation master file, created by
   sphinx-quickstart on Mon Oct 14 09:01:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

VHRdb documentation
===================

If you found a bug, do not hesitate to submit an issue in the `issue tracking system <https://gitlab.pasteur.fr/hub/12765-viralhostrangedb/issues>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial
   data_import
   compatible_file

Quick Start
===========

