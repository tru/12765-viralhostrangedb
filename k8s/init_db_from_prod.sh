#!/bin/bash

echo ${STORAGE_SUFFIX}
echo $NAMESPACE

export LOCAL_RUN=""
#export LOCAL_RUN=$(echo -n "--token=$(cat token) --server https://157.99.20.111:6443 --certificate-authority k8s.crt"); export NAMESPACE="viralhostrange-dev"; export STORAGE_SUFFIX="-init-db-2"

export need_init=0

kubectl $LOCAL_RUN --namespace=$NAMESPACE get pvc -l branch=branch${STORAGE_SUFFIX} --output jsonpath='{.items[0]}' || export need_init=1

echo $need_init

if [ "$need_init" == "1" ]; then
  # start the pvc
  envsubst < k8s/kubernetes-storage.yaml | kubectl $LOCAL_RUN --wait=true apply -n $NAMESPACE -f - || exit 3
  # start the sidekick which mount the pvc
  envsubst < k8s/manifest-sidekick-postgres.yaml | kubectl $LOCAL_RUN --wait=true apply -n $NAMESPACE -f - || exit 4

  # get the name of the production db
  prod_db=$(kubectl $LOCAL_RUN  --namespace=viralhostrange-prod get po -l branch=branch-master -l app=postgres-app --output jsonpath='{.items[0].metadata.name}') || exit 5

  # Copy prod db on local runner
  kubectl $LOCAL_RUN --namespace=viralhostrange-prod cp ${prod_db}:/var/lib/postgresql/data/ postgresql-moving/ || exit 6

  du -h postgresql-moving/

  # waiting after the sidekick, as $(...) are not working in the CI (don't know why) I use this ugly way:
  kubectl $LOCAL_RUN --namespace=$NAMESPACE exec sidekick-db${STORAGE_SUFFIX} -- sh -c 'ls' || sleep 3
  kubectl $LOCAL_RUN --namespace=$NAMESPACE exec sidekick-db${STORAGE_SUFFIX} -- sh -c 'ls' || sleep 3
  kubectl $LOCAL_RUN --namespace=$NAMESPACE exec sidekick-db${STORAGE_SUFFIX} -- sh -c 'ls' || sleep 3
  kubectl $LOCAL_RUN --namespace=$NAMESPACE exec sidekick-db${STORAGE_SUFFIX} -- sh -c 'ls' || sleep 3
  kubectl $LOCAL_RUN --namespace=$NAMESPACE exec sidekick-db${STORAGE_SUFFIX} -- sh -c 'ls' || sleep 3
  kubectl $LOCAL_RUN --namespace=$NAMESPACE exec sidekick-db${STORAGE_SUFFIX} -- sh -c 'ls' || sleep 3
  kubectl $LOCAL_RUN --namespace=$NAMESPACE exec sidekick-db${STORAGE_SUFFIX} -- sh -c 'ls' || sleep 3

  # Copy from runner to the sidekick
  kubectl $LOCAL_RUN --namespace=$NAMESPACE cp postgresql-moving/ sidekick-db${STORAGE_SUFFIX}:/data || exit 7

  # fixe permission and localisation
  kubectl $LOCAL_RUN --namespace=$NAMESPACE exec sidekick-db${STORAGE_SUFFIX} -- sh -c 'cd /data; ls -lahn;mv postgresql-moving/* .;rmdir postgresql-moving; chown 999:0 -R .; ls -lahn' || exit 8

  # stop the sidekick gently
  envsubst < k8s/manifest-sidekick-postgres.yaml | kubectl delete --wait=true -n $NAMESPACE -f - || exit 9
fi
