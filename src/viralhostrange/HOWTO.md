```
docker build . -t test

docker run -v $(pwd)/persistent_volume:/code/persistent_volume -e "USE_SQLITE_AS_DB=True" -e "ALLOWED_HOSTS=localhost" -e "SECRET_KEY=*" -e "DEBUG=True" -e "STATIC_URL=/static" -e "PROJECT_NAME=viralhostrange" -p 8086:8086 test
```