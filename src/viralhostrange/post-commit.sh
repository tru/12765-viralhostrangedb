#!/usr/bin/env bash

echo "Version $(git describe --always) on $(git show -s --format=%ci)" > ./src/viralhostrange/viralhostrangedb/templates/viralhostrangedb/last_update.html