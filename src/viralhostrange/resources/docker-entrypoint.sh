#!/bin/bash

cd /code

source resources/tool_shed.sh

if [ "$1" == "test" ]; then
    msg_info "Running tests"
    pip install coverage
    cp viralhostrange/settings.example.ini viralhostrange/settings.ini || exit 2

    MEDIA_ROOT_DIR=$(python manage.py shell -c "from django.conf import settings; print(settings.MEDIA_ROOT)" | grep -v django.db.backends)
    mkdir -p $MEDIA_ROOT_DIR
    chmod 777 $MEDIA_ROOT_DIR

    python manage.py collectstatic --noinput
    python manage.py compilemessages || exit 6
    coverage run --source='.' manage.py test || exit 3
    coverage report --skip-covered
    coverage html -d persistent_volume/htmlcov
    exit 0
fi

if [ "$1" == "test_with_upgrade" ]; then
    msg_info "Running tests with latest dependencies"
    pip install --user --upgrade pip
    grep -v @ requirements.txt | grep -ve "^#" | cut -d= -f1 | cut -d'<' -f1 | cut -d'>' -f1 | xargs -n 1 pip install --user -U || exit 4
    pip install coverage
    cp viralhostrange/settings.example.ini viralhostrange/settings.ini || exit 2
    coverage run --source='.' manage.py test || exit 3
    coverage report
    exit 0
fi

if [ "$1" == "hold_on" ]; then
    msg_info "holding on util you delete /tmp/hold_on"
    touch /tmp/hold_on
    while [ -e "/tmp/hold_on" ]; do
        sleep 1 ;
        echo "holding on" ;
    done
fi

if [ ! -e viralhostrange/settings.ini ] ; then
    if [ "$ALLOWED_HOSTS" == ""  ] ; then
        msg_error "Missing settings.ini and \$ALLOWED_HOSTS"
        exit 1
    fi
    if [ "$SECRET_KEY" == ""  ] ; then
        msg_error "Missing settings.ini and \$SECRET_KEY"
        exit 1
    fi
fi

msg_info "Applying database migrations"
python manage.py migrate || exit 4

msg_info "Compilling localization (.po -> .mo)"
python manage.py compilemessages

msg_info "Getting static root"
STATIC_ROOT_DIR=$(python manage.py shell -c "from django.conf import settings; print(settings.STATIC_ROOT)")
msg_info "Creating static root at $STATIC_ROOT_DIR"
if [ "$STATIC_ROOT_DIR" != "" ]; then
    mkdir -p $STATIC_ROOT_DIR
    chmod 777 $STATIC_ROOT_DIR
else
    msg_warning "settings.STATIC_ROOT missing, passed"
fi

msg_info "Collecting static files"
python manage.py collectstatic --noinput

msg_info "Compressing css files"
if [ "$(pip freeze | grep csscompressor | wc -l )" == "1" ]; then
    for css_file in $(find $(python manage.py shell -c "from django.conf import settings; print(settings.STATIC_ROOT)") -name \*.css -print | grep -v .min.css); do
        ls -laH $css_file
        d=$(date -R -r $css_file)
        python -m csscompressor $css_file -o $css_file;
        touch -d "$d" $css_file
    done
else
    msg_warning "csscompressor missing, passed"
fi

MEDIA_ROOT_DIR=$(python manage.py shell -c "from django.conf import settings; print(settings.MEDIA_ROOT)" | grep -v django.db.backends)
msg_info "Creating media root at $MEDIA_ROOT_DIR"
if [ "$MEDIA_ROOT_DIR" != "" ]; then
    mkdir -p $MEDIA_ROOT_DIR
    chmod 777 $MEDIA_ROOT_DIR
else
    msg_warning "settings.MEDIA_ROOT missing, passed"
fi

sudo /usr/sbin/service cron start
msg_info  "Adding cron task to dump db every day"
cat <(crontab -l) <(echo "") <(echo "* * * * * find $MEDIA_ROOT_DIR/ -type f -name 'Template*.xlsx' -mtime +1 -exec rm {} \; >> /code/persistent_volume/django-crontab.log 2>> /code/persistent_volume/django-crontab.err") | crontab -

# Setting up cron tasks
msg_info "Registering cron tasks"
if [ "$(pip freeze | grep django-crontab | wc -l )" == "1" ]; then
    python manage.py crontab add
    #sudo /usr/sbin/service cron start
    grep = /code/resources/default.ini /code/resources/local.ini -h | sed "s/^\(.*\)$/export \1/g" > /home/kiwi/env.sh
    echo "export ALLOWED_HOSTS=$ALLOWED_HOSTS" >> /home/kiwi/env.sh
    echo "export DEBUG=$DEBUG" >> /home/kiwi/env.sh
    echo "export SECRET_KEY=NOT_NEEDED" >> /home/kiwi/env.sh
else
    msg_warning "django-crontab missing, passed"
fi

crontab -l

if [ "$1" == "do_not_start" ]; then
    msg_info "Entrypoint have been run successfully, we are not starting the project as requested"
elif [ "$1" == "django" ]; then
    exec python manage.py runserver 0.0.0.0:8086
elif [ "$1" == "gunicorn" ]; then
    if [ "$(pip freeze | grep gunicorn | wc -l )" == "0" ]; then
        msg_error "gunicorn not installed, we will install it but you should add it to your requirements.txt, it will be installed once for all during the build"
        pip install gunicorn --no-cache-dir
    fi
    exec gunicorn bdchem.wsgi -b 0.0.0.0:443 --certfile certs/$CERT_NAME.crt --keyfile=certs/$CERT_NAME.key
elif [ "$1" == "makemessages" ]; then
    exec python manage.py makemessages -l en --no-location
else
    rm -f /var/run/apache2/apache2.pid
    rm -rf /run/httpd/* /tmp/httpd*
    chmod -R 777 /tmp/* 2>/dev/null
    echo "ServerName ${ALLOWED_HOSTS}" >> /etc/apache2/apache2.conf
    sudo chmod a-w /etc/apache2/apache2.conf
    exec "$@"
fi

# Start server
#echo "Starting server"
#python manage.py runserver 0.0.0.0:8001
