from basetheme_bootstrap.admin import ViewOnSiteModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.auth import get_permission_codename
from django.utils.translation import ugettext_lazy as _

from viralhostrangedb import models, business_process


def reset_mapping(modeladmin, request, queryset):
    business_process.reset_mapping(queryset)


def purge_without_data_source(modeladmin, request, queryset):
    business_process.purge_without_data_source(queryset)


@admin.register(models.DataSource)
class DataSourceAdmin(ViewOnSiteModelAdmin):
    list_display = (
        'name',
        'public',
        'owner',
        'has_publication',
        'creation_date',
        'last_edition_date',
        'is_mapping_done',
    )
    list_display_links = (
        'name',
    )
    list_filter = (
        'public',
        'owner',
        'life_domain',
    )
    filter_horizontal = (
        'allowed_users',
    )
    date_hierarchy = 'creation_date'
    actions = [
        reset_mapping,
    ]

    def has_publication(self, obj):
        return obj.publication_url is not None and len(obj.publication_url) > 0

    has_publication.boolean = True

    def is_mapping_done(self, obj):
        return obj.is_mapping_done

    is_mapping_done.boolean = True


@admin.register(models.Virus, models.Host)
class VirusHostAdmin(ViewOnSiteModelAdmin):
    search_fields = ('name', 'identifier',)
    list_display = (
        'name',
        'identifier',
    )
    list_filter = (
        'data_source',
    )
    filter_horizontal = (
        'data_source',
    )
    actions = [
        purge_without_data_source,
    ]


@admin.register(models.ViralHostResponseValueInDataSource)
class ViralHostResponseValueInDataSourceAdmin(ViewOnSiteModelAdmin):
    list_display = (
        'raw_response',
        'virus_name',
        'host_name',
        'response',
    )
    list_filter = (
        'response',
        'data_source',
    )
    readonly_fields = (
        'data_source',
        'virus',
        'host',
    )

    def virus_name(self, o):
        return o.virus.explicit_name

    def host_name(self, o):
        return o.host.explicit_name


@admin.register(models.GlobalViralHostResponseValue)
class GlobalViralHostResponseValueAdmin(ViewOnSiteModelAdmin):
    list_display = (
        'name',
        'value',
        'color',
    )

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = self.readonly_fields
        if not request.user.has_perm("%s.%s" % (self.opts.app_label, get_permission_codename('change', self.opts))):
            readonly_fields += ('name', 'value')
        return readonly_fields

    def has_change_permission(self, request, obj=None):
        if super().has_change_permission(request=request, obj=obj):
            return True
        opts = self.opts
        codename = get_permission_codename('change', opts) + "_color"
        return request.user.has_perm("%s.%s" % (opts.app_label, codename))


for model in apps.get_app_config('viralhostrangedb').models.values():
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered as are:
        continue

admin.site.site_header = _('ViralHostRangeDB website administration')

admin.site.site_title = admin.site.site_header

admin.site.index_header = _('ViralHostRangeDB administration')

admin.site.index_title = admin.site.index_header
