from django.apps import AppConfig


class ViralhostrangedbConfig(AppConfig):
    name = 'viralhostrangedb'
