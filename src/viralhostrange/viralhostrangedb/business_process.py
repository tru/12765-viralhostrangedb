import csv
import re
from collections import namedtuple
from sqlite3 import DataError as sqlite3_DataError

import pandas as pd
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils import timezone
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext
from psycopg2._psycopg import DataError as psycopg2_DataError

from viralhostrangedb import models

ViralHostResponse = namedtuple(
    "ViralHostResponse",
    [
        "virus",
        "virus_identifier",
        "host",
        "host_identifier",
        "response",
        "row_id",
        "col_id",
    ])

NameAndIdentifierRegexp = re.compile("([^(]*)([(]([^(]+)[)])?")


class ImportationObserver:
    class Meta:
        abstract = True

    def notify_response_error(self, virus, host, response_str, replaced):
        pass

    def notify_host_error(self, host, column_id, reason=None):
        pass

    def notify_virus_error(self, virus, row_id, reason=None):
        pass

    @staticmethod
    def id_to_excel_index(n):
        # https://stackoverflow.com/a/23862195/2144569
        string = ""
        while n > 0:
            n, remainder = divmod(n - 1, 26)
            string = chr(65 + remainder) + string
        return string


class MessageImportationObserver(ImportationObserver):

    def __init__(self, request):
        self.request = request
        self.host_warned = set()
        self.virus_warned = set()

    def notify_response_error(self, virus, host, response_str, replaced):
        messages.add_message(
            self.request,
            messages.WARNING,
            "[ImportErr2] " + ugettext(
                "Could not import response \"%(response)s\" for virus \"%(virus)s\", host\"%(host)s\", "
                "replacing it with \"%(rpl)s\"") % dict(
                response=str(response_str),
                virus=str(virus),
                host=str(host),
                rpl=str(replaced),
            )
        )

    @staticmethod
    def reason_to_str(msg, reason):
        msg = [msg, "<br/>"]
        if type(reason) == ValidationError:
            reason = reason.error_dict
        if type(reason) == dict:
            msg.append("<ul>")
            for k, v in reason.items():
                msg.append("<li><b>")
                msg.append(escape(k))
                msg.append("</b>: ")
                msg.append(escape(str(v)))
                msg.append("</li>")
            msg.append("</ul>")
        else:
            msg.append(str(reason))
        return "".join(msg)

    def notify_host_error(self, host, column_id, reason=None):
        if column_id in self.host_warned:
            return
        self.host_warned.add(column_id)
        msg = "[ImportErr1] " + ugettext("Could not parse host \"%(host)s\" at column \"%(column_id)s\" "
                                         "(i.e column \"%(column_index)s\")") % dict(
            host=str(host),
            column_id=str(column_id),
            column_index=str(self.id_to_excel_index(column_id)),
        )
        if reason:
            msg = self.reason_to_str(msg, reason)
        messages.add_message(
            self.request,
            messages.WARNING,
            mark_safe(msg),
        )

    def notify_virus_error(self, virus, row_id, reason=None):
        if row_id in self.virus_warned:
            return
        self.virus_warned.add(row_id)
        msg = "[ImportErr3] " + ugettext("Could not parse virus \"%(virus)s\" at row \"%(row_id)s\"") % dict(
            virus=str(virus),
            row_id=str(row_id),
        )
        if reason:
            msg = self.reason_to_str(msg, reason)
        messages.add_message(
            self.request,
            messages.WARNING,
            mark_safe(msg),
        )


def parse_file(file, importation_observer: ImportationObserver = None):
    content = pd.read_excel(file, index_col=0, header=0)
    my_reader = csv.reader(content.to_csv(sep=';').split('\n'), delimiter=';')
    header = None
    start_at = 0
    has_seen_data = False
    row_id = 0
    for row in my_reader:
        row_id += 1
        sub_row = row[start_at:]
        virus = None
        virus_identifier = None
        for id_col, cell in enumerate(sub_row):
            if header is None or not has_seen_data and id_col == 1 and sub_row[0] == "":
                if cell is not "" and not cell.startswith("Unnamed: "):
                    header = row
                    start_at = id_col - 1 + start_at
                    break
            elif id_col > 0 and sub_row[0] != "":
                cell = cell.strip()
                if cell == "":
                    continue
                if not virus:
                    virus, virus_identifier = extract_name_and_identifier(sub_row[0])
                has_seen_data = True
                h = header[id_col + start_at]
                if h == "" or h.startswith("Unnamed: "):
                    if importation_observer:
                        importation_observer.notify_host_error(h, id_col + start_at)
                    continue
                host, host_identifier = extract_name_and_identifier(h)
                yield ViralHostResponse(
                    virus=virus,
                    virus_identifier=virus_identifier,
                    host=host,
                    host_identifier=host_identifier,
                    response=cell,
                    row_id=row_id,
                    col_id=id_col + start_at,
                )


def extract_name_and_identifier(text):
    match = NameAndIdentifierRegexp.search(text)
    name = match.groups()[0].strip()
    identifier = match.groups()[-1]
    if identifier:
        identifier = identifier.strip()
    else:
        identifier = ''
    return name, identifier


def to_dict(viral_host_responses, transposed=False):
    data_as_dict = {}
    for vhr in viral_host_responses:
        host = explicit_item(vhr.host, vhr.host_identifier)
        virus = explicit_item(vhr.virus, vhr.virus_identifier)
        host_dict = data_as_dict.setdefault(virus if transposed else host, dict())
        host_dict[host if transposed else virus] = vhr.response
    return data_as_dict


def explicit_item(name, identifier):
    return "%s (%s)" % (name, identifier) if identifier else name


@transaction.atomic
def import_file(*, data_source, file, importation_observer: ImportationObserver = None):
    """
    Import the file and associate responses to the data source provided. If responses are already present there are
    overwritten with the one from the file. Updated response are automatically map following the mapping observed in db

    :param data_source: data source that will be updated/enriched (can be a fresh data source)
    :param file: file or path to the file to import for the passed data source
    :param importation_observer: an observer allowing to notify of problem during the importation without crashing
    :return: the data source, updated with the response gathered from the file
    """
    not_mapped_yet = models.GlobalViralHostResponseValue.get_not_mapped_yet()
    # virus already associated with this data source
    old_virus_pk = set(data_source.virus_set.values_list('pk', flat=True))
    # host already associated with this data source
    old_host_pk = set(data_source.host_set.values_list('pk', flat=True))
    # cached virus
    virus_dict = dict()
    # cached host
    host_dict = dict()
    # response to bulk create
    responses_to_create = []
    # former mapping, if present empty dict otherwise
    former_mapping = dict(data_source.get_mapping(only_pk=True))
    for vhr in parse_file(file, importation_observer):
        # if vhr.virus == "" or vhr.host == "" or vhr.response == "":
        #     continue
        explicit_virus = explicit_item(vhr.virus, vhr.virus_identifier)
        try:
            # search virus in dict
            virus = virus_dict[explicit_virus]
        except KeyError:
            try:
                # search virus in db
                virus = models.Virus.objects.filter(
                    name=vhr.virus,
                    identifier=vhr.virus_identifier,
                )[0]
            except IndexError:
                # create virus
                virus = models.Virus(
                    name=vhr.virus,
                    identifier=vhr.virus_identifier,
                )
                try:
                    # check is validators are ok
                    virus.full_clean()
                    # save in db
                    virus.save()
                except (sqlite3_DataError, psycopg2_DataError, ValidationError) as e:
                    if importation_observer:
                        importation_observer.notify_virus_error(
                            str(vhr.virus),
                            vhr.row_id,
                            reason=e,
                        )
                    continue

            virus.data_source.add(data_source)
            # set virus in dict
            virus_dict[explicit_virus] = virus
        explicit_host = explicit_item(vhr.host, vhr.host_identifier)
        try:
            # search host in dict
            host = host_dict[explicit_host]
        except KeyError:
            try:
                # search host in db
                host = models.Host.objects.filter(
                    name=vhr.host,
                    identifier=vhr.host_identifier,
                )[0]
            except IndexError:
                # create host
                host = models.Host(
                    name=vhr.host,
                    identifier=vhr.host_identifier,
                )
                try:
                    # check is validators are ok
                    host.full_clean()
                    # save in db
                    host.save()
                except (sqlite3_DataError, psycopg2_DataError, ValidationError) as e:
                    if importation_observer:
                        importation_observer.notify_host_error(
                            str(vhr.host),
                            vhr.col_id,
                            reason=e,
                        )
                    continue
            host.data_source.add(data_source)
            # set host in dict
            host_dict[explicit_host] = host

        try:
            raw_response = float(vhr.response)
        except ValueError as e:
            if importation_observer:
                raw_response = -1000
                importation_observer.notify_response_error(vhr.virus, vhr.host, vhr.response, raw_response)
            else:
                raise e
        try:
            # search response in db
            response = models.ViralHostResponseValueInDataSource.objects.get(
                data_source=data_source,
                virus=virus,
                host=host,
            )

            if response.raw_response != raw_response:
                # when raw response have changed, update it an try to keep the mapping from what it was
                response.raw_response = raw_response
                response.response_id = former_mapping.get(raw_response, not_mapped_yet.pk)
                response.save()
            old_virus_pk.discard(virus.pk)
            old_host_pk.discard(host.pk)
        except models.ViralHostResponseValueInDataSource.DoesNotExist:
            # response is missing creating a new response, to save later
            responses_to_create.append(
                models.ViralHostResponseValueInDataSource(
                    data_source=data_source,
                    virus=virus,
                    host=host,
                    raw_response=raw_response,
                    response=not_mapped_yet,
                )
            )

    # save new responses
    models.ViralHostResponseValueInDataSource.objects.bulk_create(responses_to_create)

    # remove responses associated to virus that we did not see
    models.ViralHostResponseValueInDataSource.objects.filter(
        data_source=data_source,
        virus_id__in=old_virus_pk,
    ).delete()
    # de-associate virus not seen from the data source
    for o in models.Virus.objects.filter(pk__in=old_virus_pk):
        if o.data_source.count() <= 1:
            o.delete()
        else:
            o.data_source.remove(data_source)

    # remove responses associated to host that we did not see
    models.ViralHostResponseValueInDataSource.objects.filter(
        data_source=data_source,
        host_id__in=old_host_pk,
    ).delete()
    # de-associate host not seen from the data source
    for o in models.Host.objects.filter(pk__in=old_host_pk):
        if o.data_source.count() <= 1:
            o.delete()
        else:
            o.data_source.remove(data_source)

    # update lat edition time
    data_source.last_edition_date = timezone.now()
    data_source.save()
    return data_source


def reset_mapping(queryset):
    not_mapped_yet = models.GlobalViralHostResponseValue.get_not_mapped_yet()
    for o in queryset:
        models.ViralHostResponseValueInDataSource.objects.filter(data_source=o).update(response=not_mapped_yet)


def purge_without_data_source(queryset):
    for o in queryset:
        if o.data_source.count() == 0:
            o.delete()
