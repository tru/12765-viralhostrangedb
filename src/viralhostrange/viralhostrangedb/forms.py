import csv
import io
import os
import random
import string

import pandas as pd
from basetheme_bootstrap.user_preferences import get_user_preferences_for_user
from crispy_forms import helper as  crispy_forms_helper
from crispy_forms import layout as crispy_forms_layout
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Layout, Row, Column
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q, Min, Max
from django.forms import widgets
from django.utils import timezone
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ugettext

from viralhostrangedb import models, business_process
from viralhostrangedb.business_process import ImportationObserver, explicit_item
from viralhostrangedb.mixins import only_public_or_granted_or_owned_queryset_filter


class ByLastFirstNameModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.last_name.upper() + " " + obj.first_name.title()


class DateInput(forms.DateInput):
    input_type = 'date'


class ImportDataSourceForm(forms.ModelForm):
    file = forms.FileField(
        label=_('File'),
        help_text=_("The file to upload."),
        required=True,
    )
    make_upload_mandatory = True

    class Meta:
        model = models.DataSource
        fields = (
            'name',
            'description',
            'public',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.on_disk_file = None

    def full_clean(self):
        super().full_clean()
        if not self.is_bound or not self.make_upload_mandatory:  # Stop further processing.
            return
        # if (self.files is None or len(self.files) == 0) and len(self.cleaned_data["url"]) == 0 or \
        #         (self.files is not None and len(self.files) > 0) and len(self.cleaned_data["url"]) > 0:
        #     self.add_error("url", _("You have to either provide a file or an URL, and not both."))
        #     self.add_error("file", _("You have to either provide a file or an URL, and not both."))

    def save(self, owner=None, commit=True, importation_observer: ImportationObserver = None):
        instance = super().save(commit=False)
        if owner is not None:
            instance.owner = owner
        instance.save()
        file = None
        if self.files is not None and len(self.files) == 1:
            file = list(self.files.values())[0]
            if (instance.raw_name or "") == "":
                instance.raw_name = file.name
                instance.kind = "FILE"
        # if len(self.cleaned_data["url"]) > 0:
        #     try:
        #         instance.kind = "URL"
        #         url = self.cleaned_data["url"]
        #         instance.raw_name = url
        #         raise NotImplementedError("Downloading from an URL is not available yet")
        #     except KeyError:
        #         pass
        if file is not None:
            # with NamedTemporaryFile(mode='wb+', suffix="-%s" % file.name, delete=False) as destination:
            #     print(destination.name)
            #     for chunk in file.chunks():
            #         destination.write(chunk)
            #     file = destination.name

            # with open(file, "rb") as input_file:
            business_process.import_file(
                file=file,
                data_source=instance,
                importation_observer=importation_observer,
            )
        if commit:
            instance.save()
        return instance


class UploadToUpdateDataSourceForm(ImportDataSourceForm):
    class Meta:
        model = models.DataSource
        fields = ()


class DataSourceUserCreateOrUpdateForm(forms.ModelForm):
    new_allowed_users = forms.CharField(
        label=_("new_allowed_users__label"),
        help_text=_("new_allowed_users__help_text"),
        widget=widgets.Textarea(attrs={'rows': '4'}),
        required=False,
    )
    allowed_users = ByLastFirstNameModelMultipleChoiceField(
        label=_("allowed_users__verbose_name"),
        help_text=_("allowed_users__help_text"),
        queryset=get_user_model().objects.none(),
        widget=widgets.CheckboxSelectMultiple(),
        required=False,
    )

    class Meta:
        model = models.DataSource
        fields = (
            'name',
            # 'experiment_date',
            'publication_url',
            'life_domain',
            'description',
            'public',
            'allowed_users',
        )
        widgets = dict(
            raw_name=widgets.TextInput(),
            # experiment_date=DateInput,
        )

    def __init__(self, owner=None, data=None, *args, **kwargs):
        self.owner = owner
        super().__init__(data=data, *args, **kwargs)
        if self.instance and data and "public" in data and data["public"]:
            self.fields["description"].required = True
            # self.fields["experiment_date"].required = True

        if self.instance and self.instance.pk is not None:
            self.fields["allowed_users"].queryset = self.instance.allowed_users.order_by("last_name", "first_name")
            if not self.fields["allowed_users"].queryset.exists():
                try:
                    self.fields.pop("allowed_users")
                except KeyError:
                    pass
        else:
            # self.fields["allowed_users"].queryset = get_user_model().objects.none()
            try:
                self.fields.pop("allowed_users")
            except KeyError:
                pass
        try:
            self.fields["description"].widget.attrs["placeholder"] = _("description_placeholder_in_form")
        except KeyError:
            pass
        try:
            self.fields["public"].widget.attrs['data-onchange-js'] = 'update_allowed_user_visibility'
        except KeyError:
            pass

    def clean(self):
        f = super().clean()
        if "name" in self.cleaned_data:
            name = self.cleaned_data["name"]
            homonyms = only_public_or_granted_or_owned_queryset_filter(
                None,
                request=None,
                queryset=self._meta.model.objects.filter(name=name),
                user=self.owner
            )
            if self.instance:
                homonyms = homonyms.filter(~Q(pk=self.instance.pk))
            if homonyms.exists():
                self.data = self.data.copy()
                try:
                    key = self.data['data_source_wizard-current_step'] + "-name"
                except MultiValueDictKeyError:
                    key = "name"
                suffix_numbered = ' (%i)'
                suffix_named = None
                if homonyms.filter(owner=self.owner):
                    pass
                else:
                    suffix_named = ' - %s %s' % (self.owner.last_name, self.owner.first_name)
                    suffix_numbered = suffix_named + suffix_numbered

                if suffix_named and not self._meta.model.objects.filter(name=name + suffix_named).exists():
                    suffix = suffix_named
                else:
                    i = 1
                    while self._meta.model.objects.filter(name=name + (suffix_numbered % i)).exists():
                        i += 1
                    suffix = suffix_numbered % i
                self.data[key] += suffix
                self.add_error("name", mark_safe(_("homomys detected, suffixed by \"%s\"") % suffix))

        # self.cleaned_data["name"] = self.cleaned_data["name"] + " - toto"
        # self.add_error("name", "ee")
        # self.data = self.data.copy()
        # self.data[self.data['data_source_wizard-current_step'] + "-name"] += " - toto"
        try:
            if self.cleaned_data.get("public", False) and "experiment_date" not in self.cleaned_data:
                self.add_error("experiment_date", _("experiment_date_required_when_public"))
        except ValueError:
            pass
        try:
            if self.cleaned_data.get("public", False) and len(self.cleaned_data.get("description", "")) < 10:
                self.add_error("description", _("description_required_when_public%i") % 10)
        except ValueError:
            pass
        if "new_allowed_users" in self.cleaned_data:
            for c in self.cleaned_data["new_allowed_users"].replace("\r", "\n").replace("\n\n", "\n").split("\n"):
                if c == "":
                    continue
                cs = c.split(" ")
                if len(cs) == 1:
                    if not get_user_model().objects.filter(email=cs[0]).exists():
                        self.add_error("new_allowed_users", _("Unknown user '%s'") % c)
                elif len(cs) == 2:
                    users_count = get_user_model().objects.filter(
                        Q(first_name__iexact=cs[0]) & Q(last_name__iexact=cs[1])
                        | Q(last_name__iexact=cs[0]) & Q(first_name__iexact=cs[1])
                    ).count()
                    if users_count == 0:
                        self.add_error("new_allowed_users", _("Unknown user '%s'") % c)
                    elif users_count > 1:
                        self.add_error("new_allowed_users", _("Cannot determine of which user we are talking about, "
                                                              "please provide its email '%s'") % c)
        return f

    def save(self, commit=True):
        instance = super().save(commit=False)
        try:
            # Do not allow to edit the owner with this form
            getattr(instance, 'owner')
        except models.DataSource.owner.RelatedObjectDoesNotExist as e:
            instance.owner = self.owner
        if commit:
            instance.save()
            self.save_m2m()
        if "new_allowed_users" in self.cleaned_data:
            for c in self.cleaned_data["new_allowed_users"].replace("\r", "\n").replace("\n\n", "\n").split("\n"):
                c = c.strip()
                if c == "":
                    continue
                cs = c.split(" ")
                if len(cs) == 1:
                    q = Q(email=cs[0])
                elif len(cs) == 2:
                    q = Q(first_name__iexact=cs[0]) & Q(last_name__iexact=cs[1]) \
                        | Q(last_name__iexact=cs[0]) & Q(first_name__iexact=cs[1])
                else:
                    q = Q(pk=None)
                for user in get_user_model().objects.filter(q):
                    instance.allowed_users.add(user)
        if commit:
            instance.save()
        return instance


class MappingForm(forms.Form):
    raw_response = forms.FloatField(
    )
    mapping = forms.ModelChoiceField(
        queryset=models.GlobalViralHostResponseValue.objects_mappable().order_by("value"),
        required=True,
    )


MappingFormSet = forms.formset_factory(
    form=MappingForm,
    extra=0,
    can_delete=False,
)


class RangeMappingForm(forms.Form):
    def __init__(self, data_source=None, data=None, *args, **kwargs):
        super().__init__(data=data, *args, **kwargs)
        self.data_source = data_source
        use_default = not models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=data_source) \
            .filter(~Q(response__pk=models.GlobalViralHostResponseValue.get_not_mapped_yet_pk())) \
            .exists()
        min_maxs = []  # min_maxs will contains min and max for each GlobalViralHostResponseValue
        if use_default:
            try:
                from sklearn.cluster import KMeans
                import numpy as np
                responses = models.ViralHostResponseValueInDataSource.objects \
                    .filter(data_source=data_source) \
                    .values_list("raw_response", flat=True)
                clusters = KMeans(
                    n_clusters=models.GlobalViralHostResponseValue.objects_mappable().count(),
                    random_state=0,
                ).fit(np.array(responses).reshape(-1, 1)).cluster_centers_
                centroids = sorted([c[0] for c in clusters])
                # put in min_maxs the centroid as min and max for each GlobalViralHostResponseValue as we do not known
                # which element have been associated to each cluster (or don't want to compute it)
                min_maxs = [[x, x] for x in centroids]
            except ModuleNotFoundError:
                # Here when we do not have numpy
                pass
            except ValueError:
                # Here when there is no data to work on
                pass

        # if we do not use KMeans, of if it failed
        if not min_maxs:
            for m in models.GlobalViralHostResponseValue.objects_mappable().order_by("value"):
                min_max = models.ViralHostResponseValueInDataSource.objects \
                    .filter(data_source=data_source) \
                    .filter(response=m) \
                    .aggregate(Min('raw_response'), Max('raw_response'))
                min_maxs.append([min_max["raw_response__min"], min_max["raw_response__max"]])
            prev = None
            # Replace the None with an upper value
            for i in range(len(min_maxs) - 1, -1, -1):
                # Go backward from higher to lower GlobalViralHostResponseValue
                for j in range(1, -1, -1):
                    # Go backward from max (j=1) to min (j=0)
                    if min_maxs[i][j] is None:
                        # if the value is None replace it with the last seen values, and thus greater or equal of what
                        # we will see in the future
                        if prev is None:
                            # if prev is None, we find the overall maximum value, and increment it of one
                            prev = max(*[x for mm in min_maxs for x in mm if x is not None], *[-1000, -1000]) + 1
                        min_maxs[i][j] = prev
                    else:
                        # we see a value, so keeping it in memory for later None value to replace
                        prev = min_maxs[i][j]

        for pos, m in enumerate(models.GlobalViralHostResponseValue.objects_mappable().order_by("value")[1:], 1):
            key = 'mapping_for_%i_starts_with' % m.pk
            # val is the average between the maximum of the previous response and the minimum of the current response
            val = (min_maxs[pos][0] + min_maxs[pos - 1][1]) / 2
            self.fields[key] = forms.FloatField(
                initial=val,
                required=True,
            )
            self.fields[key].obj = m

    def clean(self):
        f = super().clean()
        previous_key = None
        for m in models.GlobalViralHostResponseValue.objects_mappable().order_by("value")[1:]:
            key = 'mapping_for_%i_starts_with' % m.pk
            if previous_key is None:
                previous_key = key
                continue
            try:
                val = self.cleaned_data[key]
                try:
                    if val < self.cleaned_data[previous_key]:
                        self.add_error(previous_key, _('Value should not be higher than the next one'))
                except KeyError:
                    self.add_error(previous_key, _('Field is required'))
            except KeyError:
                self.add_error(key, _('Field is required'))
        return f

    @property
    def get_current_range_start(self):
        return {f.obj.pk: f.initial for f in self.fields.values()}

    def save(self):
        ceiling = None
        for m in models.GlobalViralHostResponseValue.objects_mappable().order_by("-value"):
            key = 'mapping_for_%i_starts_with' % m.pk
            try:
                floor = self.cleaned_data[key]
            except KeyError:
                floor = None
            qs = models.ViralHostResponseValueInDataSource.objects.filter(data_source=self.data_source)
            if floor is not None:
                qs = qs.filter(raw_response__gte=floor)
            if ceiling is not None:
                qs = qs.filter(raw_response__lt=ceiling)
            qs.update(response=m)

            ceiling = floor


class AutoMergeSplitUpdateFormSet(forms.BaseModelFormSet):
    def __init__(self, data_source=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__data_source = data_source

    def save(self, commit=True):
        # objects = super().save(commit=False)
        # self.save_m2m()
        # for obj, reason in self.changed_objects:
        #     obj.save()
        #
        # def saveXXX(self, commit=True):
        assert (self.__data_source is not None)
        assert commit, "It has to be actually saved in DB"
        objects = super().save(commit=False)
        indexed_objects = dict([(o.pk, o) for o in objects])

        for obj, reason in self.changed_objects:
            # get an instance unchanged
            old_o = obj.__class__.objects.get(pk=obj.pk)
            # Find an object which have same identifier and names as what we are trying to save
            alter_ego = obj.__class__.objects \
                .filter(identifier=obj.identifier) \
                .filter(name=obj.name) \
                .filter(~Q(pk=obj.pk)).first()
            if alter_ego is None:
                if obj.data_source.count() == 1:
                    obj.save()
                    continue
                obj.pk = None
                obj.save()
                alter_ego = obj

            # remove the old instance from the current data_source
            old_o.data_source.remove(self.__data_source)
            old_o.save()

            # in any case, replace the instance that was about to be returned by the alter ego/newly created one
            indexed_objects[old_o.pk] = alter_ego
            # obj is now an other db instance, while old_o is still pointing to the original db instance

            # add the new instance to the data source
            alter_ego.data_source.add(self.__data_source)

            # get all the response associated to the original instance and the data source
            for r in old_o.responseindatasource.filter(data_source=self.__data_source):
                # and set the new instance in place of the old
                setattr(r, alter_ego.__class__.__name__.lower(), alter_ego)
                # don't forget to save !
                r.save()

            # If the old instance is not linked to anything anymore, we delete it
            if old_o.data_source.count() == 0:
                old_o.delete()
        # return all the instance changed
        return indexed_objects.values()


UpdateVirusFormSet = forms.modelformset_factory(
    model=models.Virus,
    form=forms.modelform_factory(
        model=models.Virus,
        fields=("name", "identifier", "id",),
    ),
    formset=AutoMergeSplitUpdateFormSet,
    extra=0,
    can_delete=False,

)

UpdateHostFormSet = forms.modelformset_factory(
    model=models.Host,
    form=forms.modelform_factory(
        model=models.Host,
        fields=("name", "identifier", "id",),
    ),
    formset=AutoMergeSplitUpdateFormSet,
    extra=0,
    can_delete=False,
)

DeleteVirusFormSet = forms.modelformset_factory(
    model=models.Virus,
    form=forms.modelform_factory(
        model=models.Virus,
        fields=("name", "identifier", "id",),
        widgets={
            "name": forms.TextInput(attrs={"readonly": True}),
            "identifier": forms.TextInput(attrs={"readonly": True}),
        }
    ),
    extra=0,
    can_delete=True,
)

DeleteHostFormSet = forms.modelformset_factory(
    model=models.Host,
    form=forms.modelform_factory(
        model=models.Host,
        fields=("name", "identifier", "id",),
        widgets={
            "name": forms.TextInput(attrs={"readonly": True}),
            "identifier": forms.TextInput(attrs={"readonly": True}),
        }
    ),
    extra=0,
    can_delete=True,
)

VirusForm = forms.modelform_factory(
    model=models.Virus,
    fields=(
        "name",
        "identifier",
    ),
)

HostForm = forms.modelform_factory(
    model=models.Host,
    fields=(
        "name",
        "identifier",
    ),
)


class BoostrapSelectMultiple(forms.SelectMultiple):
    def __init__(
            self,
            allSelectedText,
            nonSelectedText,
            SelectedText,
            onChangeFunctionName=None,
            attrs=None,
            *args,
            **kwargs,
    ):
        if attrs is not None:
            attrs = attrs.copy()
        else:
            attrs = {}
        if onChangeFunctionName is not None:
            attrs["data-on-change-fcn-name"] = onChangeFunctionName
        attrs.update({
            "data-all-selected-text": allSelectedText,
            "data-non-selected-text": nonSelectedText,
            "data-n-selected-text": SelectedText,
            "class": "bootstrap-multiselect-applicant",
            "style": "display:none;",
        })
        super().__init__(attrs=attrs, *args, **kwargs)


class ByNameModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class ByExplicitNameModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.explicit_name


class CommonSearchPrefForm(forms.Form):
    class Meta:
        abstract = True

    only_published_data = forms.BooleanField(
        label=_("Only published data"),
        help_text=_("only_published_data.help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'only-published',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 0,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        required=False,
        initial=False,
    )
    only_host_ncbi_id = forms.BooleanField(
        label=_("only_host_ncbi_id.label"),
        help_text=_("only_host_ncbi_id.help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'only-nbci-id',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 0,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        required=False,
        initial=False,
    )
    only_virus_ncbi_id = forms.BooleanField(
        label=_("only_virus_ncbi_id.label"),
        help_text=_("only_virus_ncbi_id.help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'only-nbci-id',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 0,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        required=False,
        initial=False,
    )
    life_domain = forms.ChoiceField(
        label=_("preference.life_domain.label"),
        help_text=_("preference.life_domain.help_text"),
        choices=(('', _('All')),) + models.DataSource._meta.get_field("life_domain").choices,
        widget=widgets.RadioSelect(
            attrs={
                'data-toggle-css-class': 'only-nbci-id',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 0,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        initial='',
        required=False,
    )
    helper = FormHelper()


class CommonPrefBrowseForm(CommonSearchPrefForm):
    class Meta:
        abstract = True

    advanced_option_families = {
        "2": _("Analysis Tools"),
        "0": _("Data filtering options"),
        "1": _("Rendering options"),
    }
    display_all_at_once = forms.BooleanField(
        # label=_("Fit table to screen, scroll when needed"),
        # help_text=_("Otherwise display all the table at once but may not fit on screen"),
        label=_("Display all the table at once, even if it does not fit on screen"),
        help_text=_("Otherwise we fit the table to the screen and use scroll bar when needed."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'freeze-col-header-v3 freeze-row-header-v3',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
            },
        ),
        required=False,
        initial=False,
    )
    horizontal_column_header = forms.BooleanField(
        label=_("horizontal_column_header_label"),
        help_text=_("horizontal_column_header_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
                'data-toggle-css-class': 'horizontal-col-header',
            },
        ),
        required=False,
        initial=False,
    )
    weak_infection = forms.BooleanField(
        label=_("Consider all positive response as a infection"),
        help_text=_("By default we consider that there is an infection only when the highest response is observed."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-1 order-1',
            },
        ),
        required=False,
        initial=False,
    )


class BrowseForm(CommonPrefBrowseForm):
    ds = ByNameModelMultipleChoiceField(
        label=_("Data source"),
        queryset=models.DataSource.objects.none(),
        widget=BoostrapSelectMultiple(
            allSelectedText=_("All data sources selected"),
            nonSelectedText=_("All relevant data sources") + "*",
            SelectedText=_(" data source selected"),
            onChangeFunctionName="filter_changed",
        ),
        required=False,
    )
    virus = ByExplicitNameModelMultipleChoiceField(
        label=_("Virus"),
        queryset=models.Virus.objects.none(),
        widget=BoostrapSelectMultiple(
            allSelectedText=_("All viruses selected"),
            nonSelectedText=_("All relevant viruses") + "*",
            SelectedText=_(" viruses selected"),
            onChangeFunctionName="filter_changed",
            attrs={
                "data-select-all-inv-nop": True,
            },
        ),
        required=False,
    )
    host = ByExplicitNameModelMultipleChoiceField(
        label=_("Host"),
        queryset=models.Host.objects.none(),
        widget=BoostrapSelectMultiple(
            allSelectedText=_("All hosts selected"),
            nonSelectedText=_("All relevant hosts") + "*",
            SelectedText=_(" hosts selected"),
            onChangeFunctionName="filter_changed",
            attrs={
                "data-select-all-inv-nop": True,
            },
        ),
        required=False,
    )
    focus_disagreements_toggle = forms.BooleanField(
        label=_("Render table focusing on disagreements"),
        help_text=_("Responses where data sources disagree are highlighted, others are faded."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'only-disagree',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
            },
        ),
        required=False,
        initial=False,
    )
    render_actual_aggregated_responses = forms.BooleanField(
        label=_("render_actual_aggregated_responses_label"),
        help_text=_("render_actual_aggregated_responses_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        required=False,
        initial=False,
    )
    virus_infection_ratio = forms.BooleanField(
        label=_("Show the infection ratio for the viruses"),
        help_text=_("Note that it can deteriorate the overall response time."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-2 order-3',
            },
        ),
        required=False,
        initial=False,
    )
    host_infection_ratio = forms.BooleanField(
        label=_("Show the infection ratio for the hosts"),
        help_text=_("Note that it can deteriorate the overall response time."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-3 order-5',
            },
        ),
        required=False,
        initial=False,
    )
    hide_rows_with_no_infection = forms.BooleanField(
        label=_("hide_virus_with_no_infection_label"),
        help_text=_("hide_rows_with_no_infection_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-toggle-css-class': 'hide-rows-with-no-infection',
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-5 order-4',
            },
        ),
        required=False,
        initial=False,
    )
    hide_cols_with_no_infection = forms.BooleanField(
        label=_("hide_host_with_no_infection_label"),
        help_text=_("hide_cols_with_no_infection_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-toggle-css-class': 'hide-cols-with-no-infection',
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-6 order-6',
            },
        ),
        required=False,
        initial=False,
    )
    agreed_infection = forms.BooleanField(
        label=_(
            "Consider that there is an infection only when <i>all data sources</i> "
            "documenting the interaction observed an infection"),
        help_text=_("By default we consider that there is an infection when in <i>at least one</i> "
                    "data source an infection was observed."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-4 order-2',
            },
        ),
        required=False,
        initial=False,
    )
    sort_rows = forms.ChoiceField(
        widget=widgets.HiddenInput(),
        choices=(('asc', 'asc'), ('desc', 'desc'),),
        required=False,
    )
    sort_cols = forms.ChoiceField(
        widget=widgets.HiddenInput(),
        choices=(('asc', 'asc'), ('desc', 'desc'),),
        required=False,
    )

    def __init__(self, data=None, user=None, initial=None, *args, **kwargs):
        use_pref = True
        initial = initial or dict()
        if data is not None:
            data = data.copy()
            initial.setdefault('host', [])
            initial.setdefault('virus', [])
            initial.setdefault("ds", [])
            for k, v in data.items():
                initial[k] = v
            for k in ['host', 'ds', 'virus']:
                list_for_k = data.getlist(k)
                if len(list_for_k) > 1:
                    initial[k] = list_for_k
                elif len(list_for_k) == 1:
                    initial[k] = list_for_k[0].split(',')
            use_pref = False
        if use_pref or data and data.get('use_pref', 'False').lower() == 'true':
            pref = get_user_preferences_for_user(user)
            for f in [
                'focus_disagreements_toggle',
                'display_all_at_once',
                'virus_infection_ratio',
                'host_infection_ratio',
                'hide_rows_with_no_infection',
                'hide_cols_with_no_infection',
                'weak_infection',
                'agreed_infection',
                'only_virus_ncbi_id',
                'only_host_ncbi_id',
                'only_published_data',
                'life_domain',
            ]:
                initial[f] = getattr(pref, f)
                if data:
                    data[f] = getattr(pref, f)
        super().__init__(initial=initial, data=data if data and len(data) > 0 else None, *args, **kwargs)
        if user is None:
            return
        self.fields["virus"].queryset = only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=user,
            queryset=models.Virus.objects,
            path_to_data_source="data_source__",
        )
        self.fields["host"].queryset = only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=user,
            queryset=models.Host.objects,
            path_to_data_source="data_source__",
        )
        self.fields["ds"].queryset = only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=user,
            queryset=models.DataSource.objects,
            path_to_data_source="",
        )


class VirusHostDetailViewForm(CommonPrefBrowseForm):
    infection_ratio = forms.BooleanField(
        label=_("Show the infection ratio"),
        help_text=_("Note that it can deteriorate the overall response time."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-0',
            },
        ),
        required=False,
        initial=False,
    )
    hide_rows_with_no_infection = forms.BooleanField(
        label=_("hide_rows_with_no_infection_label"),
        help_text=_("hide_rows_with_no_infection_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-toggle-css-class': 'hide-rows-with-no-infection',
                'data-container-additional-class': 'order-3',
            },
        ),
        required=False,
        initial=False,
    )
    hide_cols_with_no_infection = forms.BooleanField(
        label=_("hide_ds_with_no_infection_label"),
        help_text=_("hide_cols_with_no_infection_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-toggle-css-class': 'hide-cols-with-no-infection',
                'data-container-additional-class': 'order-4',
            },
        ),
        required=False,
        initial=False,
    )
    sort_rows = forms.ChoiceField(
        widget=widgets.HiddenInput(),
        choices=(('asc', 'asc'), ('desc', 'desc'),),
        required=False,
    )
    sort_cols = forms.ChoiceField(
        widget=widgets.HiddenInput(),
        choices=(('asc', 'asc'), ('desc', 'desc'),),
        required=False,
    )
    row_order = forms.CharField(
        widget=widgets.HiddenInput(),
        required=False,
    )
    col_order = forms.CharField(
        widget=widgets.HiddenInput(),
        required=False,
    )

    def __init__(self, data=None, user=None, pref=None, initial=None, *args, **kwargs):
        initial = initial or dict()
        pref = pref or get_user_preferences_for_user(user)
        initial['infection_ratio'] = pref.virus_infection_ratio or pref.host_infection_ratio
        initial['horizontal_column_header'] = True
        for f in [
            'focus_disagreements_toggle',
            'display_all_at_once',
            'weak_infection',
            'hide_rows_with_no_infection',
            'hide_cols_with_no_infection',
            'only_virus_ncbi_id',
            'only_host_ncbi_id',
            'only_published_data',
            'life_domain',
        ]:
            initial[f] = getattr(pref, f)
        super().__init__(initial=initial, data=data if data and len(data) > 0 else None, *args, **kwargs)


class EmptyForm(forms.Form):
    helper = FormHelper()
    helper.form_tag = False


class DataSourceNameModelForm(DataSourceUserCreateOrUpdateForm):
    class Meta:
        model = models.DataSource
        fields = (
            'name',
            'public',
            'life_domain',
        )

    helper = FormHelper()
    helper.form_tag = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop("new_allowed_users")


class DataSourceDescriptionModelForm(forms.ModelForm):
    class Meta:
        model = models.DataSource
        fields = (
            # 'experiment_date',
            'description',
            'publication_url',
        )
        widgets = dict(
            experiment_date=DateInput,
        )

    def __init__(self, required, msg=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["description"].required = required
        # self.fields["experiment_date"].required = required
        # Translators: The text displayed in the form field when it is empty.
        self.fields["description"].widget.attrs["placeholder"] = _("description_placeholder_in_form")
        self.helper = crispy_forms_helper.FormHelper()
        if msg != None:
            self.helper.layout = crispy_forms_layout.Layout(
                crispy_forms_layout.Row(
                    crispy_forms_layout.Column(
                        HTML(msg),
                        css_class='col-md-6 mb-0',
                    ),
                    crispy_forms_layout.Column(
                        # 'experiment_date',
                        'description',
                        'publication_url',
                        css_class='col-md-6 mb-0',
                    ),
                    css_class='form-row',
                ),
            )
        self.helper.form_tag = False

    def clean(self):
        f = super().clean()
        if self.fields["description"].required and len(self.cleaned_data.get("description", "")) < 10:
            self.add_error("description", _("description_required_when_public%i") % 10)
        return f


class DataSourceVisibilityModelForm(DataSourceUserCreateOrUpdateForm):
    class Meta:
        model = models.DataSource
        fields = (
            'allowed_users',
        )

    helper = FormHelper()
    helper.form_tag = False


class UploadOrLiveInput(forms.Form):
    upload_or_live_input = forms.ChoiceField(
        label=_("upload_or_live_input__label"),
        choices=(
            ("upload", _("UploadOrLiveInput__upload__choice")),
            # ("template", _("UploadOrLiveInput__template__choice")),
            ("live", _("UploadOrLiveInput__live_input__choice")),
        ),
        required=True,
        initial=None,
        widget=forms.RadioSelect,
    )
    helper = FormHelper()
    helper.form_tag = False


class UploadDataSourceForm(ImportDataSourceForm):
    make_upload_mandatory = False

    class Meta:
        model = models.DataSource
        fields = (
        )

    helper = FormHelper()
    helper.form_tag = False


DELIMITER_CHOICES = [
    # ('\n', _("delimiter__line_jump")),
    (None, mark_safe(ugettext("delimiter__automatic"))),
    ('\t', mark_safe(ugettext("delimiter__tabulation"))),
    (',', mark_safe(ugettext("delimiter__comma"))),
    (';', mark_safe(ugettext("delimiter__semicolon"))),
    ('two_col\t', mark_safe(ugettext("delimiter__two_col_tabulation_format"))),
    ('two_col ', mark_safe(ugettext("delimiter__two_col_tabulation_format"))),
    ('two_col,', mark_safe(ugettext("delimiter__two_col_comma_format"))),
    ('two_col;', mark_safe(ugettext("delimiter__two_col_semicolon_format"))),
    # (' ', mark_safe(ugettext("delimiter__space"))),
]


class LiveInputVirusHostForm(forms.Form):
    virus = forms.CharField(
        label=_("Virus"),
        help_text=_("LiveVirusHostInput__Virus__help_text"),
        required=True,
        widget=forms.widgets.Textarea({'rows': '8'})
    )
    virus_delimiter = forms.ChoiceField(
        label=_("LiveVirusHostInput__virus_delimiter__label"),
        help_text=_("LiveVirusHostInput__virus_delimiter__help_text"),
        choices=DELIMITER_CHOICES,
        required=False,
        initial=None,
        widget=forms.Select,
    )
    host = forms.CharField(
        label=_("Host"),
        help_text=_("LiveVirusHostInput__Host__help_text"),
        required=True,
        widget=forms.widgets.Textarea({'rows': '8'})
    )
    host_delimiter = forms.ChoiceField(
        label=_("LiveVirusHostInput__host_delimiter__label"),
        help_text=_("LiveVirusHostInput__host_delimiter__help_text"),
        choices=DELIMITER_CHOICES,
        required=False,
        initial=None,
        widget=forms.Select,
    )

    def __init__(self, data=None, *args, **kwargs):
        super().__init__(data=data, *args, **kwargs)
        self.fields["virus"].widget.attrs["placeholder"] = mark_safe(_("virus_placeholder_in_form"))
        self.fields["host"].widget.attrs["placeholder"] = _("host_placeholder_in_form")
        self.helper = crispy_forms_helper.FormHelper()
        self.helper.layout = crispy_forms_layout.Layout(
            crispy_forms_layout.Row(
                crispy_forms_layout.Column(
                    'virus',
                    'virus_delimiter',
                    css_class='col-md-6 mb-0',
                ),
                crispy_forms_layout.Column(
                    'host',
                    'host_delimiter',
                    css_class='col-md-6 mb-0',
                ),
                css_class='form-row',
            ),
        )
        self.helper.form_tag = False
        if not data:
            self.fields["host_delimiter"].widget = forms.HiddenInput()
            self.fields["virus_delimiter"].widget = forms.HiddenInput()

    def get_as_list(self, key):
        f = io.StringIO(self.cleaned_data[key])
        delimiter = self.cleaned_data["%s_delimiter" % key]
        if delimiter.startswith('two_col'):
            reader = csv.reader(f, delimiter=delimiter[7:])
            for row in reader:
                yield row[0].strip(), '' if len(row) < 2 else row[1].strip()
        else:
            reader = csv.reader(f, delimiter=delimiter)
            for row in reader:
                for cell in row:
                    cell = cell.replace('\r', '').replace('\n', '').strip()
                    if len(cell) > 0:
                        yield business_process.extract_name_and_identifier(cell)

    def get_guessed_delimiter(self, key):
        text = self.cleaned_data[key]
        guessing = []
        for d, _ in DELIMITER_CHOICES:
            if d is None:
                continue
            count = text.count(d)
            if count == 0:
                continue
            guessing.append((d, count))
        if len(guessing) == 0:
            return '\t'
        if len(guessing) == 1:
            if not text.endswith('\n'):
                text += '\n'
            line_jump = text.count('\n')
            if line_jump > 1 and line_jump == guessing[0][1]:
                return "two_col" + guessing[0][0]
            return guessing[0][0]
        return None

    def full_clean(self):
        super().full_clean()
        if not self.is_bound:  # Stop further processing.
            return
        for key in ["virus", "host"]:
            delimiter_key = "%s_delimiter" % key
            if '' == (self.cleaned_data[delimiter_key] or ''):
                self.cleaned_data[delimiter_key] = self.get_guessed_delimiter(key)
                if self.cleaned_data[delimiter_key] is None:
                    self.add_error(key, _("delimiter_not_found_please_check_input"))
                    self.add_error(delimiter_key, _("delimiter_not_found_please_specify_it"))
                # else:
                #     self.add_error(key, "eee")

    def get_template_files(self):
        col_header = [explicit_item(n, i) for n, i in self.get_as_list('host')]
        row_header = [explicit_item(n, i) for n, i in self.get_as_list('virus')]

        # Prepare the legend
        mapping = models.GlobalViralHostResponseValue.objects_mappable().order_by('value')
        legend_name = []
        legend_value = []
        legend = []
        for m in models.GlobalViralHostResponseValue.objects_mappable().order_by('value'):
            legend_name.append([m.name])
            legend_value.append(m.value)
            legend.append([m.name, m.value])
        df_legend = pd.DataFrame(legend_name, columns=[str(_("Recommended responses scheme"))], index=legend_value)

        filename = 'Template %(seed)s generated on %(date)s at %(time)s' % dict(
            date=timezone.now().strftime('%Y-%m-%d'),
            time=timezone.now().strftime('%Hh%Mm%Ss'),
            seed="".join([random.choice(string.ascii_letters) for i in range(4)])
        )
        file_path = os.path.join(settings.MEDIA_ROOT, filename + ".xlsx")
        with pd.ExcelWriter(file_path) as writer:
            df_data = pd.DataFrame(
                data=[["<response>"] * len(col_header)] * len(row_header),
                columns=col_header,
                index=row_header,
            )
            df_data.to_excel(writer)
            df_legend.to_excel(writer, startcol=1, startrow=len(row_header) + 3)
        os.chmod(file_path, 0o777)

        if len(col_header) > 4:
            col_header = col_header[:4] + ['...', ]
            data = ["<response>"] * 4 + ['...', ]
        else:
            data = ["<response>"] * len(col_header)
        if len(row_header) > 4:
            row_header = row_header[:4] + ['...', ]
            data = [data] * 4 + [['...'] * len(data), ]
        else:
            data = [data, ] * len(row_header)
        file_path = os.path.join(settings.MEDIA_ROOT, filename + ".sample.xlsx")
        with pd.ExcelWriter(file_path) as writer:
            df_data = pd.DataFrame(
                data=data,
                columns=col_header,
                index=row_header,
            )
            df_data.to_excel(writer)
        os.chmod(file_path, 0o777)
        return dict(
            file_url=settings.MEDIA_URL + filename + ".xlsx",
            sample_path=os.path.join(settings.MEDIA_ROOT, filename + ".sample.xlsx"),
        )


class ResponseUpdateForm(forms.ModelForm):
    # virus = forms.HiddenInput()
    # host = forms.HiddenInput()
    # data_source = forms.HiddenInput()

    class Meta:
        model = models.ViralHostResponseValueInDataSource
        fields = (
            'raw_response',
        )

    def __init__(self,
                 instance=None,
                 virus_pk=None,
                 host_pk=None,
                 ds_pk=None,
                 *args, **kwargs
                 ):
        if instance is not None and (
                virus_pk is not None or
                host_pk is not None or
                ds_pk is not None
        ):
            raise NotImplementedError()
        super().__init__(
            instance=models.ViralHostResponseValueInDataSource.objects.get(
                virus__pk=virus_pk,
                data_source__pk=ds_pk,
                host__pk=host_pk),
            *args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        mapping = dict(instance.data_source.get_mapping())
        try:
            instance.response = mapping[instance.raw_response]
        except KeyError:
            instance.response = models.GlobalViralHostResponseValue.get_not_mapped_yet()
        if commit:
            instance.save()
            instance.data_source.save()
            self.save_m2m()
        return instance


class ContactOwnerForm(forms.Form):
    recipient = forms.CharField(
        max_length=256,
        required=False,
        label=_("ContactOwnerForm__recipient"),
    )
    data_source = forms.CharField(
        max_length=256,
        required=False,
        label=models.DataSource._meta.verbose_name.title(),
    )
    subject = forms.CharField(
        max_length=256,
        required=True,
        label=_("ContactOwnerForm__email_subject"),
    )
    body = forms.CharField(
        required=True,
        label=_("ContactOwnerForm__email_body"),
        widget=forms.widgets.Textarea(),
    )

    def __init__(self, data_source=None, recipient=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if recipient:
            self.fields["recipient"].initial = recipient.last_name.upper() + " " + recipient.first_name.title()
        self.fields["recipient"].widget.attrs["disabled"] = True
        self.fields["data_source"].initial = str(data_source)
        self.fields["data_source"].widget.attrs["disabled"] = True


class SearchForm(CommonSearchPrefForm):
    search = forms.CharField(
        label=_("Term to search for"),
        help_text=_("Example: T4, NC_000866.4, spotting method"),
        required=True,
        min_length=1,
        widget=forms.HiddenInput(),
    )
    sample_size = forms.IntegerField(
        required=False,
        initial=0,
        widget=forms.HiddenInput(),
    )
    kind = forms.ChoiceField(
        choices=(
            ('all', _('All')),
            ('virus', _('Virus')),
            ('host', _('Host')),
            ('data_source', _('Data source')),
        ),
        initial='all',
        required=False,
        label=_("Search in"),
        widget=forms.RadioSelect(
            attrs={
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
    )
    ui_help = forms.BooleanField(
        initial=False,
        required=False,
        widget=forms.HiddenInput(),
    )
    owner = ByLastFirstNameModelMultipleChoiceField(
        queryset=get_user_model().objects.none(),
        label="",
        # label=_("SearchForm.owner.label"),
        help_text=_("SearchForm.owner.help_text"),
        widget=BoostrapSelectMultiple(
            allSelectedText=_("From all owner"),
            nonSelectedText=_("From all owner"),
            SelectedText=_(" owners selected"),
            onChangeFunctionName="load_data_wrapper",
            attrs={
                "data-select-all-inv-nop": False,
            },
        ),
        required=False,
    )
    helper = FormHelper()
    helper.layout = Layout(
        Row(
            Column(
                'sample_size',
                'ui_help',
                'kind',
                'search',
                css_class="col"
            ),
        ),
        Row(
            Column('life_domain', css_class="col"),
        ),
        Row(
            Column('only_published_data', css_class="col"),
        ),
        Row(
            Column('only_virus_ncbi_id', css_class="col"),
        ),
        Row(
            Column('only_host_ncbi_id', css_class="col"),
        ),
        Row(
            Column('owner', css_class="col"),
        ),
    )

    def __init__(self, user=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if user is None:
            return
        self.fields["owner"].queryset = get_user_model().objects.filter(
            datasource__in=only_public_or_granted_or_owned_queryset_filter(
                None,
                request=None,
                user=user,
                queryset=models.DataSource.objects.all(),
            )
        ).distinct()

    def clean(self):
        cleaned_data = super().clean()
        for key, value in cleaned_data.items():
            if value is None or value == "":
                initial = self.initial.get(key, self.fields[key].initial)
                cleaned_data[key] = initial
        return cleaned_data
