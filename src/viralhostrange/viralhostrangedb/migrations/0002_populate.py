# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def migration_code(apps, schema_editor):
    GlobalViralStrainResponseValue = apps.get_model("viralhostrangedb", "GlobalViralStrainResponseValue")

    GlobalViralStrainResponseValue.objects.update_or_create(
        name="NOT MAPPED YET",
    )


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(migration_code, reverse_code=migrations.RunPython.noop),
    ]
