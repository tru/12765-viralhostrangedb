# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def migration_code(apps, schema_editor):
    GlobalViralStrainResponseValue = apps.get_model("viralhostrangedb", "GlobalViralStrainResponseValue")

    for name, value in [("Lysis", 1), ("No lysis", 0)]:
        GlobalViralStrainResponseValue.objects.update_or_create(
            name=name,
            defaults=dict(
                value=value,
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0003_auto_20190304_1720'),
    ]

    operations = [
        migrations.RunPython(migration_code, reverse_code=migrations.RunPython.noop),
    ]
