# Generated by Django 2.1.7 on 2019-05-07 09:30

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viralhostrangedb', '0015_auto_20190502_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='host',
            name='identifier',
            field=models.CharField(blank=True, help_text='host__identifier__help_text', max_length=128, null=True, validators=[django.core.validators.MaxLengthValidator(128)], verbose_name='host__identifier__verbose_name'),
        ),
        migrations.AddField(
            model_name='virus',
            name='identifier',
            field=models.CharField(blank=True, help_text='virus__identifier__help_text', max_length=128, null=True, validators=[django.core.validators.MaxLengthValidator(128)], verbose_name='virus__identifier__verbose_name'),
        ),
    ]
