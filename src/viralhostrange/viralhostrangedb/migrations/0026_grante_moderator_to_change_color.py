# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import get_permission_codename
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.db import migrations


def migration_code(apps, schema_editor):
    moderator, _ = Group.objects.get_or_create(name="Moderator")
    GlobalViralHostResponseValue = apps.get_model("viralhostrangedb", "GlobalViralHostResponseValue")
    ct = ContentType.objects.get_for_model(GlobalViralHostResponseValue)
    opts = GlobalViralHostResponseValue._meta
    for action, codename, suffix in [
        ('change', get_permission_codename('change', opts) + "_color", "color"),
        ('view', get_permission_codename('view', opts), ""),
    ]:
        perm, _ = Permission.objects.get_or_create(
            codename=codename,
            name='Can %s %s%s' % (action, opts.verbose_name_raw, suffix),
            content_type=ct)
        moderator.permissions.add(perm)
        for perm in Permission.objects.filter(codename=codename, content_type=ct):
            moderator.permissions.add(perm)


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0025_auto_20191016_1400'),
    ]

    operations = [
        migrations.RunPython(migration_code, reverse_code=migrations.RunPython.noop),
    ]
