from django.contrib.auth.mixins import AccessMixin
from django.db.models import Q
from django.http import Http404


class MyAccessAndDispatchMixin(AccessMixin):
    def get_queryset(self):
        return self.queryset_filtering(
            request=self.request,
            queryset=super().get_queryset(),
            path_to_data_source=getattr(self, "path_to_data_source", "")
        )

    def dispatch(self, request, *args, **kwargs):
        try:
            return super().dispatch(request=request, *args, **kwargs)
        except Http404 as e:
            if self.request.user.is_authenticated:
                raise e
            return self.handle_no_permission()


def only_public_or_owned_queryset_filter(self, request, queryset, path_to_data_source="", user=None):
    if user is None:
        user = request.user
    if not user.is_authenticated:
        return queryset.filter(**{path_to_data_source + "public": True})
    return queryset.filter(
        Q(**{path_to_data_source + "owner": user})
        | Q(**{path_to_data_source + "public": True})
    )


class OnlyPublicOrOwnedMixin(MyAccessAndDispatchMixin):
    queryset_filtering = only_public_or_owned_queryset_filter


def only_public_or_granted_or_owned_queryset_filter(self, request, queryset, path_to_data_source="", user=None):
    if user is None:
        user = request.user
    if not user.is_authenticated:
        queryset = queryset.filter(**{path_to_data_source + "public": True}).distinct()
        if len(path_to_data_source) > 0:
            queryset = queryset.distinct()
        return queryset
    return queryset.filter(
        Q(**{path_to_data_source + "owner": user})
        | Q(**{path_to_data_source + "allowed_users": user})
        | Q(**{path_to_data_source + "public": True})
    ).distinct()


class OnlyPublicOrGrantedOrOwnedMixin(MyAccessAndDispatchMixin):
    queryset_filtering = only_public_or_granted_or_owned_queryset_filter


class OnlyPublicOrGrantedOrOwnedRelatedMixin(MyAccessAndDispatchMixin):
    queryset_filtering = only_public_or_granted_or_owned_queryset_filter
    path_to_data_source = "data_source__"


def only_public_or_granted_or_staff_or_owned_queryset_filter(self, request, queryset, path_to_data_source="",
                                                             user=None):
    if user is None:
        user = request.user
    if user.is_staff:
        return queryset
    if not user.is_authenticated:
        queryset = queryset.filter(**{path_to_data_source + "public": True}).distinct()
        if len(path_to_data_source) > 0:
            queryset = queryset.distinct()
        return queryset
    return queryset.filter(
        Q(**{path_to_data_source + "owner": user})
        | Q(**{path_to_data_source + "allowed_users": user})
        | Q(**{path_to_data_source + "public": True})
    ).distinct()


class OnlyPublicOrGrantedOrStaffOrOwnedMixin(MyAccessAndDispatchMixin):
    queryset_filtering = only_public_or_granted_or_staff_or_owned_queryset_filter


class OnlyPublicOrGrantedOrStaffOrOwnedRelatedMixin(MyAccessAndDispatchMixin):
    queryset_filtering = only_public_or_granted_or_staff_or_owned_queryset_filter
    path_to_data_source = "data_source__"


def only_public_or_owned_or_staff_queryset_filter(self, request, queryset, path_to_data_source="", user=None):
    if user is None:
        user = request.user
    if user.is_staff:
        return queryset
    if not user.is_authenticated:
        return queryset.filter(**{path_to_data_source + "public": True})
    return queryset.filter(
        Q(**{path_to_data_source + "owner": user})
        | Q(**{path_to_data_source + "public": True})
    )


class OnlyPublicOrOwnedOrStaffMixin(MyAccessAndDispatchMixin):
    queryset_filtering = only_public_or_owned_or_staff_queryset_filter


# class StaffOnlyMixin(AccessMixin):
#     def dispatch(self, request, *args, **kwargs):
#         if not request.user.is_authenticated:
#             return self.handle_no_permission()
#         if not request.user.is_staff:
#             return HttpResponseForbidden()
#         return super().dispatch(request=request, *args, **kwargs)


def only_owned_queryset_filter(self, request, queryset, path_to_data_source="", user=None):
    if user is None:
        user = request.user
    if not user.is_authenticated:
        return queryset.none()
    queryset = queryset.filter(**{path_to_data_source + "owner": user})
    if len(path_to_data_source) > 0:
        queryset = queryset.distinct()
    return queryset


class OnlyOwnedMixin(MyAccessAndDispatchMixin):
    queryset_filtering = only_owned_queryset_filter


class OnlyOwnedRelatedMixin(MyAccessAndDispatchMixin):
    queryset_filtering = only_owned_queryset_filter
    path_to_data_source = "data_source__"

# def only_owned_or_staff_queryset_filter(self, request, queryset):
#     if request.user.is_staff:
#         return queryset
#     if not request.user.is_authenticated:
#         return queryset.none()
#     return queryset.filter(owner=request.user)
#
#
# class OnlyOwnedOrStaffMixin(MyAccessAndDispatchMixin):
#     queryset_filtering = only_owned_or_staff_queryset_filter
