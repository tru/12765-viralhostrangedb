from django.contrib.auth import get_user_model
from drf_dynamic_fields import DynamicFieldsMixin
from rest_framework import serializers

from viralhostrangedb import models


class VirusSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Virus
        depth = 1
        fields = ('name', 'identifier', 'id',)


class HostSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Host
        depth = 1
        fields = ('name', 'identifier', 'id',)


class DataSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DataSource
        depth = 1
        fields = (
            'id',
            'name',
            'description',
            'public',
            'creation_date',
            'last_edition_date',
        )


class VirusSerializerWithURL(DynamicFieldsMixin, serializers.ModelSerializer):
    url = serializers.URLField(read_only=True, source='get_absolute_url')

    class Meta:
        model = models.Virus
        depth = 1
        fields = ('name', 'identifier', 'id', 'url',)


class HostSerializerWithURL(DynamicFieldsMixin, serializers.ModelSerializer):
    url = serializers.URLField(read_only=True, source='get_absolute_url')

    class Meta:
        model = models.Host
        depth = 1
        fields = ('name', 'identifier', 'id', 'url',)


class OwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name']


class OwnerRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        return value.last_name.upper() + " " + value.first_name.title()


class DataSourceSerializerForSearch(DynamicFieldsMixin, serializers.ModelSerializer):
    url = serializers.URLField(read_only=True, source='get_absolute_url')
    last_edition_date = serializers.DateTimeField(format="%Y-%m-%d")
    creation_date = serializers.DateTimeField(format="%Y-%m-%d")
    owner = OwnerRelatedField()

    class Meta:
        model = models.DataSource
        depth = 1
        fields = (
            'id',
            'name',
            'description',
            'public',
            'owner',
            'url',
            'creation_date',
            'last_edition_date',
        )
