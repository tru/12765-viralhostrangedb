function update_allowed_user_visibility(e){
    var parents = $('[name="allowed_users"]').add('[name="new_allowed_users"]').closest(".form-group");
    if($(e.target).prop("checked")){
        parents.hide();
    }else{
        parents.show();
    }
}
$(document).ready(function(){
    update_allowed_user_visibility({target:$('[name="public"]')});
});