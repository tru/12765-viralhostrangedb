$(document).ready(function(){
    $('input[name="raw_response"]')
    .keyup(raw_response_changed)
    .mouseup(raw_response_changed)
    .change(raw_response_changed)
    .change()
    $('input[name="raw_response"]').change();
});

function raw_response_changed(event){
    console.log(event);
    console.log(parseFloat($(event.target).val()));
    console.log(scheme[parseFloat($(event.target).val())]);
    console.log("#scheme-"+$(event.target).val()    );
    console.log(document.getElementById("scheme-"+$(event.target).val()    ));

    var val=$(event.target).val().replace(",",".");
    if ( val.indexOf('.') == -1){
        val+=".0";
    }
    mapping = $(document.getElementById("scheme-"+val)).html();
    if ( typeof mapping == "undefined" ){
        $("button[type='submit']").html(gettext('Update the response and proceed to mapping updated'));
    }else{
        $("button[type='submit']").html(gettext('Update the response and map it to ') + '<i>" ' + mapping + '"</i>' );
    }
}