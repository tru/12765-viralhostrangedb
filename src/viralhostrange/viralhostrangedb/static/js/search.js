$(document).ready(function(){
    $('input.searched_term').trigger("input");
    search_now();
    $("#big_search_bar").keyup(function(e){
        delayed_search_now();
    });
});

delayed_search_now = delayed_action(
        search_now,
        function (){
            $("#big_search_bar").siblings(".spinner").show();
        },
        500
    );

function search_now(){
    let $form=$('#search_deeper');
    $.ajax({
        url : $form.attr("data-action"),
        dataType : 'json',
        data : $form.serialize(),
        success : function(data){
            console.log(data);
            let $detail=$('.results-detail');
            $detail.empty();
            $("#big_search_bar").siblings(".spinner").hide();
            for(k in data["ui_help"]){
                let $see_all=null;
                count=data[k]["count"];

                $header_html = $($(
                    '[data-template*="'+k+'"][data-template-kind="header'+(count==0?'-no-match':'')+'"]'
                ).html());
                $header_html.find("[data-source=ui_help]").text(data["ui_help"][k]);
                $header_html.find("[data-source=subset_url]").attr("href",data[k]["subset_url"]);
                $header_html.appendTo($detail);

                $(data[k]["sample"]).each(function(i,entry){
                    $entry_html = $($('[data-template*="'+k+'"][data-template-kind="main"]').html());
                    console.log(entry);
                    for (entry_key in entry){
                        let elt = $entry_html.find('[data-source="'+entry_key+'"]');
                        if (typeof elt.attr("data-target") == "undefined"){
                            elt.text(entry[entry_key]);
                        }else{
                            elt.attr(elt.attr("data-target"),entry[entry_key]);
                        }
                    }
                    $entry_html.appendTo($detail);
                });
                if (count>data["query"]["sample_size"] && data["query"]["sample_size"] > 0){
                    $see_all = $($('[data-template*="'+k+'"][data-template-kind="see-all-has-more"]').html());
                }else if (count>0){
                    $see_all = $($('[data-template*="'+k+'"][data-template-kind="see-all"]').html());
                }
                if ($see_all != null){
                    $see_all.find("[data-source=subset_url]").attr("href",data[k]["subset_url"]);
                    $see_all.appendTo($detail);
                }
            }
        }
    });
}

function load_data_wrapper(){
    $("input[name=sample_size]").val(0);
    search_now();
}