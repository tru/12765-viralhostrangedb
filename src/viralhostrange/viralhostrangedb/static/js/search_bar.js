$(document).ready(function(){
    $('#search_bar').autocomplete({
        source : function(request, response_callback){
            $.ajax({
                url : $('#search_bar').closest("form").attr("data-action"),
                dataType : 'json',
                data : {
                    search : request["term"],
                },

                success : function(data){
                    console.log(data);
                    responses=[];
                    has_no_match=true;
                    $.each(data, function(k,v){
                        if (k != "query" && v.count>0){
                            has_no_match=false;
                        }
                    });
                    $.each(data, function(k,v){
                        if (k != "query" && (has_no_match || v.count>0)){
                            v["kind"]=k;
                            responses.push(v);
                        }
                    });
                    response_callback(responses);
                }
            });
        },
        appendTo: '#search-bar-container'
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        console.log(item);

        let html="",
            icon,
            verbose_name,
            verbose_has_more;

        if (item["kind"]=="host"){
            icon='<span class="btf-host"></span> ';
            verbose_name=gettext("Hosts");
            verbose_has_more= gettext("See all %s matching hosts");
        }else if (item["kind"]=="virus"){
            icon='<span class="btf-virus"></span> ';
            verbose_name=gettext("Viruses");
            verbose_has_more= gettext("See all %s matching viruses");
        }else if (item["kind"]=="data_source"){
            icon='<i class="fa fa-database"></i> ';
            verbose_name=gettext("Data source");
            verbose_has_more= gettext("See all %s matching data sources");
        }
        html+='<li class="ui-state-disabled ui-menu-title" aria-disabled="true">'
            +'<div tabindex="-1" class="ui-menu-item-wrapper">'
            +icon+verbose_name
            +'</div>'
            +'</li>';

        if (item["count"]==0){
            html+='<li class="ui-state-disabled">'
                +'<div tabindex="-1" class="ui-menu-item-wrapper">'
                +'<i>'
                +gettext("No match")
                +'</i>'
                +'</div>'
                +'</li>';
        }
        $.map(item["sample"], function(o){
            let content;
            if ("identifier" in o){
                content = o.name +(o.identifier==''?'':'<span class="identifier"> ('+o.identifier+') </span>');
            }else{
                content = "<div>"
                            + o.name +' ('+gettext('From: ')+o.owner.first_name + ' ' + o.owner.last_name+')'
                            + "</div><small>"
                            + o.description.substring(0,100)+(o.description.length>100?"...":'')
                            + "</small>";

            }
            html+='<li class="ui-menu-item">'
                +'<a tabindex="-1" class="ui-menu-item-wrapper" href="'+o.url+'">'
                +content
                +'</a>'
                +'</li>';
        });
        if (item["count"]>item["sample"].length){
            html+='<li class="ui-menu-item">'
                +'<a tabindex="-1" class="ui-menu-item-wrapper" href="'+item.subset_url+'">'
                +"<i>"
                +interpolate(verbose_has_more, [item.count, ])
                +"</i>"
                +'</a>'
                +'</li>';
        }
        html+='<li>-</li>';
        return $(html).appendTo( ul );
    }
    ;
});