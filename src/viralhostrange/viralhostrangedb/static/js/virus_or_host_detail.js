$(document).ready(function(){
    load_data_vh().then(refresh_all_infection_ratio);
    $("[data-toggle-css-class]").change(function(e){
        $(e.target).attr("data-toggle-css-class").split(" ").map(function(c){
            $("#grid_container").toggleClass(c);
        });
    });
    refresh_advanced_option_counter();
    $("[data-advanced-option]").change(function(e){
        refresh_advanced_option_counter();
        update_get_parameter();
    });
    $("[data-toggle-css-class]:checked").change();
    $('#advanced_holder').mouseleave(function(){$('#advanced_holder:not(.pinned)').collapse('hide');});
});

function toggle_pin_advanced_holder() {
    $("#advanced_holder").toggleClass("pinned");
}

function update_get_parameter(){
    var other_history="";
    $.map($("#form_filter").serializeArray(), function(n, i){
        if  (n['name'] == "csrfmiddlewaretoken" ||
             n['name'] == "row_order" ||
             n['name'] == "col_order"){
            //nothing
        }else {
            other_history+="&"+n['name']+"="+(n['value'] == "on" ? "true" :n['value']);
        }
    });
    window.history.replaceState('VH_DETAIL', 'VH_DETAIL', "?p"+other_history);
}


function load_data_vh(){
    return $.ajax({
        headers: {
            'X-CSRFToken':getCookie('csrftoken'),
        },
        type: "GET",
        data:{},
        url:get_responses,
        success: function (data, textStatus, xhr) {
            $('#grid_host tbody').empty();
            hover_behavior($('#grid_host thead>tr>th[data-col]'));
            if(focus_is_on_first_level_of_data){
                let pk;
                for (k in data){
                    pk=k;
                }
                data=data[pk];
            }
            data_init_order=-1;
            for(let row in data){
                let data_col = data[row];
                if(! focus_is_on_first_level_of_data){
                    let pk;
                    for (k in data_col){
                        pk=k;
                    }
                    data_col=data_col[pk];
                }
                data_init_order+=1;
                let data_row_attr = "data-row="+row;
                let tr_row_str='<tr '+data_row_attr+'>'+
                            '<th '+data_row_attr+' data-init-order="'+data_init_order+'">'+
                            '<a target="_blank" href="'+get_detail_url.replace("000000",row)+'"><i>_'+row+'</i></a>'+
                            '<span class="ratio"></span>'+
                            '</th>'+
                          '</tr>';
                let tr_row = $(tr_row_str);
                tr_row.appendTo($("#grid_host").find("tbody"));
                let hosts = $('#grid_host thead>tr>th[data-col]').map(function(i,o){
                    tr_row.append($('<td '+data_row_attr+' data-col="'+o.getAttribute("data-col")+'">&nbsp;</td>'));
                });
                hover_behavior(tr_row.children("td"));
                $.ajax({
                    headers: {
                        'X-CSRFToken':getCookie('csrftoken'),
                    },
                    type: "GET",
                    url:get_detail_api_url.replace("000000",row),
                    data: {
                    },
                    success: function (data, textStatus, xhr) {
                        if (data["identifier"] == null || data["identifier"] == ""){
                            $('.grid_host tbody>tr>th[data-row='+data["id"]+']>a').html(
                                '<span>'+data["name"]+'</span>'
                            );
                        }else{
                            $('.grid_host tbody>tr>th[data-row='+data["id"]+']>a').html(
                                '<span class="identifier"> ('+data["identifier"]+') </span>'
                                +'<span>'+data["name"]+'</span>'
                            );
                        }
                    },
                });

                for(let col in data_col){
                    let val = data_col[col]
                    td=tr_row.children('td[data-col="'+col+'"]');
                    // update the value
                    td.text(val);
                    //keep in mind if it was hovered
                    let hover=td.hasClass("hover");
                    //remove all class, easier than removing all schema-* classes
                    td.removeClass();
                    //restore hover status
                    if (hover){
                        td.addClass("hover");
                    }
                    //generate custom css key ...
                    let css_key=("schema-"+val).replace(".","-");
                    td.addClass(css_key);
                    // ... and ask to load it
                    addCSS(custom_css+val, css_key);
                }
            }
        },
    });
}

// Include CSS file
function addCSS(filename, css_key){
    if (typeof css_key != "undefined" && $("#"+css_key).length>0){
        return;
    }
    var head = document.getElementsByTagName('head')[0];

    var style = document.createElement('link');
    style.href = filename;
    style.type = 'text/css';
    style.id = css_key;
    style.rel = 'stylesheet';
    head.append(style);
}

function hover_behavior(selector){
    $(selector).hover(function() {
        let pos=$(this).index()+1;
        $(this).closest('table').find('tr>*:nth-child('+pos+')').toggleClass('hover');
    });
    $(selector).filter("td").click(function() {
        var elt=$(this);
        if( typeof elt.attr("aria-describedby") == "undefined"){
            elt.popover({
                content: '<i class="fa fa-spinner fa-spin"></i>',
                html: true,
                title : '<span>'+gettext('Details')+'</span>'+
                        '&nbsp;<a role="button" class="close_cmd text-danger float-right">'+
                        '<i class="fa fa-times"></i></a>',
                container: 'body'
            }).on('shown.bs.popover', function(e){
                var popover=$("#"+$(this).attr("aria-describedby"));
                popover.find("a.close_cmd").on('click', function(e){
                    elt.popover('hide').popover('dispose').attr("aria-describedby",null);
                });
                var url=update_response.replace("000000",$(elt).attr("data-col")).replace("111111",$(elt).attr("data-row"));
                content = $('<dl class="mb-0">'+
                    '<dt>'+gettext('Data source name')+'</dt>'+
                    '<dd data-data-source='+$(elt).attr("data-col")+'></dd>'+
                    '<dt>'+in_row_kind+'</dt>'+
                    '<dd data-row='+$(elt).attr("data-row")+'></dd>'+
                    '<dt>'+detailed_kind+'</dt>'+
                    '<dd>'+$("#detailed_name").html()+'</dd>'+
                    '</dl>'+
                    '<a class="allow-deco pull-right mb-2" href="'+url+'">'+gettext('Edit response')+' <i class="fa fa-pencil"></i></a>'
                ).appendTo($(popover).find(".popover-body").empty());
                $($('#grid_host thead>tr>th[data-col='+$(elt).attr("data-col")+']').html()).appendTo(
                    content.find('[data-data-source='+$(elt).attr("data-col")+']')
                );
                $($(elt).closest('tr[data-row='+$(elt).attr("data-row")+']').children("th").html()).appendTo(
                    content.find('[data-row='+$(elt).attr("data-row")+']')
                );
            }).popover('show');
        }else{
            elt.popover('hide').popover('dispose').attr("aria-describedby",null);
        }
    });
}

function load_infection_ratios(){
    let data_to_send=$('[name="weak_infection"]:checked').length?'weak_infection=on':'';
    $.ajax({
        headers: {
            'X-CSRFToken':getCookie('csrftoken'),
        },
        type: "GET",
        data:data_to_send,
        url:get_infection_ratios,
        success: function (data, textStatus, xhr) {
            let actual_total=$('#grid_host th[data-col]').length;
            data_hv = data["host"] || data["virus"];
            data_ds = data["data_source"];
            $('#grid_host tbody tr').removeClass("zero-infection");
            for(let row in data_hv){
                var ratio=data_hv[row]['ratio'];
                var total=data_hv[row]['total'];
                ratio=ratio*total/actual_total;
                var title=gettext("Infection ratio:")+(ratio*100).toFixed(3)+"%";
                var tr = $('#grid_host tr[data-row="'+row+'"]');
                if (ratio==0){
                    tr.addClass("zero-infection");
                }
                ratio_str=(ratio<0.1?"&nbsp;":"")+(ratio*100).toFixed(0)+"%";
                tr.find('th .ratio')
                    .html(ratio_str)
                    .attr('title',title)
                    .attr('data-sort',ratio)
                    .addClass("computed");
            }
            $('#grid_host th').add($('#grid_host td')).removeClass("zero-infection");
            actual_total=$('#grid_host tr[data-row]').length;
            for(let col in data_ds){
                let ratio=data_ds[col]['ratio'];
                let total=data_ds[col]['total'];
                ratio=ratio*total/actual_total;
                title=gettext("Infection ratio:")+(ratio*100).toFixed(3)+"%";
                let tc = $('#grid_host thead th[data-col="'+col+'"]');
                if (ratio==0){
                    let pos=$(tc).index()+1;
                    $(tc).closest('table').find('tr>*:nth-child('+pos+')').addClass('zero-infection');
                }
                ratio_str=(ratio<0.1?"&nbsp;":"")+(ratio*100).toFixed(0)+"%";
                tc.find('.ratio')
                    .html(ratio_str)
                    .attr('title',title)
                    .attr('data-sort',ratio)
                    .addClass("computed");
            }
            toggle_sort_rows(true);
            toggle_sort_cols(true);
        }
    });
}

function refresh_all_infection_ratio(){
    if ($('[name="infection_ratio"]:checked').length == 1){
        $('#grid_host .splitter .sorter').show();
        $("[name='weak_infection']").prop("disabled",false);
        $("[name='hide_rows_with_no_infection']").prop("disabled",false);
        $("[name='hide_cols_with_no_infection']").prop("disabled",false);
        $('#grid_host .ratio').text("--%");
        load_infection_ratios();
    }else{
        $('#grid_host .ratio').text("").attr("data-sort","");
        $('#grid_host .splitter .sorter').hide();
        $("[name='weak_infection']").prop("disabled",true);
        $("[name='hide_rows_with_no_infection']").prop("disabled",true);
        $("[name='hide_cols_with_no_infection']").prop("disabled",true);
    }
}

function refresh_advanced_option_counter(){
    let l = $("[data-advanced-option]:checked:not([value=''])").length;
    l=(l==0?"":" ("+l+")");
    $('[href="#advanced_holder"] .counter').text(l);
    $("#advanced_holder [data-toggle]").map(function(i,o){
        let l = $($(o).attr("href")).find(":checked:not([value=''])").length;
        l=(l==0?"":" ("+l+")");
        $(o).find('.counter').text(l);
    });
}