import logging

from django import template
from django.conf import settings
from django.db.models import Q, QuerySet

from viralhostrangedb import models

register = template.Library()

logger = logging.getLogger(__name__)


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def has_mapping_pending(owner):
    return owner is not None and models.DataSource.has_not_mapped_data_sources(owner=owner)


@register.filter
def class_verbose_name(obj):
    return obj._meta.verbose_name.title()


# @register.filter
# def class_verbose_name_plural(obj):
#     return obj._meta.verbose_name_plural.title()


@register.filter
def field_verbose_name(obj, field_name):
    return obj._meta.get_field(field_name).verbose_name.title()


# @register.filter
# def field_verbose_class_name(obj, field_name):
#     return obj._meta.get_field(field_name).related_model._meta.verbose_name.title()


# @register.filter
# def field_verbose_class_name_plural(obj, field_name):
#     return obj._meta.get_field(field_name).related_model._meta.verbose_name_plural.title()


@register.filter
def can_edit(user, obj):
    # if user is a WSGIRequest, get the attr user
    user = getattr(user, "user", user)
    try:
        # if hasattr(obj, "data_source"):
        #     return obj.data_source.filter(owner__pk=user.pk).exists()
        return obj.owner.pk == user.pk
    except Exception as e:
        if settings.DEBUG:
            raise e
    return False


@register.filter
def can_see(user, obj):
    # if user is a WSGIRequest, get the attr user
    user = getattr(user, "user", user)
    try:
        # if hasattr(obj, "data_source"):
        #     return obj.data_source.filter(Q(Q(owner__pk=user.pk) | Q(allowed_users__pk=user.pk))).exists()
        if isinstance(obj, QuerySet):
            if obj.model == models.DataSource:
                qs_filter = Q(Q(owner__pk=user.pk) | Q(public=True))
                if user.pk is not None:
                    qs_filter = Q(qs_filter | Q(allowed_users__pk=user.pk))
                return obj.filter(qs_filter)
            raise NotImplementedError("tag can_see not implemented for QuerySet of " + obj.model.__name__)
        return obj.owner.pk == user.pk or obj.allowed_users.filter(pk=user.pk).exists()
    except Exception as e:
        if settings.DEBUG:
            raise e
    return False


@register.filter
def is_advanced_option(obj):
    try:
        return obj.field.widget.attrs['data-advanced-option']
    except KeyError:
        return False


@register.filter
def get_in_widget(obj, attr):
    try:
        return str(obj.field.widget.attrs[attr])
    except KeyError as e:
        return ''
