import json
import logging
import os

from django.contrib.auth import get_user_model
from django.db.models import Q
from django.test import TestCase
from django.utils import timezone

from viralhostrangedb import business_process, models
from viralhostrangedb.business_process import extract_name_and_identifier, ImportationObserver
from viralhostrangedb.tests.test_views_others import ViewTestCase

logger = logging.getLogger(__name__)


class DataFileReaderTestCase(TestCase):
    test_data = "./test_data"
    example_public_data = "./viralhostrangedb/static/media/"

    def setUp(self):
        self.owner = get_user_model().objects.create(
            username="root",
        )

    def test_read_work_fine(self):
        for f in [f for f in os.listdir(self.test_data) if f.endswith(".xlsx") and not f.endswith(".err.xlsx")]:
            vhrs = business_process.parse_file(os.path.join(self.test_data, f))
            vhrs_dict = business_process.to_dict(vhrs)
            try:
                with open(os.path.join(self.test_data, f + ".json")) as stream:
                    data = json.load(stream)
                    self.assertEqual(data, vhrs_dict, "with file %s" % f)
            except FileNotFoundError as e:
                if os.path.exists(os.path.join(self.test_data, f + ".json.not_needed")):
                    continue
                logger.exception("json associated file is missing, reducing robustness of tests. "
                                 "We created it, check if it is correct")
                logger.exception(json.dumps(vhrs_dict, indent=2))
                open(os.path.join(self.test_data, f + ".json.not_needed.candidate"), "w")
                with open(os.path.join(self.test_data, f + ".json.candidate"), "w") as f:
                    f.write(json.dumps(vhrs_dict, indent=2))

    def test_read_work_with_stream(self):
        with open(os.path.join(self.test_data, "simple.xlsx"), "rb") as input_file:
            business_process.parse_file(input_file)

    def test_examples_works(self):
        for f in [f for f in os.listdir(self.example_public_data) if
                  f.endswith(".xlsx") and not f.endswith(".err.xlsx")]:
            with open(os.path.join(self.example_public_data, f), "rb") as input_file:
                business_process.parse_file(input_file)

    def test_import_in_db(self):
        filename = os.path.join(self.test_data, "simple.xlsx")
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.owner, self.owner)
        self.assertEqual(models.Virus.objects.count(), 3)
        self.assertEqual(models.Host.objects.count(), 4)
        self.assertEqual(models.ViralHostResponseValueInDataSource.objects.get(
            data_source=data_source,
            host=models.Host.objects.get(data_source=data_source, name="C4"),
            virus=models.Virus.objects.get(data_source=data_source, name="r1"),
        ).raw_response, 4)
        self.assertEqual(models.ViralHostResponseValueInDataSource.objects.get(
            data_source=data_source,
            host=models.Host.objects.get(data_source=data_source, name="C2"),
            virus=models.Virus.objects.get(data_source=data_source, name="r2"),
        ).raw_response, 6)
        self.assertEqual(models.ViralHostResponseValueInDataSource.objects.get(
            data_source=data_source,
            host=models.Host.objects.get(data_source=data_source, name="C3"),
            virus=models.Virus.objects.get(data_source=data_source, name="r3"),
        ).raw_response, 11)
        self.assertEqual(models.ViralHostResponseValueInDataSource.objects.get(
            data_source=data_source,
            host=models.Host.objects.get(data_source=data_source, name="C1"),
            virus=models.Virus.objects.get(data_source=data_source, name="r1"),
        ).raw_response, 1)

    def test_import_file_remove_deleted_virus(self):
        models.Virus.objects.all().delete()
        models.Host.objects.all().delete()
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name="blabla",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4)

        filename = os.path.join(self.test_data, "empty_row.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.virus_set.count(), 3 - 1)
        self.assertEqual(data_source.host_set.count(), 4)
        self.assertEqual(models.Virus.objects.count(), 3 - 1)
        self.assertEqual(models.Host.objects.count(), 4)

    def test_import_file_remove_deleted_host(self):
        models.Virus.objects.all().delete()
        models.Host.objects.all().delete()
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name="blabla",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4)

        filename = os.path.join(self.test_data, "empty_col.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4 - 1)
        self.assertEqual(models.Virus.objects.count(), 3)
        self.assertEqual(models.Host.objects.count(), 4 - 1)

    def test_import_file_remove_deleted_response_but_do_not_delete_host_or_virus(self):
        # shift pk for hosts (easier to debug)
        models.Host.objects.create(name="won't last", pk=100).delete()
        # clean up the db
        models.ViralHostResponseValueInDataSource.objects.all().delete()
        models.Virus.objects.all().delete()
        models.Host.objects.all().delete()

        # create a first file, so every Virus/Host is used for sure
        filename = os.path.join(self.test_data, "simple.xlsx")
        stable, _ = models.DataSource.objects.get_or_create(
            raw_name="stable",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=stable,
            file=filename,
        )

        # create the fluctuating object
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name="blabla",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        # check everything is as expected
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4)

        # re-import without a column
        filename = os.path.join(self.test_data, "empty_col.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        # one column is missing, but the rest remains
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4 - 1)
        self.assertEqual(models.Virus.objects.count(), 3)
        self.assertEqual(models.Host.objects.count(), 4)

        # re-import without a row
        filename = os.path.join(self.test_data, "empty_row.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        # on row is missing, but the rest remains
        self.assertEqual(data_source.virus_set.count(), 3 - 1)
        self.assertEqual(data_source.host_set.count(), 4)
        self.assertEqual(models.Virus.objects.count(), 3)
        self.assertEqual(models.Host.objects.count(), 4)

    def test_import_in_db_failure(self):
        filename = os.path.join(self.test_data, "fails.xlsx")
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        self.assertRaises(ValueError, business_process.import_file, data_source=data_source, file=filename)
        business_process.import_file(data_source=data_source, file=filename, importation_observer=ImportationObserver())


class DataFileReaderWithIdentifierTestCase(TestCase):
    test_data = "./test_data"

    def setUp(self):
        self.owner = get_user_model().objects.create(
            username="root",
        )

    def test_import_in_db(self):
        filename = os.path.join(self.test_data, "simple-with-id.xlsx")
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(models.Host.objects.get(data_source=data_source, name="C1").identifier, "HG738867.1")
        self.assertEqual(models.Host.objects.get(data_source=data_source, name="C2").identifier, "HGC2738867.3")
        self.assertEqual(models.Host.objects.get(data_source=data_source, name="C3").identifier, 'blablablabla')
        self.assertEqual(models.Host.objects.get(data_source=data_source, name="C4").identifier, "tralala")
        self.assertEqual(models.Virus.objects.get(data_source=data_source, name="r1").identifier, "NC_001416")
        self.assertEqual(models.Virus.objects.get(data_source=data_source, name="r3").identifier, 'blabla')
        self.assertRaises(
            models.Virus.MultipleObjectsReturned,
            models.Virus.objects.get
            , data_source=data_source, name="r2")
        va = models.Virus.objects.get(data_source=data_source, name="r2", identifier="NC_00141454566")
        vb = models.Virus.objects.get(data_source=data_source, name="r2", identifier="NC_00141454566 variant T")
        self.assertNotEqual(va, vb)
        self.assertEqual(models.Host.objects.filter(data_source=data_source).count(), 4)
        self.assertEqual(models.Virus.objects.filter(data_source=data_source).count(), 4)


class AutomatedMergeOnImportTestCase(TestCase):
    test_data = "./test_data"

    def setUp(self):
        self.owner = get_user_model().objects.create(
            username="root",
        )

    def test_import_in_db(self):
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        three_reponse_simple, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=three_reponse_simple,
            file=filename,
        )
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        three_reponse_simple_2, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=three_reponse_simple_2,
            file=filename,
        )

        self.assertEqual(models.Virus.objects.filter(name="r1").count(), 1)
        self.assertEqual(models.Virus.objects.filter(name="r2").count(), 1)
        self.assertEqual(models.Virus.objects.filter(name="r3").count(), 1)
        self.assertEqual(models.Virus.objects.filter().count(), 3)
        self.assertEqual(models.Host.objects.filter(name="C1").count(), 1)
        self.assertEqual(models.Host.objects.filter(name="C2").count(), 1)
        self.assertEqual(models.Host.objects.filter(name="C3").count(), 1)
        self.assertEqual(models.Host.objects.filter(name="C4").count(), 1)
        self.assertEqual(models.Host.objects.filter().count(), 4)

    def test_merge_considers_identifier(self):
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        three_reponse_simple, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=three_reponse_simple,
            file=filename,
        )
        filename = os.path.join(self.test_data, "simple-with-some-id.xlsx")
        simple_with_id, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_id,
            file=filename,
        )

        self.assertEqual(models.Virus.objects.filter(name="r1").count(), 2)
        self.assertEqual(models.Virus.objects.filter(name="r2").count(), 3)
        self.assertEqual(models.Virus.objects.filter(name="r3").count(), 1)
        self.assertEqual(models.Virus.objects.filter().count(), 6)
        self.assertEqual(models.Virus.objects.filter(name="r3", data_source=three_reponse_simple).count(), 1)
        self.assertEqual(models.Virus.objects.filter(name="r3", data_source=simple_with_id).count(), 1)
        self.assertEqual(models.Virus.objects.get(data_source=three_reponse_simple, name="r3").identifier, '')
        self.assertEqual(models.Virus.objects.get(data_source=simple_with_id, name="r3").identifier, '')
        self.assertEqual(models.Virus.objects.get(data_source=simple_with_id, name="r1").identifier, "NC_001416")
        self.assertEqual(models.Virus.objects.get(data_source=three_reponse_simple, name="r1").identifier, '')

        self.assertEqual(models.Host.objects.filter(name="C1").count(), 2)
        self.assertEqual(models.Host.objects.filter(name="C2").count(), 2)
        self.assertEqual(models.Host.objects.filter(name="C3").count(), 1)
        self.assertEqual(models.Host.objects.filter(name="C4").count(), 2)
        self.assertEqual(models.Host.objects.filter().count(), 7)

    def test_creation_handle_duplicate_in_db(self):
        models.Virus.objects.create(name="r1", identifier="")
        models.Virus.objects.create(name="r1", identifier="")
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        three_reponse_simple, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=three_reponse_simple,
            file=filename,
        )
        self.assertEqual(models.Virus.objects.filter(name="r1").count(), 2)


class ResetMappingTestCase(ViewTestCase):
    def test_reset_mapping_ok(self):
        self.assertTrue(self.four_reponse_simple.is_mapping_done)
        business_process.reset_mapping(models.DataSource.objects.filter(pk=self.four_reponse_simple.pk))
        self.assertFalse(self.four_reponse_simple.is_mapping_done)
        self.assertFalse(models.ViralHostResponseValueInDataSource.objects \
                         .filter(data_source=self.four_reponse_simple) \
                         .filter(~Q(response=self.not_mapped_yet)).exists())


class TestOthers(TestCase):
    def test_extract_name_and_identifier(self):
        expected_name = "A"
        expected_identifier = "i"
        for p in [
            "{} ({})",
            "{}({})",
            "   {} ({})",
            "   {}({})",
            "   {}({})   ",
            "   {}({})   ",
            "{} (  {})",
            "{}(  {})",
            "   {} (  {})",
            "   {}(  {})",
            "   {}(  {})   ",
            "   {}(  {})   ",
            "{} ({}  )",
            "{}({}  )",
            "   {} ({}  )",
            "   {}({}  )",
            "   {}({}  )   ",
            "   {}({}  )   ",
        ]:
            name, identifier = extract_name_and_identifier(p.format(expected_name, expected_identifier))
            self.assertEqual(name, expected_name)
            self.assertEqual(identifier, expected_identifier)
        for p in [
            "{}",
            "{}    ",
            "    {}    ",
            "    {}",
        ]:
            name, identifier = extract_name_and_identifier(p.format(expected_name))
            self.assertEqual(name, expected_name)
            self.assertEqual(identifier, "")
        for p in [
            "({})",
            "({})    ",
            "    ({})    ",
            "    ({})",
        ]:
            name, identifier = extract_name_and_identifier(p.format(expected_identifier))
            self.assertEqual(name, "")
            self.assertEqual(identifier, expected_identifier)
