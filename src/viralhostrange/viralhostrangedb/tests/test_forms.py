import os

from basetheme_bootstrap.user_preferences import get_user_preferences_for_user
from django.contrib.auth import get_user_model
from django.db.models import Min, Max
from django.db.models import Q
from django.http import QueryDict
from django.test import TestCase
from django.urls import reverse

from viralhostrangedb import forms
from viralhostrangedb import models, business_process
from viralhostrangedb.tests.test_views_others import ViewTestCase


class RangeMappingFormTestCase(ViewTestCase):

    def setUp(self):
        super().setUp()

        ################################################################################
        self.two_response_simple = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="two_response_simple user",
            raw_name="two_response_simple.xlsx",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "two_response_simple.xlsx")
        business_process.import_file(
            data_source=self.two_response_simple,
            file=filename,
        )

        ################################################################################
        self.one_response = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="one_response user",
            raw_name="one_response.xlsx",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "one_response.xlsx")
        business_process.import_file(
            data_source=self.one_response,
            file=filename,
        )

    def test_find_thresholds(self):
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple)
        self.assertNotIn('mapping_for_%i_starts_with', form.fields)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, self.no_lysis.value)
        self.assertLess(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, self.lysis.value)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial, self.weak.value)

    def test_works_when_empty(self):
        self.four_reponse_simple.responseindatasource.all().delete()
        forms.RangeMappingForm(data_source=self.four_reponse_simple)

    def test_ok_when_only_no_lysis(self):
        models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .update(response=self.no_lysis)
        min_raw, max_raw = models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .aggregate(Min('raw_response'), Max('raw_response')).values()
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, max_raw)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial, max_raw)

        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertFalse(models.ViralHostResponseValueInDataSource.objects \
                         .filter(data_source=self.four_reponse_simple_not_mapped) \
                         .filter(~Q(response=self.no_lysis)).exists())

    def test_ok_when_only_weak(self):
        models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .update(response=self.weak)
        min_raw, max_raw = models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .aggregate(Min('raw_response'), Max('raw_response')).values()
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped)
        self.assertLessEqual(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, min_raw)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial, max_raw)

        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertFalse(models.ViralHostResponseValueInDataSource.objects \
                         .filter(data_source=self.four_reponse_simple_not_mapped) \
                         .filter(~Q(response=self.weak)).exists())

    def test_ok_when_only_lysis(self):
        models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .update(response=self.lysis)
        min_raw, max_raw = models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .aggregate(Min('raw_response'), Max('raw_response')).values()
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped)
        self.assertLessEqual(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, min_raw)
        self.assertLessEqual(form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial, min_raw)

        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertFalse(models.ViralHostResponseValueInDataSource.objects \
                         .filter(data_source=self.four_reponse_simple_not_mapped) \
                         .filter(~Q(response=self.lysis)).exists())

    def test_range_is_respected(self):
        self.actual_test_range_is_respected(3, 8, self.private_data_source_of_user)
        self.actual_test_range_is_respected(1, 3.1, self.four_reponse_simple_not_mapped)

    def actual_test_range_is_respected(self, min_for_weak, min_for_lysis, ds):
        form = self.build_form(min_for_weak, min_for_lysis, ds)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertSetEqual(
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(raw_response__lt=min_for_weak, data_source=ds) \
                .values_list('pk', flat=True))
            ,
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(response=self.no_lysis, data_source=ds) \
                .values_list('pk', flat=True))
        )
        self.assertSetEqual(
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(raw_response__gte=min_for_weak, raw_response__lt=min_for_lysis, data_source=ds) \
                .values_list('pk', flat=True))
            ,
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(response=self.weak, data_source=ds) \
                .values_list('pk', flat=True))
        )
        self.assertSetEqual(
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(raw_response__gte=min_for_lysis, data_source=ds) \
                .values_list('pk', flat=True))
            ,
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(response=self.lysis, data_source=ds) \
                .values_list('pk', flat=True))
        )

    def build_form(self, min_for_weak, min_for_lysis, ds):
        data = {
            'mapping_for_%i_starts_with' % self.weak.pk: min_for_weak,
            'mapping_for_%i_starts_with' % self.lysis.pk: min_for_lysis
        }
        form = forms.RangeMappingForm(
            data_source=ds,
            data=data
        )
        return form

    def test_ko_with_missing_threshold(self):
        for a, b in [('', '1'), ('1', ''), ('', '')]:
            self.assertFalse(self.build_form(a, b, self.four_reponse_simple_not_mapped).is_valid())

    def test_ok_with_equal(self):
        for a, b in [('1', '1'), ('1', '1.0'), ('1.0', '1'), ('1.0', '1.0')]:
            self.assertTrue(self.build_form(a, b, self.four_reponse_simple_not_mapped).is_valid())

    def test_ok_with_int_and_float(self):
        for a, b in [('1', '2'), ('1.0', '2'), ('1', '2.0'), ('1.0', '2.0')]:
            self.assertTrue(self.build_form(a, b, self.four_reponse_simple_not_mapped).is_valid())

    def test_ok_when_new_value_inserted_in_existing_range(self):
        for v in [4, 1]:
            changed = models.ViralHostResponseValueInDataSource.objects \
                .filter(data_source=self.four_reponse_simple) \
                .filter(raw_response=v).first()
            changed.raw_response = v - 0.1
            changed.response = models.GlobalViralHostResponseValue.get_not_mapped_yet()
            changed.save()
            self.four_reponse_simple.refresh_from_db()
            self.assertFalse(self.four_reponse_simple.is_mapping_done)
            self.four_reponse_simple.refresh_from_db()
            form = forms.RangeMappingForm(data_source=self.four_reponse_simple)
            data = {key: form.fields[key].initial for key in form.fields.keys()}
            form = forms.RangeMappingForm(data_source=self.four_reponse_simple, data=data)
            self.assertTrue(form.is_valid())
            form.save()
            self.four_reponse_simple.refresh_from_db()
            self.assertTrue(self.four_reponse_simple.is_mapping_done)

    def test_two_response_simple(self):
        ds = self.two_response_simple
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 2,
            "mapping_for_%i_starts_with" % self.lysis.pk: 2,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertGreater(weak_start, 0)
        self.assertGreater(lysis_start, 0)

    def test_two_response_simple_2(self):
        ds = self.two_response_simple
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 2,
            "mapping_for_%i_starts_with" % self.lysis.pk: 3,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertGreater(weak_start, 0, msg="0s are not associated with weak")
        self.assertLessEqual(weak_start, 2, msg="2s are not associated with weak")
        self.assertGreater(lysis_start, 2, msg="2s are not associated with weak")

    def test_one_response_simple_to_no_lysis(self):
        ds = self.one_response
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 1,
            "mapping_for_%i_starts_with" % self.lysis.pk: 1,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertGreater(weak_start, 0)
        self.assertGreater(lysis_start, 0)

    def test_one_response_simple_to_weak(self):
        ds = self.one_response
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 0,
            "mapping_for_%i_starts_with" % self.lysis.pk: 1,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertLessEqual(weak_start, 0)
        self.assertGreater(lysis_start, 0)

    def test_one_response_simple_to_lysis(self):
        ds = self.one_response
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 0,
            "mapping_for_%i_starts_with" % self.lysis.pk: 0,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertLessEqual(weak_start, 0)
        self.assertLessEqual(lysis_start, 0)


class BrowseFormTestCase(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create(
            username="user",
            email="user@a.a",
        )
        self.user.save()
        p = get_user_preferences_for_user(self.user)
        p.virus_infection_ratio = True
        p.save()
        p = get_user_preferences_for_user(None)
        p.host_infection_ratio = True
        p.save()

    def test_get_pref(self):
        form = forms.BrowseForm(
            user=None,
        )
        self.assertTrue(form.initial["host_infection_ratio"])
        self.assertFalse(form.initial["virus_infection_ratio"])

    def test_avoid_pref(self):
        form = forms.BrowseForm(
            user=None,
            data=QueryDict(
                "&use_pref=False"
            )
        )
        self.assertTrue(form.is_valid())
        self.assertFalse(form.cleaned_data["host_infection_ratio"])
        self.assertFalse(form.cleaned_data["virus_infection_ratio"])

    def test_get_pref_for_user(self):
        form = forms.BrowseForm(
            user=self.user,
        )
        self.assertFalse(form.initial["host_infection_ratio"])
        self.assertTrue(form.initial["virus_infection_ratio"])

    def test_avoid_pref_for_user(self):
        form = forms.BrowseForm(
            user=self.user,
            data=QueryDict(
                "&use_pref=False"
            )
        )
        self.assertTrue(form.is_valid())
        self.assertFalse(form.cleaned_data["host_infection_ratio"])
        self.assertFalse(form.cleaned_data["virus_infection_ratio"])
