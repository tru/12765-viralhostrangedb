from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory

from viralhostrangedb import mixins, models
from viralhostrangedb.tests import test_models_methods


class DataSourceDownloadTestCase(test_models_methods.ModelsTestCaseMixin):

    def test_visibility_only_public_or_owned_or_staff_queryset_filter(self):
        my_set = set([o.pk for o in mixins.only_public_or_owned_or_staff_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_visibility_only_public_or_owned_or_staff_queryset_filter_req(self):
        request = RequestFactory().get('/blabla')
        request.user = AnonymousUser()
        my_set = set([o.pk for o in mixins.only_public_or_owned_or_staff_queryset_filter(
            self=None,
            request=request,
            queryset=models.DataSource.objects.all(),
            user=None
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_visibility_only_public_or_owned_or_staff_queryset_filter_admin_user(self):
        self.toto.is_staff = True
        self.toto.save()
        my_set = set([o.pk for o in mixins.only_public_or_owned_or_staff_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_unicity_ok_only_public_or_owned_or_staff_queryset_filter(self):
        my_set = set()
        for ds in mixins.only_public_or_owned_or_staff_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.titi
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_unicity_ok_only_public_or_owned_or_staff_queryset_filter_2(self):
        my_set = set()
        for ds in mixins.only_public_or_owned_or_staff_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.toto
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_visibility_only_public_or_granted_or_owned_queryset_filter(self):
        my_set = set([o.pk for o in mixins.only_public_or_granted_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_unicity_ok_only_public_or_granted_or_owned_queryset_filter(self):
        my_set = set()
        for ds in mixins.only_public_or_granted_or_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.titi
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_unicity_ok_only_public_or_granted_or_owned_queryset_filter_2(self):
        my_set = set()
        for ds in mixins.only_public_or_granted_or_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.toto
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_visibility_only_owned_queryset_filter(self):
        my_set = set([o.pk for o in mixins.only_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertNotIn(self.public_data_source_of_user.pk, my_set)
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_visibility_only_owned_queryset_filter_from_virus(self):
        my_set = set([o.pk for o in mixins.only_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.toto,
            path_to_data_source="data_source__",
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertNotIn(self.public_data_source_of_user.pk, my_set)
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_unicity_ok_only_owned_queryset_filter(self):
        my_set = set()
        for ds in mixins.only_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.titi
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_unicity_ok_only_owned_queryset_filter_2(self):
        my_set = set()
        for ds in mixins.only_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.user
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_visibility_only_public_or_owned_queryset_filter(self):
        my_set = set([o.pk for o in mixins.only_public_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_visibility_only_public_or_owned_queryset_filter_req(self):
        request = RequestFactory().get('/blabla')
        request.user = AnonymousUser()
        my_set = set([o.pk for o in mixins.only_public_or_owned_queryset_filter(
            self=None,
            request=request,
            queryset=models.DataSource.objects.all(),
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_unicity_ok_only_public_or_owned_queryset_filter(self):
        my_set = set()
        for ds in mixins.only_public_or_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.titi
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_unicity_ok_only_public_or_owned_queryset_filter_2(self):
        my_set = set()
        for ds in mixins.only_public_or_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.toto
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)
