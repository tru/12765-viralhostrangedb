from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.utils import timezone

from viralhostrangedb import models
from viralhostrangedb.templatetags.viralhostrange_tags import can_edit, can_see


class TemplateTagsTests(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create(
            username="user",
            email="b@a.a",
        )
        self.toto = get_user_model().objects.create(
            username="toto",
            email="toto@a.a",
        )
        self.d_user = models.DataSource.objects.create(
            owner=self.user,
            name="d_user",
            raw_name="d_user",
            creation_date=timezone.now(),
        )
        self.d_toto = models.DataSource.objects.create(
            owner=self.toto,
            name="d_toto",
            raw_name="d_toto",
            creation_date=timezone.now(),
        )

    @override_settings(DEBUG=True)
    def test_can_edit_wrong_param_debug(self):
        self.assertRaises(Exception, can_edit, self.user, self.user)

    @override_settings(DEBUG=False)
    def test_can_edit_wrong_param(self):
        self.assertFalse(can_edit(self.user, self.user))

    @override_settings(DEBUG=False)
    def test_can_edit(self):
        self.assertFalse(can_edit(self.user, self.d_toto))
        self.assertFalse(can_edit(self.toto, self.d_user))
        self.assertTrue(can_edit(self.toto, self.d_toto))
        self.assertTrue(can_edit(self.user, self.d_user))

    @override_settings(DEBUG=True)
    def test_can_see_wrong_param_debug(self):
        self.assertRaises(Exception, can_see, self.user, self.user)
        self.assertRaises(NotImplementedError, can_see, self.user, get_user_model().objects.all())

    @override_settings(DEBUG=False)
    def test_can_see_wrong_param(self):
        self.assertFalse(can_see(self.user, self.user))

    @override_settings(DEBUG=False)
    def test_can_see(self):
        self.assertFalse(can_see(self.user, self.d_toto))
        self.assertFalse(can_see(self.toto, self.d_user))
        self.assertTrue(can_see(self.toto, self.d_toto))
        self.assertTrue(can_see(self.user, self.d_user))
