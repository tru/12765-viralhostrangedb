import itertools
import json
from unittest import TestCase

from django.contrib.auth.models import AnonymousUser
from django.urls import reverse

from viralhostrangedb import models, mixins
from viralhostrangedb.tests.test_views_others import ViewTestCase
from viralhostrangedb.views_api import MyAPIView, to_int_array


class ResponseAggregatedTestCase(ViewTestCase):
    def test_works_no_auth(self):
        url = reverse('viralhostrangedb-api:aggregated-responses') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        url = reverse('viralhostrangedb-api:aggregated-responses') \
              + "?format=json&ds=%i" % self.private_data_source_of_toto.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

    def test_works_auth(self):
        self.client.force_login(self.toto)
        url = reverse('viralhostrangedb-api:aggregated-responses') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        url = reverse('viralhostrangedb-api:aggregated-responses') \
              + "?format=json&ds=%i" % self.private_data_source_of_toto.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        viruses = ",".join(str(i) for i in self.private_data_source_of_toto.virus_set.values_list('pk', flat=True))
        hosts = ",".join(str(i) for i in self.private_data_source_of_toto.host_set.values_list('pk', flat=True))
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:aggregated-responses'),
            viruses=viruses,
            hosts=hosts,
            ds=str(self.private_data_source_of_toto.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

    def test_works_with_mapped_data_source(self):
        url = reverse('viralhostrangedb-api:aggregated-responses') \
              + "?format=json&ds=%i" % self.public_data_source_of_user_mapped.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection").value
        no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection").value
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis},
                str(self.C3.pk): {'diff': 1, 'val': no_lysis},
                str(self.C4.pk): {'diff': 1, 'val': lysis}},
            str(self.r2.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis},
                str(self.C3.pk): {'diff': 1, 'val': lysis},
                str(self.C4.pk): {'diff': 1, 'val': no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': lysis},
                str(self.C3.pk): {'diff': 1, 'val': no_lysis},
                str(self.C4.pk): {'diff': 1, 'val': no_lysis}}
        })

        viruses = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.virus_set.values_list('pk', flat=True))
        hosts = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.host_set.values_list('pk', flat=True))
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:aggregated-responses'),
            viruses=viruses,
            hosts=hosts,
            ds=str(self.public_data_source_of_user_mapped.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis},
                str(self.C3.pk): {'diff': 1, 'val': no_lysis},
                str(self.C4.pk): {'diff': 1, 'val': lysis}},
            str(self.r2.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis},
                str(self.C3.pk): {'diff': 1, 'val': lysis},
                str(self.C4.pk): {'diff': 1, 'val': no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': lysis},
                str(self.C3.pk): {'diff': 1, 'val': no_lysis},
                str(self.C4.pk): {'diff': 1, 'val': no_lysis}}
        })

        viruses = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.virus_set.values_list('pk', flat=True)[1:])
        hosts = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.host_set.values_list('pk', flat=True)[:2])
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:aggregated-responses'),
            viruses=viruses,
            hosts=hosts,
            ds=str(self.public_data_source_of_user_mapped.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r2.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': lysis}}
        })

    def test_works_when_filtering_on_only_published_data(self):
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        pk_host = str(self.public_data_source_of_user_mapped.host_set.first().pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&only_published_data=on&host=%s" % \
              (pk_ds, pk_host)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
        })

        self.public_data_source_of_user_mapped.publication_url = "http://a.a"
        self.public_data_source_of_user_mapped.save()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {pk_host: {pk_ds: self.no_lysis.value}},
            str(self.r2.pk): {pk_host: {pk_ds: self.no_lysis.value}},
            str(self.r3.pk): {pk_host: {pk_ds: self.no_lysis.value}},
        })

    def test_works_when_filtering_on_only_virus_ncbi_id(self):
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        pk_host = str(self.public_data_source_of_user_mapped.host_set.first().pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&only_virus_ncbi_id=on&host=%s" % \
              (pk_ds, pk_host)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
        })

        self.r1.identifier = "r"
        self.r1.save()
        self.r3.identifier = "azeaze"
        self.r3.save()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {pk_host: {pk_ds: self.no_lysis.value}},
            str(self.r3.pk): {pk_host: {pk_ds: self.no_lysis.value}},
        })

    def test_works_when_filtering_on_only_host_ncbi_id(self):
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&only_host_ncbi_id=on&virus=%i" % \
              (pk_ds, self.r1.pk)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
        })

        self.C1.identifier = "r"
        self.C1.save()
        self.C4.identifier = "azeaze"
        self.C4.save()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {pk_ds: self.no_lysis.value},
                str(self.C4.pk): {pk_ds: self.lysis.value},
            },
        })

    def test_works_when_filtering_life_domain(self):
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        virus_count = self.public_data_source_of_user_mapped.virus_set.count()
        pk_host = str(self.public_data_source_of_user_mapped.host_set.first().pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&host=%s" % \
              (pk_ds, pk_host)
        response = self.client.get(url + "&life_domain=bacteria")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(str(response.content, encoding='utf8'))), virus_count)

        self.public_data_source_of_user_mapped.life_domain = "archaea"
        self.public_data_source_of_user_mapped.save()

        response = self.client.get(url + "&life_domain=bacteria")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(str(response.content, encoding='utf8'))), 0)

        response = self.client.get(url + "&life_domain=")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(str(response.content, encoding='utf8'))), virus_count)


class CompleteResponseTestCase(ViewTestCase):
    def test_works_no_auth(self):
        url = reverse('viralhostrangedb-api:responses') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        url = reverse('viralhostrangedb-api:responses') \
              + "?format=json&ds=%i" % self.private_data_source_of_toto.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

    def test_works_auth(self):
        self.client.force_login(self.toto)
        url = reverse('viralhostrangedb-api:responses') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        url = reverse('viralhostrangedb-api:responses') \
              + "?format=json&ds=%i" % self.private_data_source_of_toto.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        viruses = ",".join(str(i) for i in self.private_data_source_of_toto.virus_set.values_list('pk', flat=True))
        hosts = ",".join(str(i) for i in self.private_data_source_of_toto.host_set.values_list('pk', flat=True))
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:responses'),
            viruses=viruses,
            hosts=hosts,
            ds=str(self.private_data_source_of_toto.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

    def test_works_with_mapped_data_source(self):
        pk = str(self.public_data_source_of_user_mapped.pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s" % pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection").value
        no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection").value
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis},
                str(self.C3.pk): {pk: no_lysis},
                str(self.C4.pk): {pk: lysis}},
            str(self.r2.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis},
                str(self.C3.pk): {pk: lysis},
                str(self.C4.pk): {pk: no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: lysis},
                str(self.C3.pk): {pk: no_lysis},
                str(self.C4.pk): {pk: no_lysis}}
        })

        viruses = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.virus_set.values_list('pk', flat=True))
        hosts = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.host_set.values_list('pk', flat=True))
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:responses'),
            viruses=viruses,
            hosts=hosts,
            ds=pk,
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        # print(json.dumps(json.loads(str(response.content, encoding='utf8')), indent=4))
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis},
                str(self.C3.pk): {pk: no_lysis},
                str(self.C4.pk): {pk: lysis}},
            str(self.r2.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis},
                str(self.C3.pk): {pk: lysis},
                str(self.C4.pk): {pk: no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: lysis},
                str(self.C3.pk): {pk: no_lysis},
                str(self.C4.pk): {pk: no_lysis}}
        })

        viruses = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.virus_set.values_list('pk', flat=True)[1:])
        hosts = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.host_set.values_list('pk', flat=True)[:2])
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:responses'),
            viruses=viruses,
            hosts=hosts,
            ds=pk,
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        # print(json.dumps(json.loads(str(response.content, encoding='utf8')), indent=4))
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r2.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: lysis}}
        })

    def test_works_when_filtering_on_host(self):
        # lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection").value
        no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection").value
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        pk_host = str(self.public_data_source_of_user_mapped.host_set.first().pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&host=%s" % (pk_ds, pk_host)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {pk_host: {pk_ds: no_lysis}},
            str(self.r2.pk): {pk_host: {pk_ds: no_lysis}},
            str(self.r3.pk): {pk_host: {pk_ds: no_lysis}},
        })


class DataSourceListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url + "?pks=1,2")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url + "?pk=1,2")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url + "?ids=1,2")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url + "?id=1,2")
        self.assertEqual(response.status_code, 200)

    def test_filter_only_published_data(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.user)

        self.public_data_source_of_user.publication_url = "http://a.com"
        self.public_data_source_of_user.save()
        self.public_data_source_of_user_mapped.publication_url = "http://a.com"
        self.public_data_source_of_user_mapped.save()
        self.private_data_source_of_user.publication_url = "http://a.com"
        self.private_data_source_of_user.save()

        response = self.client.get(url + "?only_published_data=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, len(ret))

    def test_filter_life_domain(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.user)

        response = self.client.get(url + "?life_domain=bacteria")
        ret = json.loads(str(response.content, encoding='utf8'))
        bacteria_count = len(ret)

        self.public_data_source_of_user.life_domain = "eukaryote"
        self.public_data_source_of_user.save()
        self.public_data_source_of_user_mapped.life_domain = "eukaryote"
        self.public_data_source_of_user_mapped.save()
        self.private_data_source_of_user.life_domain = "eukaryote"
        self.private_data_source_of_user.save()

        response = self.client.get(url + "?life_domain=eukaryote")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, len(ret))

        response = self.client.get(url + "?life_domain=bacteria")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, bacteria_count - len(ret))

    def test_filter_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.toto)

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        v.identifier = "ee"
        v.save()

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(2, len(ret))

    def test_filter_only_host_ncbi_id(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.toto)

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        v.identifier = "ee"
        v.save()

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(2, len(ret))

    def test_filter_only_host_ncbi_id_and_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.toto)

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        v.identifier = "ee"
        v.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        v.identifier = "ee"
        v.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(2, len(ret))

    def test_owner(self):
        url = reverse('viralhostrangedb-api:datasource-list') + "?sample_size=1000&owner=%i" % self.user.pk
        qs_data_source = models.DataSource.objects.filter(owner__pk=self.user.pk)
        self.client.logout()

        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(mixins.only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=AnonymousUser(),
            queryset=qs_data_source,
        ).count(), len(ret))

        self.client.force_login(self.toto)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(mixins.only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=self.toto,
            queryset=qs_data_source,
        ).count(), len(ret))


class VirusListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb-api:virus-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_sublist(self):
        pk1 = str(self.public_data_source_of_user_mapped.pk)
        pk2 = str(self.public_data_source_of_user.pk)
        virus_pks = set(models.Virus.objects.filter(data_source__pk__in=[pk1, pk2]).values_list("id", flat=True))
        url = reverse('viralhostrangedb-api:virus-list') + "?ds=" + pk1 + "&ds=" + pk2
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        virus_pks_api = set([d["id"] for d in ret])
        self.assertSetEqual(virus_pks, virus_pks_api)
        url = reverse('viralhostrangedb-api:virus-list') + "?ds=" + pk1 + "," + pk2
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        virus_pks_api = set([d["id"] for d in ret])
        self.assertSetEqual(virus_pks, virus_pks_api)

    def test_fields_works(self):
        url = reverse('viralhostrangedb-api:virus-list') + "?fields=id"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {"id"})
        url = reverse('viralhostrangedb-api:virus-list') + "?fields=id,name"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {"id", "name"})

    def test_filter_only_published_data(self):
        url = reverse('viralhostrangedb-api:virus-list')
        self.client.force_login(self.user)

        response = self.client.get(url + "?only_published_data=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        self.public_data_source_of_user.publication_url = "http://a.com"
        self.public_data_source_of_user.save()

        response = self.client.get(url + "?only_published_data=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, len(ret))

    def test_filter_life_domain(self):
        url = reverse('viralhostrangedb-api:virus-list')
        self.client.force_login(self.user)

        response = self.client.get(url + "?life_domain=eukaryote")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        self.public_data_source_of_user.life_domain = "eukaryote"
        self.public_data_source_of_user.save()

        response = self.client.get(url + "?life_domain=eukaryote")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, len(ret))

    def test_filter_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:virus-list')
        self.client.force_login(self.toto)
        self.public_data_source_with_ids_of_toto.owner = self.user
        self.public_data_source_with_ids_of_toto.public = False
        self.public_data_source_with_ids_of_toto.save()

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        v.identifier = "ee"
        v.save()

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_filter_only_host_ncbi_id_and_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:virus-list')
        self.client.force_login(self.toto)
        self.public_data_source_with_ids_of_toto.owner = self.user
        self.public_data_source_with_ids_of_toto.public = False
        self.public_data_source_with_ids_of_toto.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        o.identifier = "ee"
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        o.identifier = "ee"
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_owner(self):
        url = reverse('viralhostrangedb-api:virus-list') + "?sample_size=1000&owner=%i" % self.user.pk
        qs_virus = models.Virus.objects.filter(data_source__owner__pk=self.user.pk)
        self.client.logout()

        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(mixins.only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=AnonymousUser(),
            queryset=qs_virus,
            path_to_data_source="data_source__",
        ).count(), len(ret))

        self.client.force_login(self.toto)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(mixins.only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=self.toto,
            queryset=qs_virus,
            path_to_data_source="data_source__",
        ).count(), len(ret))


class HostListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb-api:host-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_sublist(self):
        pk1 = str(self.public_data_source_of_user_mapped.pk)
        pk2 = str(self.public_data_source_of_user.pk)
        host_pks = set(models.Host.objects.filter(data_source__pk__in=[pk1, pk2]).values_list("id", flat=True))
        url = reverse('viralhostrangedb-api:host-list') + "?ds=" + pk1 + "&ds=" + pk2
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        host_pks_api = set([d["id"] for d in ret])
        self.assertSetEqual(host_pks, host_pks_api)
        url = reverse('viralhostrangedb-api:host-list') + "?ds=" + pk1 + "," + pk2
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        host_pks_api = set([d["id"] for d in ret])
        self.assertSetEqual(host_pks, host_pks_api)

    def test_fields_works(self):
        url = reverse('viralhostrangedb-api:host-list') + "?fields=id"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {"id"})
        url = reverse('viralhostrangedb-api:host-list') + "?fields=id,name"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {"id", "name"})

    def test_filter_only_host_ncbi_id(self):
        url = reverse('viralhostrangedb-api:host-list')
        self.client.force_login(self.toto)
        self.public_data_source_with_ids_of_toto.owner = self.user
        self.public_data_source_with_ids_of_toto.public = False
        self.public_data_source_with_ids_of_toto.save()

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        o.identifier = "ee"
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))


class DetailViewTestCase(ViewTestCase):
    def test_host_works_no_auth(self):
        o = models.Host.objects.first()
        url = reverse('viralhostrangedb-api:host-detail', args=[o.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'id': o.pk, 'identifier': o.identifier, 'name': o.name},
        )

    def test_virus_works_no_auth(self):
        o = models.Virus.objects.first()
        url = reverse('viralhostrangedb-api:virus-detail', args=[o.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'id': o.pk, 'identifier': o.identifier, 'name': o.name},
        )

    def test_data_source_works_no_auth(self):
        url = reverse('viralhostrangedb-api:datasource-detail', args=[self.public_data_source_with_ids_of_toto.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        resp = json.loads(str(response.content, encoding='utf8'))
        del resp['creation_date']
        del resp['last_edition_date']
        self.assertJSONEqual(
            json.dumps(resp),
            dict(
                description=self.public_data_source_with_ids_of_toto.description,
                id=self.public_data_source_with_ids_of_toto.id,
                name=self.public_data_source_with_ids_of_toto.name,
                public=self.public_data_source_with_ids_of_toto.public,
            ),
        )


class DummyTest(TestCase):
    def test_dummy(self):
        self.assertRaises(NotImplementedError, MyAPIView().get, None)


class InfectionRatioTestCase(ViewTestCase):
    def test_wrong_slug(self):
        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-list', args=["toto"]) + "?virus=%i" % self.r1.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

    def test_works__and_focus_list_or_detail_idempotent(self):
        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "virus",
            self.r1.pk
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        resp_1 = json.loads(str(response.content, encoding='utf8'))

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-list', args=["virus"]) + "?virus=%i" % self.r1.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        resp_2 = json.loads(str(response.content, encoding='utf8'))
        self.assertJSONEqual(
            json.dumps(resp_1),
            json.dumps(resp_2),
            msg="The ratio when focusing on a virus (resp_1) should be the same as "
                "the ratios when filtering on this virus (resp_2)"
        )

    def test_ratio_r1(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])
        x = self.r1

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "virus",
            x.pk
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.75, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.5, 'total': 4}}),
        )

    def test_ratio_r2(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])
        x = self.r2

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "virus",
            x.pk
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.5, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )

    def test_ratio_r3(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])
        x = self.r3

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "virus",
            x.pk
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.5, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )

    def test_ratio_C1(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])
        x = self.C1

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "host",
            x.pk
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 3}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 1.0 / 3, 'total': 3}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 3}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 3}}),
        )

    def test_ratio_C1_to_C4(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-list', args=[
            "host"
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({
                self.C1.pk: {
                    "ratio": 0.0,
                    "total": 3
                },
                self.C2.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C3.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C4.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                }
            }),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({
                self.C1.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C2.pk: {
                    "ratio": 1.0,
                    "total": 3
                },
                self.C3.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C4.pk: {
                    "ratio": 2.0 / 3,
                    "total": 3
                }
            }),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w + a).content, encoding='utf8'))),
            json.dumps({
                self.C1.pk: {
                    "ratio": 0.0,
                    "total": 3
                },
                self.C2.pk: {
                    "ratio": 2.0 / 3,
                    "total": 3
                },
                self.C3.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C4.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                }
            }),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({
                self.C1.pk: {
                    "ratio": 0.0,
                    "total": 3
                },
                self.C2.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C3.pk: {
                    "ratio": 0,
                    "total": 3
                },
                self.C4.pk: {
                    "ratio": 0,
                    "total": 3
                }
            }),
        )


class SearchTestCase(ViewTestCase):
    def test_ko(self):
        url = reverse('viralhostrangedb-api:search')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn("err", json.loads(response.content, encoding='utf8'))

    def test_works_no_auth(self):
        url = reverse('viralhostrangedb-api:search') + "?search=privateprivateprivate"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content, encoding='utf8')
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)
        self.assertEqual(response["data_source"]["count"], 0)

    def test_works_auth(self):
        url = reverse('viralhostrangedb-api:search') + "?search=private%20user"
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content, encoding='utf8')
        self.assertGreater(response["data_source"]["count"], 0)

    def test_works_ui_help(self):
        url = reverse('viralhostrangedb-api:search') + "?search=private%20user&ui_help=True"
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content, encoding='utf8')
        self.assertGreater(response["data_source"]["count"], 0)

    def test_privacy_cross_auth(self):
        url = reverse('viralhostrangedb-api:search') + "?search=private%20user"
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content, encoding='utf8')
        self.assertEqual(response["data_source"]["count"], 0)

    def test_advanced(self):
        ds = self.private_data_source_of_toto_with_no_virus_or_host_shared
        url = reverse('viralhostrangedb-api:search') + "?search=" + ds.name
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content, encoding='utf8')
        self.assertEqual(response["data_source"]["count"], 1)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)
        self.client.logout()
        response = json.loads(self.client.get(url).content, encoding='utf8')
        self.assertEqual(response["data_source"]["count"], 0)

        url = reverse('viralhostrangedb-api:search') + "?search=" + ds.virus_set.first().name
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content, encoding='utf8')
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 1)
        self.assertEqual(response["host"]["count"], 0)
        self.client.logout()
        response = json.loads(self.client.get(url).content, encoding='utf8')
        self.assertEqual(response["virus"]["count"], 0)

        url = reverse('viralhostrangedb-api:search') + "?search=" + ds.host_set.first().name
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content, encoding='utf8')
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 1)
        self.client.logout()
        response = json.loads(self.client.get(url).content, encoding='utf8')
        self.assertEqual(response["host"]["count"], 0)


class OtherTests(TestCase):
    def test_functions(self):
        self.assertEqual(list(to_int_array(["e", "1", "2", "1", "1"])), [1, 2, 1, 1])
