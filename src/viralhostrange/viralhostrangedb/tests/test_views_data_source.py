import logging
import os
from tempfile import NamedTemporaryFile

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Count, Q
from django.urls import reverse

from viralhostrangedb import models, business_process
from viralhostrangedb.tests.test_views_others import ViewTestCase

logger = logging.getLogger(__name__)


class DataSourceMappingPendingListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-pending-mapping-list')
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class DataSourceListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-list')
        self.client.logout()
        self.assertEqual(self.client.get(url).status_code, 200)
        self.client.force_login(self.toto)
        self.assertEqual(self.client.get(url).status_code, 200)

    def test_private_selectively_shawn(self):
        url = reverse('viralhostrangedb:data-source-list')

        self.client.logout()
        response = self.client.get(url)
        link = self.private_data_source_of_toto.get_absolute_url()
        self.assertFalse(link in str(response.content))
        link = self.private_data_source_of_user.get_absolute_url()
        self.assertFalse(link in str(response.content))
        link = self.public_data_source_of_user.get_absolute_url()
        self.assertTrue(link in str(response.content))

        self.client.force_login(self.toto)
        response = self.client.get(url)
        link = self.private_data_source_of_toto.get_absolute_url()
        self.assertTrue(link in str(response.content))
        link = self.private_data_source_of_user.get_absolute_url()
        self.assertFalse(link in str(response.content))
        link = self.public_data_source_of_user.get_absolute_url()
        self.assertTrue(link in str(response.content))

        self.client.force_login(self.user)
        response = self.client.get(url)
        link = self.private_data_source_of_toto.get_absolute_url()
        self.assertFalse(link in str(response.content))
        link = self.private_data_source_of_user.get_absolute_url()
        self.assertTrue(link in str(response.content))
        link = self.public_data_source_of_user.get_absolute_url()
        self.assertTrue(link in str(response.content))


class DataSourceDeleteViewTestCase(ViewTestCase):

    def test_works(self):
        url = reverse('viralhostrangedb:data-source-delete', args=[self.private_data_source_of_user.pk])
        host_pk = list(self.private_data_source_of_user.host_set.values_list('pk', flat=True))
        virus_pk = list(self.private_data_source_of_user.virus_set.values_list('pk', flat=True))
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 302)
        # check that the object is not here anymore
        self.assertFalse(models.DataSource.objects.filter(pk=self.private_data_source_of_user.pk).exists())
        # check that virus only used by it are removed
        self.assertEqual(
            models.Virus.objects
                .filter(pk__in=virus_pk)
                .annotate(cpt=Count('data_source'))
                .filter(cpt=0)
                .count(),
            0,
        )
        # check that host only used by it are removed
        self.assertEqual(
            models.Host.objects
                .filter(pk__in=host_pk)
                .annotate(cpt=Count('data_source'))
                .filter(cpt=0)
                .count(),
            0,
        )

    def test_cross_user_block(self):
        url = reverse('viralhostrangedb:data-source-delete', args=[self.private_data_source_of_user.pk])
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class DataSourceUpdateViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.private_data_source_of_user.pk])
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_cross_users(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.private_data_source_of_user.pk])
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_staff_can_not_edit_private(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.private_data_source_of_toto.pk])
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_update_work(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)

        former_name = self.public_data_source_of_user.name
        allowed_users = set([u.pk for u in self.public_data_source_of_user.allowed_users.all()])

        form_data = dict(
            name=former_name + former_name,
            life_domain="bacteria",
            description="",
            new_allowed_users="",
            allowed_users=",".join([str(u.pk) for u in self.public_data_source_of_user.allowed_users.all()]),
        )

        response = self.client.post(url, form_data)
        self.assertRedirects(
            response,
            expected_url=reverse('viralhostrangedb:data-source-detail', args=[self.public_data_source_of_user.pk])
        )

        self.assertEqual(
            models.DataSource.objects.get(pk=self.public_data_source_of_user.pk).name,
            former_name + former_name
        )
        self.assertSetEqual(allowed_users, set([u.pk for u in self.public_data_source_of_user.allowed_users.all()]))

    def test_new_user_granted_by_email(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)

        allowed_users = set([u.pk for u in self.public_data_source_of_user.allowed_users.all()])

        form_data = dict(
            name=self.public_data_source_of_user.name,
            life_domain="bacteria",
            description="",
            new_allowed_users="a@a.a",
            allowed_users=",".join([str(u.pk) for u in self.public_data_source_of_user.allowed_users.all()]),
        )
        allowed_users.add(self.toto.pk)

        self.client.post(url, form_data)
        self.assertSetEqual(allowed_users, set([u.pk for u in self.public_data_source_of_user.allowed_users.all()]))

    def test_new_user_granted_by_name(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)

        allowed_users = set([u.pk for u in self.public_data_source_of_user.allowed_users.all()])
        candidate_1 = get_user_model().objects.create(
            username="candidate_1",
            email="candidate_1@a.a",
            first_name="john",
            last_name="doe",
        )
        candidate_2 = get_user_model().objects.create(
            username="candidate_2",
            email="candidate_2@a.a",
            first_name="jane",
            last_name="doe",
        )

        form_data = dict(
            name=self.public_data_source_of_user.name,
            life_domain="bacteria",
            description="",
            new_allowed_users="%s %s\n%s %s" % (candidate_1.first_name, candidate_1.last_name,
                                                candidate_2.first_name, candidate_2.last_name),
            allowed_users=",".join([str(u.pk) for u in self.public_data_source_of_user.allowed_users.all()]),
        )
        allowed_users.add(candidate_1.pk)
        allowed_users.add(candidate_2.pk)

        self.client.post(url, form_data)
        self.assertSetEqual(allowed_users, set([u.pk for u in self.public_data_source_of_user.allowed_users.all()]))

    def test_unknown_user_fails(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)

        allowed_users = set([u.pk for u in self.public_data_source_of_user.allowed_users.all()])

        form_data = dict(
            name=self.public_data_source_of_user.name,
            life_domain="bacteria",
            description="",
            new_allowed_users="a@a.a\nblabla",
            allowed_users=",".join([str(u.pk) for u in self.public_data_source_of_user.allowed_users.all()]),
        )
        allowed_users.add(self.toto.pk)

        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 200)

    def test_user_injection_prevented(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)

        allowed_users = set([u.pk for u in self.public_data_source_of_user.allowed_users.all()])

        form_data = dict(
            name=self.public_data_source_of_user.name,
            life_domain="bacteria",
            description="",
            new_allowed_users="",
            allowed_users=",".join([str(u.pk) for u in get_user_model().objects.all()]),
        )

        self.client.post(url, form_data)
        self.assertSetEqual(allowed_users, set([u.pk for u in self.public_data_source_of_user.allowed_users.all()]))

    def test_cross_user_edition_prevented(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.private_data_source_of_toto.pk])
        self.client.force_login(self.user)

        former_description = self.private_data_source_of_toto.description
        form_data = dict(
            name=self.private_data_source_of_toto.name,
            life_domain="bacteria",
            description="XXX",
            new_allowed_users="",
            allowed_users=",".join([str(u.pk) for u in get_user_model().objects.all()]),
        )

        response = self.client.post(url, form_data)
        self.assertNotEqual(response.status_code, 200)

        self.assertEqual(
            models.DataSource.objects.get(pk=self.private_data_source_of_toto.pk).description,
            former_description
        )

    def test_no_public_without_desc(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.private_data_source_of_toto.pk])
        self.client.force_login(self.toto)

        former_description = self.private_data_source_of_toto.description
        form_data = dict(
            name=self.private_data_source_of_toto.name,
            life_domain="bacteria",
            description="XXX",
            new_allowed_users="",
            public=True,
        )

        response = self.client.post(url, form_data)
        self.private_data_source_of_toto.refresh_from_db()
        self.assertEqual(response.status_code, 200)

        self.assertFalse(self.private_data_source_of_toto.public)
        self.assertEqual(self.private_data_source_of_toto.description, former_description)

    def test_no_homonyms(self):
        url = reverse('viralhostrangedb:data-source-update', args=[self.private_data_source_of_toto.pk])
        self.client.force_login(self.toto)

        form_data = dict(
            name=self.public_data_source_of_user.name,
            life_domain="bacteria",
            description="XXXXXXXXX XXXXXXXXX XXXXXXXXX XXXXXXXXX ",
            new_allowed_users="",
            public=False,
        )

        response = self.client.post(url, form_data)
        self.private_data_source_of_toto.refresh_from_db()
        self.assertEqual(response.status_code, 200)

        self.assertNotEqual(self.private_data_source_of_toto.name, self.public_data_source_of_user.name)


class DataSourceCreateViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-create')
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('viralhostrangedb:data-source-create-step', args=["Intro"])
        )

    def test_creation_with_a_file(self):
        old_data_source = set(models.DataSource.objects.values_list("pk", flat=True))

        url = reverse('viralhostrangedb:data-source-create')
        self.client.force_login(self.user)
        response_home = self.client.get(url, follow=True)
        target_home = response_home.redirect_chain[0][0]
        response_0 = self.client.post(
            target_home,
            {"data_source_wizard-current_step": target_home.split("/")[-2]},
            follow=True
        )

        target_1 = response_0.redirect_chain[0][0]
        step_name_1 = target_1.split("/")[-2]
        form_data_1 = {
            step_name_1 + "-name": "test",
            step_name_1 + "-life_domain": "bacteria",
            # step_name_1 + "-public": False, # comment when false to be sure that it is considered as False
            "data_source_wizard-current_step": step_name_1,
        }
        response_1 = self.client.post(target_1, form_data_1)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.post(target_1, form_data_1, follow=True)

        allowed_users = set()
        allowed_users.add(self.toto)
        target_1bis = response_1.redirect_chain[0][0]
        step_name_1bis = target_1bis.split("/")[-2]
        form_data_1bis = {
            step_name_1bis + "-new_allowed_users": ",".join([u.email for u in allowed_users]),
            "data_source_wizard-current_step": step_name_1bis,
        }
        response_1bis = self.client.post(target_1bis, form_data_1bis)
        self.assertEqual(response_1bis.status_code, 302)
        response_1bis = self.client.post(target_1bis, form_data_1bis, follow=True)

        target_2 = response_1bis.redirect_chain[0][0]
        step_name_2 = target_2.split("/")[-2]
        form_data_2 = {
            step_name_2 + "-description": "",
            step_name_2 + "-experiment_date": "",
            step_name_2 + "-publication_url": "https://www.pasteur.fr/fr",
            "data_source_wizard-current_step": step_name_2,
        }
        response_2 = self.client.post(target_2, form_data_2)
        self.assertEqual(response_2.status_code, 302)
        response_2 = self.client.post(target_2, form_data_2, follow=True)

        target_3 = response_2.redirect_chain[0][0]
        step_name_3 = target_3.split("/")[-2]
        form_data_3 = {
            step_name_3 + "-upload_or_live_input": "upload",
            "data_source_wizard-current_step": step_name_3,
        }
        response_3 = self.client.post(target_3, form_data_3)
        self.assertEqual(response_3.status_code, 302)
        response_3 = self.client.post(target_3, form_data_3, follow=True)

        f = open(os.path.join(self.test_data, "three_reponse_simple.xlsx"), "rb")
        target_4 = response_3.redirect_chain[0][0]
        step_name_4 = target_4.split("/")[-2]
        form_data_4 = {
            step_name_4 + "-file": SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
            "data_source_wizard-current_step": step_name_4,
        }
        # response = self.client.post(target_4, form_data_4)
        # self.assertEqual(response.status_code, 302)
        response_4 = self.client.post(target_4, form_data_4, follow=True)

        new_pk = (set(models.DataSource.objects.values_list("pk", flat=True)) - old_data_source).pop()
        new_data_source = models.DataSource.objects.get(pk=new_pk)
        self.assertSetEqual(
            allowed_users,
            set([o for o in new_data_source.allowed_users.all()])
        )
        self.assertEqual(form_data_2[step_name_2 + "-description"], new_data_source.description)
        self.assertEqual(form_data_2[step_name_2 + "-publication_url"], new_data_source.publication_url)
        self.assertEqual(form_data_1[step_name_1 + "-name"], new_data_source.name)
        # self.assertIsNone(new_data_source.experiment_date)
        self.assertFalse(new_data_source.public)

        self.assertRedirects(
            response_4,
            expected_url=reverse('viralhostrangedb:data-source-mapping-edit', args=[new_pk])
        )

    def test_creation_with_a_live(self):
        old_data_source = set(models.DataSource.objects.values_list("pk", flat=True))

        url = reverse('viralhostrangedb:data-source-create')
        self.client.force_login(self.user)
        response_home = self.client.get(url, follow=True)
        target_home = response_home.redirect_chain[0][0]
        response_0 = self.client.post(
            target_home,
            {"data_source_wizard-current_step": target_home.split("/")[-2]},
            follow=True
        )

        target_1 = response_0.redirect_chain[0][0]
        step_name_1 = target_1.split("/")[-2]
        form_data_1 = {
            step_name_1 + "-name": "test",
            step_name_1 + "-life_domain": "bacteria",
            step_name_1 + "-public": True,
            "data_source_wizard-current_step": step_name_1,
        }
        response_1 = self.client.post(target_1, form_data_1)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.post(target_1, form_data_1, follow=True)

        target_1bis = response_1.redirect_chain[0][0]
        step_name_1bis = target_1bis.split("/")[-2]
        form_data_1bis = {
            step_name_1bis + "-description": "blablablabla",
            step_name_1bis + "-experiment_date": "2018-12-22",
            "data_source_wizard-current_step": step_name_1bis,
        }
        response_1bis = self.client.post(target_1bis, form_data_1bis)
        self.assertEqual(response_1bis.status_code, 302)
        response_1bis = self.client.post(target_1bis, form_data_1bis, follow=True)

        # allowed_users = set()
        # allowed_users.add(self.toto)
        # target_2 = response.redirect_chain[0][0]
        # step_name_2 = target_2.split("/")[-2]
        # form_data_2 = {
        #     step_name_2 + "-new_allowed_users": ",".join([u.username for u in allowed_users]),
        #     "data_source_wizard-current_step": step_name_2,
        # }
        # response = self.client.post(target_2, form_data_2)
        # self.assertEqual(response.status_code, 302)
        # response = self.client.post(target_2, form_data_2, follow=True)

        target_3 = response_1bis.redirect_chain[0][0]
        step_name_3 = target_3.split("/")[-2]
        form_data_3 = {
            step_name_3 + "-upload_or_live_input": "live",
            "data_source_wizard-current_step": step_name_3,
        }
        response_3 = self.client.post(target_3, form_data_3)
        self.assertEqual(response_3.status_code, 302)
        response_3 = self.client.post(target_3, form_data_3, follow=True)

        old_in_media = set(os.listdir(settings.MEDIA_ROOT))

        target_4 = response_3.redirect_chain[0][0]
        step_name_4 = target_4.split("/")[-2]
        viruses = {"v1", "v2"}
        hosts = {"h1", "h2", "h3", "h3 (r)", "h3 (NC_001416)", "h3 (another ncbi id)"}
        form_data_4 = {
            step_name_4 + "-virus": "\n".join(viruses),
            step_name_4 + "-virus_delimiter": "\t",
            step_name_4 + "-host": "\t".join(hosts) + "\n",
            step_name_4 + "-host_delimiter": "\t",
            "data_source_wizard-current_step": step_name_4,
        }
        response_4 = self.client.post(target_4, form_data_4)
        self.assertEqual(response_4.status_code, 302)
        response_4 = self.client.post(target_4, form_data_4, follow=True)

        new_in_media = set(os.listdir(settings.MEDIA_ROOT)) - old_in_media
        self.assertEqual(len(new_in_media), 2)
        complet_file = new_in_media.pop()
        if complet_file.endswith(".sample.xlsx"):
            complet_file = new_in_media.pop()
        vhrs = business_process.parse_file(os.path.join(settings.MEDIA_ROOT, complet_file))
        new_viruses = set()
        new_hosts = set()
        for vhr in vhrs:
            new_viruses.add(business_process.explicit_item(vhr.virus, vhr.virus_identifier))
            new_hosts.add(business_process.explicit_item(vhr.host, vhr.host_identifier))

        self.assertSetEqual(
            viruses,
            new_viruses,
        )
        self.assertSetEqual(
            hosts,
            new_hosts,
        )

        f = open(os.path.join(self.test_data, "three_reponse_simple.xlsx"), "rb")
        target_5 = response_4.redirect_chain[0][0]
        step_name_5 = target_5.split("/")[-2]
        form_data_5 = {
            step_name_5 + "-file": SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
            "data_source_wizard-current_step": step_name_5,
        }
        response_last = self.client.post(target_5, form_data_5, follow=True)

        new_pk = (set(models.DataSource.objects.values_list("pk", flat=True)) - old_data_source).pop()
        new_data_source = models.DataSource.objects.get(pk=new_pk)
        self.assertSetEqual(set(), set([o for o in new_data_source.allowed_users.all()]))
        self.assertEqual(form_data_1bis[step_name_1bis + "-description"], new_data_source.description)
        # self.assertEqual(
        #     form_data_1bis[step_name_1bis + "-experiment_date"],
        #     new_data_source.experiment_date.strftime('%Y-%m-%d'),
        # )
        self.assertEqual(form_data_1[step_name_1 + "-name"], new_data_source.name)
        self.assertEqual(form_data_1[step_name_1 + "-public"], new_data_source.public)

        self.assertRedirects(
            response_last,
            expected_url=reverse('viralhostrangedb:data-source-mapping-edit', args=[new_pk])
        )

    def test_creation_public_is_true_and_empty_description_fails(self):
        url = reverse('viralhostrangedb:data-source-create')
        self.client.force_login(self.user)
        response_home = self.client.get(url, follow=True)
        target_home = response_home.redirect_chain[0][0]
        response_0 = self.client.post(
            target_home,
            {"data_source_wizard-current_step": target_home.split("/")[-2]},
            follow=True
        )

        target_1 = response_0.redirect_chain[0][0]
        step_name_1 = target_1.split("/")[-2]
        form_data_1 = {
            step_name_1 + "-name": "test",
            step_name_1 + "-life_domain": "bacteria",
            step_name_1 + "-public": True,
            "data_source_wizard-current_step": step_name_1,
        }
        response_1 = self.client.post(target_1, form_data_1)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.post(target_1, form_data_1, follow=True)

        target_1bis = response_1.redirect_chain[0][0]
        step_name_1bis = target_1bis.split("/")[-2]
        form_data_1bis = {
            # step_name_1bis + "-description": "",
            step_name_1bis + "-experiment_date": "2018-12-22",
            "data_source_wizard-current_step": step_name_1bis,
        }
        response_1bis = self.client.post(target_1bis, form_data_1bis)
        self.assertEqual(response_1bis.status_code, 200)

    # def test_creation_public_is_true_and_empty_experiment_date_fails(self):
    #     url = reverse('viralhostrangedb:data-source-create')
    #     self.client.force_login(self.user)
    #     response_home = self.client.get(url, follow=True)
    #     target_home = response_home.redirect_chain[0][0]
    #     response_0 = self.client.post(
    #         target_home,
    #         {"data_source_wizard-current_step": target_home.split("/")[-2]},
    #         follow=True
    #     )
    #
    #     target_1 = response_0.redirect_chain[0][0]
    #     step_name_1 = target_1.split("/")[-2]
    #     form_data_1 = {
    #         step_name_1 + "-name": "test",
    #         step_name_1 + "-life_domain": "bacteria",
    #         step_name_1 + "-public": True,
    #         "data_source_wizard-current_step": step_name_1,
    #     }
    #     response_1 = self.client.post(target_1, form_data_1)
    #     self.assertEqual(response_1.status_code, 302)
    #     response_1 = self.client.post(target_1, form_data_1, follow=True)
    #
    #     target_1bis = response_1.redirect_chain[0][0]
    #     step_name_1bis = target_1bis.split("/")[-2]
    #     form_data_1bis = {
    #         step_name_1bis + "-description": "blablablabla",
    #         # step_name_1bis + "-experiment_date": "2018-12-22",
    #         "data_source_wizard-current_step": step_name_1bis,
    #     }
    #     response_1bis = self.client.post(target_1bis, form_data_1bis)
    #     self.assertEqual(response_1bis.status_code, 200)

    # def test_creation_public_is_true_and_empty_experiment_date_and_description_fails(self):
    #     url = reverse('viralhostrangedb:data-source-create')
    #     self.client.force_login(self.user)
    #     response_home = self.client.get(url, follow=True)
    #     target_home = response_home.redirect_chain[0][0]
    #     response_0 = self.client.post(
    #         target_home,
    #         {"data_source_wizard-current_step": target_home.split("/")[-2]},
    #         follow=True
    #     )
    #
    #     target_1 = response_0.redirect_chain[0][0]
    #     step_name_1 = target_1.split("/")[-2]
    #     form_data_1 = {
    #         step_name_1 + "-name": "test",
    #         step_name_1 + "-life_domain": "bacteria",
    #         step_name_1 + "-public": True,
    #         "data_source_wizard-current_step": step_name_1,
    #     }
    #     response_1 = self.client.post(target_1, form_data_1)
    #     self.assertEqual(response_1.status_code, 302)
    #     response_1 = self.client.post(target_1, form_data_1, follow=True)
    #
    #     target_1bis = response_1.redirect_chain[0][0]
    #     step_name_1bis = target_1bis.split("/")[-2]
    #     form_data_1bis = {
    #         # step_name_1bis + "-description": "blablablabla",
    #         # step_name_1bis + "-experiment_date": "2018-12-22",
    #         "data_source_wizard-current_step": step_name_1bis,
    #     }
    #     response_1bis = self.client.post(target_1bis, form_data_1bis)
    #     self.assertEqual(response_1bis.status_code, 200)

    def test_creation_public_is_true_and_small_description_fails(self):
        url = reverse('viralhostrangedb:data-source-create')
        self.client.force_login(self.user)
        response = self.client.get(url, follow=True)

        target_1 = response.redirect_chain[0][0]
        step_name_1 = target_1.split("/")[-2]
        form_data_1 = {
            step_name_1 + "-name": "test",
            step_name_1 + "-life_domain": "bacteria",
            step_name_1 + "-public": True,
            "data_source_wizard-current_step": step_name_1,
        }
        response_1 = self.client.post(target_1, form_data_1)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.post(target_1, form_data_1, follow=True)

        target_1bis = response_1.redirect_chain[0][0]
        step_name_1bis = target_1bis.split("/")[-2]
        form_data_1bis = {
            step_name_1bis + "-description": "too small",
            "data_source_wizard-current_step": step_name_1bis,
        }
        response_1bis = self.client.post(target_1bis, form_data_1bis)
        self.assertEqual(response_1bis.status_code, 200)

    def test_creation_with_a_live_and_auto_guess(self):
        name = "test (%i)"
        i = 0
        for sep_h in ['\t', ',', ';', '\n']:
            for sep_v in ['\t', ',', ';', '\n']:
                i += 1
                self.creation_with_a_live_and_auto_guess(host_separator=sep_h, virus_separator=sep_v, name=name % i)

    def creation_with_a_live_and_auto_guess(self, host_separator, virus_separator, name):
        old_data_source = set(models.DataSource.objects.values_list("pk", flat=True))

        url = reverse('viralhostrangedb:data-source-create')
        self.client.force_login(self.user)
        response_home = self.client.get(url, follow=True)
        target_home = response_home.redirect_chain[0][0]
        response_0 = self.client.post(
            target_home,
            {"data_source_wizard-current_step": target_home.split("/")[-2]},
            follow=True
        )

        target_1 = response_0.redirect_chain[0][0]
        step_name_1 = target_1.split("/")[-2]
        form_data_1 = {
            step_name_1 + "-name": name,
            step_name_1 + "-life_domain": "bacteria",
            step_name_1 + "-public": True,
            "data_source_wizard-current_step": step_name_1,
        }
        response_1 = self.client.post(target_1, form_data_1)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.post(target_1, form_data_1, follow=True)

        target_1bis = response_1.redirect_chain[0][0]
        step_name_1bis = target_1bis.split("/")[-2]
        form_data_1bis = {
            step_name_1bis + "-description": "blablablabla",
            step_name_1bis + "-experiment_date": "2018-12-22",
            "data_source_wizard-current_step": step_name_1bis,
        }
        response_1bis = self.client.post(target_1bis, form_data_1bis)
        self.assertEqual(response_1bis.status_code, 302)
        response_1bis = self.client.post(target_1bis, form_data_1bis, follow=True)

        target_3 = response_1bis.redirect_chain[0][0]
        step_name_3 = target_3.split("/")[-2]
        form_data_3 = {
            step_name_3 + "-upload_or_live_input": "live",
            "data_source_wizard-current_step": step_name_3,
        }
        response_3 = self.client.post(target_3, form_data_3)
        self.assertEqual(response_3.status_code, 302)
        response_3 = self.client.post(target_3, form_data_3, follow=True)

        old_in_media = set(os.listdir(settings.MEDIA_ROOT))

        target_4 = response_3.redirect_chain[0][0]
        step_name_4 = target_4.split("/")[-2]
        viruses = {"v1 (42)", "v2"}
        hosts = {"h1", "h2 (HG:qsdqsd)", "h3"}
        form_data_4 = {
            step_name_4 + "-virus": virus_separator.join(viruses),
            step_name_4 + "-virus_delimiter": "",
            step_name_4 + "-host": host_separator.join(hosts),
            step_name_4 + "-host_delimiter": "",
            "data_source_wizard-current_step": step_name_4,
        }
        response_4 = self.client.post(target_4, form_data_4)
        self.assertEqual(response_4.status_code, 302)
        response_4 = self.client.post(target_4, form_data_4, follow=True)

        new_in_media = set(os.listdir(settings.MEDIA_ROOT)) - old_in_media
        self.assertEqual(len(new_in_media), 2)
        complet_file = new_in_media.pop()
        if complet_file.endswith(".sample.xlsx"):
            complet_file = new_in_media.pop()
        vhrs = business_process.parse_file(os.path.join(settings.MEDIA_ROOT, complet_file))
        new_viruses = set()
        new_hosts = set()
        for vhr in vhrs:
            new_viruses.add(business_process.explicit_item(vhr.virus, vhr.virus_identifier))
            new_hosts.add(business_process.explicit_item(vhr.host, vhr.host_identifier))

        self.assertSetEqual(
            viruses,
            new_viruses,
        )
        self.assertSetEqual(
            hosts,
            new_hosts,
        )

        f = open(os.path.join(self.test_data, "three_reponse_simple.xlsx"), "rb")
        target_5 = response_4.redirect_chain[0][0]
        step_name_5 = target_5.split("/")[-2]
        form_data_5 = {
            step_name_5 + "-file": SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
            "data_source_wizard-current_step": step_name_5,
        }
        response_last = self.client.post(target_5, form_data_5, follow=True)

        new_pk = (set(models.DataSource.objects.values_list("pk", flat=True)) - old_data_source).pop()
        new_data_source = models.DataSource.objects.get(pk=new_pk)
        self.assertSetEqual(set(), set([o for o in new_data_source.allowed_users.all()]))
        self.assertEqual(form_data_1bis[step_name_1bis + "-description"], new_data_source.description)
        self.assertEqual(form_data_1[step_name_1 + "-name"], new_data_source.name)
        self.assertEqual(form_data_1[step_name_1 + "-public"], new_data_source.public)
        self.assertRedirects(
            response_last,
            expected_url=reverse('viralhostrangedb:data-source-mapping-edit', args=[new_pk]),
        )

    def test_creation_with_a_live_and_auto_guess_fails_if_mix_separator(self):
        old_data_source = set(models.DataSource.objects.values_list("pk", flat=True))

        url = reverse('viralhostrangedb:data-source-create')
        self.client.force_login(self.user)
        response_home = self.client.get(url, follow=True)
        target_home = response_home.redirect_chain[0][0]
        response_0 = self.client.post(
            target_home,
            {"data_source_wizard-current_step": target_home.split("/")[-2]},
            follow=True
        )

        target_1 = response_0.redirect_chain[0][0]
        step_name_1 = target_1.split("/")[-2]
        form_data_1 = {
            step_name_1 + "-name": "test",
            step_name_1 + "-life_domain": "bacteria",
            step_name_1 + "-public": True,
            "data_source_wizard-current_step": step_name_1,
        }
        response = self.client.post(target_1, form_data_1)
        self.assertEqual(response.status_code, 302)
        response_1 = self.client.post(target_1, form_data_1, follow=True)

        target_1bis = response_1.redirect_chain[0][0]
        step_name_1bis = target_1bis.split("/")[-2]
        form_data_1bis = {
            step_name_1bis + "-description": "blablablabla",
            step_name_1bis + "-experiment_date": "2018-12-22",
            "data_source_wizard-current_step": step_name_1bis,
        }
        response_1bis = self.client.post(target_1bis, form_data_1bis)
        self.assertEqual(response_1bis.status_code, 302)
        response_1bis = self.client.post(target_1bis, form_data_1bis, follow=True)

        target_3 = response_1bis.redirect_chain[0][0]
        step_name_3 = target_3.split("/")[-2]
        form_data_3 = {
            step_name_3 + "-upload_or_live_input": "live",
            "data_source_wizard-current_step": step_name_3,
        }
        response = self.client.post(target_3, form_data_3)
        self.assertEqual(response.status_code, 302)
        response = self.client.post(target_3, form_data_3, follow=True)

        target_4 = response.redirect_chain[0][0]
        step_name_4 = target_4.split("/")[-2]
        for host_separator in ['\t', ',', ';']:
            for wrong_separator in ['\t', ',', ';']:
                if wrong_separator == host_separator:
                    continue
                viruses = {"v"}
                hosts = {"h1", "h2 (HG%s54)" % wrong_separator, "h3"}
                form_data_4 = {
                    step_name_4 + "-virus": ','.join(viruses),
                    step_name_4 + "-virus_delimiter": "",
                    step_name_4 + "-host": host_separator.join(hosts),
                    step_name_4 + "-host_delimiter": "",
                    "data_source_wizard-current_step": step_name_4,
                }
                response = self.client.post(target_4, form_data_4)
                self.assertEqual(response.status_code, 200)

    def test_creation_with_a_live_and_auto_guess_two_col(self):
        old_data_source = set(models.DataSource.objects.values_list("pk", flat=True))

        url = reverse('viralhostrangedb:data-source-create')
        self.client.force_login(self.user)
        response_home = self.client.get(url, follow=True)
        target_home = response_home.redirect_chain[0][0]
        response_0 = self.client.post(
            target_home,
            {"data_source_wizard-current_step": target_home.split("/")[-2]},
            follow=True
        )

        target_1 = response_0.redirect_chain[0][0]
        step_name_1 = target_1.split("/")[-2]
        form_data_1 = {
            step_name_1 + "-name": "test",
            step_name_1 + "-life_domain": "bacteria",
            step_name_1 + "-public": True,
            "data_source_wizard-current_step": step_name_1,
        }
        response_1 = self.client.post(target_1, form_data_1)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.post(target_1, form_data_1, follow=True)

        target_1bis = response_1.redirect_chain[0][0]
        step_name_1bis = target_1bis.split("/")[-2]
        form_data_1bis = {
            step_name_1bis + "-description": "blablablabla",
            step_name_1bis + "-experiment_date": "2018-12-22",
            "data_source_wizard-current_step": step_name_1bis,
        }
        response_1bis = self.client.post(target_1bis, form_data_1bis)
        self.assertEqual(response_1bis.status_code, 302)
        response_1bis = self.client.post(target_1bis, form_data_1bis, follow=True)

        target_3 = response_1bis.redirect_chain[0][0]
        step_name_3 = target_3.split("/")[-2]
        form_data_3 = {
            step_name_3 + "-upload_or_live_input": "live",
            "data_source_wizard-current_step": step_name_3,
        }
        response_3 = self.client.post(target_3, form_data_3)
        self.assertEqual(response_3.status_code, 302)
        response_3 = self.client.post(target_3, form_data_3, follow=True)

        old_in_media = set(os.listdir(settings.MEDIA_ROOT))

        target_4 = response_3.redirect_chain[0][0]
        step_name_4 = target_4.split("/")[-2]
        viruses = {"a (1)", "b (2)", "c (3)", "d", "e (5)"}
        viruses_copy_paste = """a	1
b	2
c	3
d	
e	5
"""
        hosts = {"h1", "h2 (HG:qsdqsd)", "h3"}
        hosts_copy_paste = """h1	
h2	HG:qsdqsd
h3	
"""
        form_data_4 = {
            step_name_4 + "-virus": viruses_copy_paste,
            step_name_4 + "-virus_delimiter": "",
            step_name_4 + "-host": hosts_copy_paste,
            step_name_4 + "-host_delimiter": "two_col	",
            "data_source_wizard-current_step": step_name_4,
        }
        response_4 = self.client.post(target_4, form_data_4)
        self.assertEqual(response_4.status_code, 302)
        response_4 = self.client.post(target_4, form_data_4, follow=True)

        new_in_media = set(os.listdir(settings.MEDIA_ROOT)) - old_in_media
        self.assertEqual(len(new_in_media), 2)
        complet_file = new_in_media.pop()
        if complet_file.endswith(".sample.xlsx"):
            complet_file = new_in_media.pop()
        vhrs = business_process.parse_file(os.path.join(settings.MEDIA_ROOT, complet_file))
        new_viruses = set()
        new_hosts = set()
        for vhr in vhrs:
            new_viruses.add(business_process.explicit_item(vhr.virus, vhr.virus_identifier))
            new_hosts.add(business_process.explicit_item(vhr.host, vhr.host_identifier))

        self.assertSetEqual(
            viruses,
            new_viruses,
        )
        self.assertSetEqual(
            hosts,
            new_hosts,
        )

        f = open(os.path.join(self.test_data, "three_reponse_simple.xlsx"), "rb")
        target_5 = response_4.redirect_chain[0][0]
        step_name_5 = target_5.split("/")[-2]
        form_data_5 = {
            step_name_5 + "-file": SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
            "data_source_wizard-current_step": step_name_5,
        }
        response_last = self.client.post(target_5, form_data_5, follow=True)

        new_pk = (set(models.DataSource.objects.values_list("pk", flat=True)) - old_data_source).pop()
        new_data_source = models.DataSource.objects.get(pk=new_pk)
        self.assertSetEqual(set(), set([o for o in new_data_source.allowed_users.all()]))
        self.assertEqual(form_data_1bis[step_name_1bis + "-description"], new_data_source.description)
        self.assertEqual(form_data_1[step_name_1 + "-name"], new_data_source.name)
        self.assertEqual(form_data_1[step_name_1 + "-public"], new_data_source.public)
        self.assertRedirects(
            response_last,
            expected_url=reverse('viralhostrangedb:data-source-mapping-edit', args=[new_pk])
        )

    def test_with_same_user_same_name_fails(self):
        self.creation_with_a_live_and_auto_guess('\t', '\t', "test")
        self.assertRaises(AssertionError, self.creation_with_a_live_and_auto_guess, '\t', '\t', "test")

    def test_with_different_user_same_name_public_ds_fails(self):
        self.creation_with_a_live_and_auto_guess('\t', '\t', "test")
        self.creation_with_a_live_and_auto_guess('\t', '\t', "test (1)")
        ds = models.DataSource.objects.get(name="test")
        ds.owner = self.toto
        ds.public = True
        ds.save()
        self.assertRaises(AssertionError, self.creation_with_a_live_and_auto_guess, '\t', '\t', "test")

    def test_with_different_user_same_name_private_ds_allowed_fails(self):
        self.creation_with_a_live_and_auto_guess('\t', '\t', "test")
        ds = models.DataSource.objects.get(name="test")
        ds.owner = self.toto
        ds.allowed_users.add(self.user)
        ds.save()
        self.assertRaises(AssertionError, self.creation_with_a_live_and_auto_guess, '\t', '\t', "test")


class DataSourceDetailViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-detail', args=[self.private_data_source_of_user.pk])
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_cross_users(self):
        url = reverse('viralhostrangedb:data-source-detail', args=[self.private_data_source_of_user.pk])
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_staff_can_see_private(self):
        url = reverse('viralhostrangedb:data-source-detail', args=[self.private_data_source_of_toto.pk])
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_public_can_be_seen(self):
        url = reverse('viralhostrangedb:data-source-detail', args=[self.public_data_source_of_user.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class DataSourceMappingEditTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-mapping-label-edit', args=[self.private_data_source_of_toto.pk])
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_mapping_work(self):
        url = reverse('viralhostrangedb:data-source-mapping-label-edit', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.public_data_source_of_user.is_mapping_done)

        form_data = {
            "form-TOTAL_FORMS": 3,
            "form-INITIAL_FORMS": 3,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-raw_response": 0.0,
            "form-0-mapping": self.no_lysis.pk,
            "form-1-raw_response": 1.0,
            "form-1-mapping": self.no_lysis.pk,
            "form-2-raw_response": 2.0,
            "form-2-mapping": self.lysis.pk,
        }

        response = self.client.post(url, form_data)
        self.assertRedirects(
            response,
            expected_url=reverse('viralhostrangedb:data-source-detail', args=[self.public_data_source_of_user.pk])
        )

        self.assertTrue(self.public_data_source_of_user.is_mapping_done)
        raw_to_response = {}
        for raw_response, response in self.public_data_source_of_user.get_mapping():
            raw_to_response[raw_response] = response
        self.assertEqual(raw_to_response.pop(0.0), self.no_lysis)
        self.assertEqual(raw_to_response.pop(1.0), self.no_lysis)
        self.assertEqual(raw_to_response.pop(2.0), self.lysis)
        self.assertEqual(len(raw_to_response), 0)

    def test_mapping_form_incomplet(self):
        url = reverse('viralhostrangedb:data-source-mapping-label-edit', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.public_data_source_of_user.is_mapping_done)

        form_data = {}

        self.assertRaises(ValidationError, self.client.post, url, form_data)

        self.assertFalse(self.public_data_source_of_user.is_mapping_done)


class DataSourceVirusOrHostUpdateTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-virus-update', args=[self.private_data_source_of_toto.pk])
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_virus_update_work(self):
        url = 'viralhostrangedb:data-source-virus-update'
        self.actual_test_update_work_with_transfer_and_split(url, "virus_set", self.private_data_source_of_toto)

    def test_virus_update_work_with_deletion(self):
        url = 'viralhostrangedb:data-source-virus-update'
        self.actual_test_update_work_with_deletion(
            url, "virus_set", self.private_data_source_of_toto_with_no_virus_or_host_shared, "r%i")

    def test_virus_update_work_with_rename(self):
        url = 'viralhostrangedb:data-source-virus-update'
        self.actual_test_update_work_with_rename(
            url, "virus_set", self.private_data_source_of_toto_with_no_virus_or_host_shared)

    def test_host_update_work(self):
        url = 'viralhostrangedb:data-source-host-update'
        self.actual_test_update_work_with_transfer_and_split(url, "host_set", self.private_data_source_of_toto)

    def test_host_update_work_with_deletion(self):
        url = 'viralhostrangedb:data-source-host-update'
        self.actual_test_update_work_with_deletion(
            url, "host_set", self.private_data_source_of_toto_with_no_virus_or_host_shared, "C%i")

    def test_host_update_work_with_rename(self):
        url = 'viralhostrangedb:data-source-host-update'
        self.actual_test_update_work_with_rename(
            url, "host_set", self.private_data_source_of_toto_with_no_virus_or_host_shared)

    def actual_test_update_work_with_transfer_and_split(self, url, entry_set, data_source):
        url = reverse(url, args=[data_source.pk])
        self.client.force_login(self.toto)
        existing_pks = set(getattr(data_source, entry_set).values_list("pk", flat=True))
        count = getattr(data_source, entry_set).order_by("id").count()
        initial_overall_count = getattr(data_source, entry_set).first().__class__.objects.count()

        count_existing_ds_linked_to_with_ids = set()
        for obj in getattr(self.public_data_source_with_ids_of_toto, entry_set).all():
            count_existing_ds_linked_to_with_ids.update(set(obj.data_source.values_list("pk", flat=True)))

        top_k_to_change = int(count * 0.7)
        form_data = {
            "form-TOTAL_FORMS": count,
            "form-INITIAL_FORMS": count,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
        }
        top_k_name_changed = []
        instance_with_fixed_identifier = None
        for i, v in enumerate(getattr(data_source, entry_set).order_by("id")):
            form_data["form-%i-name" % i] = v.name
            form_data["form-%i-id" % i] = v.id
            if i == top_k_to_change:
                form_data["form-%i-identifier" % i] = "Tralala666"
                instance_with_fixed_identifier = v
            elif i < top_k_to_change:
                alter_ego = getattr(self.public_data_source_with_ids_of_toto, entry_set) \
                    .filter(name=v.name) \
                    .order_by("id").first()
                top_k_name_changed.append(v.name)
                form_data["form-%i-identifier" % i] = alter_ego.identifier
            else:
                form_data["form-%i-identifier" % i] = v.identifier

        response = self.client.post(url, form_data)
        self.assertRedirects(
            response,
            expected_url=reverse('viralhostrangedb:data-source-detail',
                                 args=[data_source.pk])
        )
        self.assertEqual(getattr(data_source, entry_set).order_by("id").count(), count)  # check that it is not deleted

        for i, v in enumerate(getattr(data_source, entry_set).filter(name__in=top_k_name_changed)):
            alter_ego = getattr(self.public_data_source_with_ids_of_toto, entry_set) \
                .filter(name=v.name) \
                .order_by("id").first()
            self.assertEqual(v.identifier, alter_ego.identifier)

        # get a queryset with potentially an object with the old name but the new identifier
        candidate = getattr(data_source, entry_set).first().__class__.objects \
            .filter(name=instance_with_fixed_identifier.name, identifier="Tralala666")
        # check it is in the DB
        self.assertTrue(candidate.exists())
        # check that this object is linked to the data_source
        self.assertTrue(candidate.first().data_source.filter(id=data_source.id).exists())
        # check that this new instance is linked to only this data_source
        self.assertEqual(candidate.first().data_source.count(), 1)
        self.assertTrue(candidate.first().data_source.filter(id=data_source.id).exists())
        # check that the old object is not linked to the data source any more
        self.assertFalse(getattr(data_source, entry_set).filter(id=instance_with_fixed_identifier.id).exists())

        count_new_ds_linked_to_with_ids = set()
        for obj in getattr(self.public_data_source_with_ids_of_toto, entry_set).all():
            count_new_ds_linked_to_with_ids.update(set(obj.data_source.values_list("pk", flat=True)))
        for existing_ds_linked_to_with_ids in count_existing_ds_linked_to_with_ids:
            count_new_ds_linked_to_with_ids.discard(existing_ds_linked_to_with_ids)
        self.assertSetEqual(count_new_ds_linked_to_with_ids, {data_source.pk})

        new_pks = set(getattr(data_source, entry_set).values_list("pk", flat=True))
        self.assertEqual(len(existing_pks), len(new_pks))
        self.assertEqual(len(new_pks.intersection(existing_pks)), len(existing_pks) - top_k_to_change - 1)

        self.assertEqual(1 + initial_overall_count, getattr(data_source, entry_set).first().__class__.objects.count())

    def actual_test_update_work_with_deletion(self, url, entry_set, data_source, name_pattern):
        url = reverse(url, args=[data_source.pk])
        self.client.force_login(self.toto)
        existing_pks = set(getattr(data_source, entry_set).values_list("pk", flat=True))
        count = getattr(data_source, entry_set).order_by("id").count()
        initial_overall_count = getattr(data_source, entry_set).first().__class__.objects.count()

        for obj in getattr(data_source, entry_set).all():
            # count_existing_ds_linked_to_with_ids.update(set(obj.data_source.values_list("pk", flat=True)))
            self.assertEqual(
                obj.data_source.count(), 1,
                "This test is only relevant if the virus/host are not shared with an other data source"
            )

        form_data = {
            "form-TOTAL_FORMS": count,
            "form-INITIAL_FORMS": count,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
        }
        for i, v in enumerate(getattr(data_source, entry_set).order_by("id")):
            form_data["form-%i-name" % i] = name_pattern % (i + 1)
            form_data["form-%i-id" % i] = v.id
            form_data["form-%i-identifier" % i] = v.identifier

        response = self.client.post(url, form_data)
        self.assertRedirects(
            response,
            expected_url=reverse('viralhostrangedb:data-source-detail',
                                 args=[data_source.pk])
        )
        # check that there is still the same number of elements
        self.assertEqual(getattr(data_source, entry_set).order_by("id").count(), count)

        new_pks = set(getattr(data_source, entry_set).values_list("pk", flat=True))
        self.assertEqual(len(existing_pks), len(new_pks))
        self.assertEqual(len(new_pks.intersection(existing_pks)), 0)

        self.assertEqual(initial_overall_count - count,
                         getattr(data_source, entry_set).first().__class__.objects.count())

    def actual_test_update_work_with_rename(self, url, entry_set, data_source):
        url = reverse(url, args=[data_source.pk])
        self.client.force_login(self.toto)
        existing_pks = set(getattr(data_source, entry_set).values_list("pk", flat=True))
        count = getattr(data_source, entry_set).order_by("id").count()
        initial_overall_count = getattr(data_source, entry_set).first().__class__.objects.count()

        for obj in getattr(data_source, entry_set).all():
            # count_existing_ds_linked_to_with_ids.update(set(obj.data_source.values_list("pk", flat=True)))
            self.assertEqual(
                obj.data_source.count(), 1,
                "This test is only relevant if the virus/host are not shared with an other data source"
            )

        count_existing_ds_linked_to_with_ids = set()
        for obj in getattr(self.public_data_source_with_ids_of_toto, entry_set).all():
            count_existing_ds_linked_to_with_ids.update(set(obj.data_source.values_list("pk", flat=True)))

        form_data = {
            "form-TOTAL_FORMS": count,
            "form-INITIAL_FORMS": count,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
        }
        for i, v in enumerate(getattr(data_source, entry_set).order_by("id")):
            form_data["form-%i-name" % i] = v.name
            form_data["form-%i-id" % i] = v.id
            form_data["form-%i-identifier" % i] = "Tralala%i-%i-%s" % (i, v.id, v.name)

        response = self.client.post(url, form_data)
        self.assertRedirects(
            response,
            expected_url=reverse('viralhostrangedb:data-source-detail',
                                 args=[data_source.pk])
        )
        self.assertEqual(getattr(data_source, entry_set).order_by("id").count(), count)  # check that it is not deleted

        for i, v in enumerate(getattr(data_source, entry_set).order_by("id")):
            self.assertEqual(v.identifier, "Tralala%i-%i-%s" % (i, v.id, v.name))

        new_pks = set(getattr(data_source, entry_set).values_list("pk", flat=True))
        self.assertSetEqual(existing_pks, new_pks)

        self.assertEqual(initial_overall_count, getattr(data_source, entry_set).first().__class__.objects.count())

    def test_delete_not_possible(self):
        url = reverse('viralhostrangedb:data-source-virus-update', args=[self.private_data_source_of_toto.pk])
        entry_set = "virus_set"
        self.client.force_login(self.toto)
        count = getattr(self.private_data_source_of_toto, entry_set).count()
        form_data = {
            "form-TOTAL_FORMS": count,
            "form-INITIAL_FORMS": count,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
        }
        for i, v in enumerate(getattr(self.private_data_source_of_toto, entry_set).order_by("id")):
            form_data["form-%i-name" % i] = v.name
            form_data["form-%i-id" % i] = v.id
            form_data["form-%i-identifier" % i] = v.identifier or ''
        form_data["form-1-DELETE"] = "on"

        response = self.client.post(url, form_data)
        self.assertRedirects(
            response,
            expected_url=reverse('viralhostrangedb:data-source-detail', args=[self.private_data_source_of_toto.pk])
        )
        self.assertEqual(
            getattr(models.DataSource.objects.get(pk=self.private_data_source_of_toto.pk), entry_set).count(),
            count
        )


class DataSourceVirusOrHostDeleteTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-virus-delete', args=[self.private_data_source_of_toto.pk])
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_virus_deletion_work(self):
        url = reverse('viralhostrangedb:data-source-virus-delete', args=[self.private_data_source_of_toto.pk])
        self.actual_test_deletion_work(url, "virus_set")

    def test_host_deletion_work(self):
        url = reverse('viralhostrangedb:data-source-host-delete', args=[self.private_data_source_of_toto.pk])
        self.actual_test_deletion_work(url, "host_set")

    def test_host_deletion_work_with_no_other_data_source(self):
        models.DataSource.objects.filter(~Q(pk=self.private_data_source_of_toto.pk)).delete()
        url = reverse('viralhostrangedb:data-source-host-delete', args=[self.private_data_source_of_toto.pk])
        self.actual_test_deletion_work(url, "host_set")

    def actual_test_deletion_work(self, url, entry_set):
        self.client.force_login(self.toto)
        count = getattr(self.private_data_source_of_toto, entry_set).count()
        form_data = {
            "form-TOTAL_FORMS": count,
            "form-INITIAL_FORMS": count,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
        }
        entry_to_remove = None
        for i, v in enumerate(getattr(self.private_data_source_of_toto, entry_set).all()):
            form_data["form-%i-name" % i] = v.name
            form_data["form-%i-id" % i] = v.id
            if i == 1:
                entry_to_remove = v
        form_data["form-1-DELETE"] = "on"
        entry_to_remove__data_source__count = entry_to_remove.data_source.count()

        # form_data = dict(parse.parse_qsl("form-TOTAL_FORMS=3&form-INITIAL_FORMS=3&form-MIN_NUM_FORMS=0&"
        #                                  "form-MAX_NUM_FORMS=1000&form-0-name=r1&form-0-id=7&form-1-name=r2&"
        #                                  "form-1-id=8&form-1-DELETE=on&form-2-name=r3&form-2-id=9"))

        response = self.client.post(url, form_data)
        self.assertRedirects(
            response,
            expected_url=reverse('viralhostrangedb:data-source-detail', args=[self.private_data_source_of_toto.pk])
        )
        self.assertEqual(
            getattr(models.DataSource.objects.get(pk=self.private_data_source_of_toto.pk), entry_set).count(),
            count - 1
        )
        if entry_to_remove__data_source__count == 1:
            self.assertRaises(models.Host.DoesNotExist, entry_to_remove.refresh_from_db)
        else:
            entry_to_remove.refresh_from_db()
            self.assertEqual(entry_to_remove.data_source.count(), entry_to_remove__data_source__count - 1)


class UpdateDataSourceTestCase(ViewTestCase):
    def test_works(self):
        # must use a mapped data source
        self.assertTrue(self.public_data_source_of_user_mapped.is_mapping_done)
        # change one mapping
        response = self.public_data_source_of_user_mapped.responseindatasource.filter(raw_response=0).first()
        old_raw_response = response.raw_response
        old_mapping = response.response
        response.raw_response = 2
        response.response = models.GlobalViralHostResponseValue.get_not_mapped_yet()
        response.save()
        intermediate_response = models.ViralHostResponseValueInDataSource.objects.get(pk=response.pk)
        self.assertNotEqual(old_raw_response, intermediate_response)

        # re-import it (and thus revert the changes done)
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        business_process.import_file(
            data_source=self.public_data_source_of_user_mapped,
            file=filename,
        )

        # expect that mapping have been correctly guessed from other mapping
        new_response = models.ViralHostResponseValueInDataSource.objects.get(pk=response.pk)
        self.assertEqual(old_raw_response, new_response.raw_response)
        self.assertEqual(old_mapping, new_response.response)


class DataSourceContentUpdateTestCase(ViewTestCase):

    def test_403_work(self):
        url = reverse('viralhostrangedb:data-source-data-update', args=[self.public_data_source_of_user_mapped.pk])

        f = open(os.path.join(self.test_data, "three_reponse_simple.xlsx"), "rb")
        form_data = dict(
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )
        self.client.force_login(self.toto)
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 403)

    def test_work_with_mapping_that_can_be_guessed(self):
        self.client.force_login(self.user)
        # Test that mapping is ok
        self.assertTrue(self.public_data_source_of_user_mapped.is_mapping_done)

        # Break mapping
        content = self.public_data_source_of_user_mapped.responseindatasource.filter(raw_response=1).first()
        old_raw_response = content.raw_response
        content.raw_response = 666
        content.response = models.GlobalViralHostResponseValue.get_not_mapped_yet()
        content.save()

        # Checking that content has changed
        intermediate_content = models.ViralHostResponseValueInDataSource.objects.get(pk=content.pk).raw_response
        self.assertNotEqual(old_raw_response, intermediate_content)

        # Checking that mapping is broken
        self.assertFalse(self.public_data_source_of_user_mapped.is_mapping_done)

        # upload the file again so that changes are overwritten
        f = open(os.path.join(self.test_data, "three_reponse_simple.xlsx"), "rb")
        form_data = dict(
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )
        self.client.force_login(self.user)
        url = reverse('viralhostrangedb:data-source-data-update', args=[self.public_data_source_of_user_mapped.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        url = reverse('viralhostrangedb:data-source-data-update', args=[self.public_data_source_of_user_mapped.pk])
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('viralhostrangedb:data-source-detail',
                                                            args=[self.public_data_source_of_user_mapped.pk]), )

        # Now test that mapping have been guessed (as there was still mapping associated with 0, 1 and 2
        self.assertTrue(self.public_data_source_of_user_mapped.is_mapping_done)

        new_content = models.ViralHostResponseValueInDataSource.objects.get(pk=content.pk)
        self.assertEqual(old_raw_response, new_content.raw_response)

    def test_work_with_mapping_that_cannot_be_guessed(self):
        self.client.force_login(self.user)
        # Test that mapping is ok
        self.assertTrue(self.public_data_source_of_user_mapped.is_mapping_done)

        # Break mapping
        cpt = 0
        for content in self.public_data_source_of_user_mapped.responseindatasource.filter(raw_response=1):
            content.raw_response = 666
            content.response = models.GlobalViralHostResponseValue.get_not_mapped_yet()
            content.save()
            cpt += 1

        # Checking that content has changed
        self.assertEqual(self.public_data_source_of_user_mapped.responseindatasource.filter(raw_response=1).count(), 0)

        # Checking that mapping is broken
        self.assertFalse(self.public_data_source_of_user_mapped.is_mapping_done)

        # upload the file again so that changes are overwritten
        f = open(os.path.join(self.test_data, "three_reponse_simple.xlsx"), "rb")
        form_data = dict(
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )
        self.client.force_login(self.user)
        url = reverse('viralhostrangedb:data-source-data-update', args=[self.public_data_source_of_user_mapped.pk])
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse(
            'viralhostrangedb:data-source-mapping-edit',
            args=[self.public_data_source_of_user_mapped.pk],
        ))

        # Now test that mapping have been guessed (as there was still mapping associated with 0, 1 and 2
        self.assertFalse(self.public_data_source_of_user_mapped.is_mapping_done)
        self.assertEqual(
            self.public_data_source_of_user_mapped.responseindatasource.filter(raw_response=1).count(),
            cpt)


class DataSourceDownloadTestCase(ViewTestCase):

    def setUp(self):
        super().setUp()
        self.private_data_source_of_user_mapped = self.public_data_source_of_user_mapped
        self.private_data_source_of_user_mapped.public = False
        self.private_data_source_of_user_mapped.save()

    def test_privacy_works(self):
        url = reverse('viralhostrangedb:data-source-download', args=[self.private_data_source_of_user_mapped.pk])
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertIn(response.status_code, [403, 404])

    def test_download_works_when_raw_name_empty(self):
        url = reverse('viralhostrangedb:data-source-download', args=[self.public_data_source_of_user.pk])
        self.public_data_source_of_user.raw_name = ""
        self.public_data_source_of_user.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_download_contains_raw_data(self):
        url = reverse('viralhostrangedb:data-source-download', args=[self.private_data_source_of_user_mapped.pk])
        self.client.force_login(self.user)
        response = self.client.get(url)
        with NamedTemporaryFile(suffix=".xlsx", delete=False) as f:
            f.write(response.content)
            f.close()
            data_source = models.DataSource.objects.create(
                owner=self.user,
                public=False,
                name="reload",
                raw_name="ee",
                kind="FILE",
            )
            business_process.import_file(
                data_source=data_source,
                file=f.name,
            )
            os.unlink(f.name)
        virus_pks = set(self.private_data_source_of_user_mapped.virus_set.values_list('pk', flat=True))
        virus_pks_re_imported = set(data_source.virus_set.values_list('pk', flat=True))
        self.assertSetEqual(virus_pks, virus_pks_re_imported)
        host_pks = set(self.private_data_source_of_user_mapped.host_set.values_list('pk', flat=True))
        host_pks_re_imported = set(data_source.host_set.values_list('pk', flat=True))
        self.assertSetEqual(host_pks, host_pks_re_imported)
        for a, b in [
            (data_source, self.private_data_source_of_user_mapped),
            (self.private_data_source_of_user_mapped, data_source),
        ]:
            for virus__name, host__name, virus__identifier, host__identifier, raw_response in a \
                    .responseindatasource \
                    .values_list('virus__name', 'host__name', 'virus__identifier', 'host__identifier', 'raw_response') \
                    .order_by('virus__name', 'host__name'):
                self.assertTrue(
                    b.responseindatasource
                        .filter(virus__name=virus__name)
                        .filter(host__name=host__name)
                        .filter(virus__identifier=virus__identifier)
                        .filter(host__identifier=host__identifier)
                        .filter(raw_response=raw_response)
                        .exists()
                )

    def test_download_and_import_again_is_idempotent_with_ids(self):
        url = reverse('viralhostrangedb:data-source-download', args=[self.public_data_source_with_ids_of_toto.pk])
        response = self.client.get(url)
        f = NamedTemporaryFile(suffix=".xlsx", delete=False)
        f.write(response.content)
        f.close()
        data_source = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="reload",
            raw_name="ee",
            kind="FILE",
        )
        business_process.import_file(
            data_source=data_source,
            file=f.name,
        )
        virus_pks = set(self.public_data_source_with_ids_of_toto.virus_set.values_list('pk', flat=True))
        virus_pks_re_imported = set(data_source.virus_set.values_list('pk', flat=True))
        self.assertSetEqual(virus_pks, virus_pks_re_imported)
        host_pks = set(self.public_data_source_with_ids_of_toto.host_set.values_list('pk', flat=True))
        host_pks_re_imported = set(data_source.host_set.values_list('pk', flat=True))
        self.assertSetEqual(host_pks, host_pks_re_imported)
        for a, b in [
            (data_source, self.public_data_source_with_ids_of_toto),
            (self.public_data_source_with_ids_of_toto, data_source),
        ]:
            for virus__name, host__name, virus__identifier, host__identifier, raw_response in a \
                    .responseindatasource \
                    .values_list('virus__name', 'host__name', 'virus__identifier', 'host__identifier', 'raw_response') \
                    .order_by('virus__name', 'host__name'):
                self.assertTrue(
                    b.responseindatasource
                        .filter(virus__name=virus__name)
                        .filter(host__name=host__name)
                        .filter(virus__identifier=virus__identifier)
                        .filter(host__identifier=host__identifier)
                        .filter(raw_response=raw_response)
                        .exists(),
                    msg='For %s(%i) vs %s(%i):\n\t(%s (%s) ; %s (%s)):\n\t%f vs %s\nFile:%s' % (
                        a.name,
                        a.pk,
                        b.name,
                        b.pk,
                        virus__name,
                        virus__identifier,
                        host__name,
                        host__identifier,
                        raw_response,
                        ', '.join(
                            [str(r) for r in b.responseindatasource
                                .filter(virus__name=virus__name)
                                .filter(host__name=host__name)
                                .filter(virus__identifier=virus__identifier)
                                .filter(host__identifier=host__identifier)]),
                        f.name,
                    ),
                )
        os.unlink(f.name)

    def test_download_contains_raw_data_and_not_demapped(self):
        url = reverse('viralhostrangedb:data-source-download', args=[self.private_data_source_of_user_mapped.pk])
        self.client.force_login(self.user)
        self.private_data_source_of_user_mapped.raw_name = "eeaa.xlsx"
        self.private_data_source_of_user_mapped.save()
        r = self.private_data_source_of_user_mapped.responseindatasource.filter(raw_response=1.0).delete()
        response = self.client.get(url)
        with NamedTemporaryFile(suffix=".xlsx", delete=False) as f:
            f.write(response.content)
            f.close()
            data_source = models.DataSource.objects.create(
                owner=self.user,
                public=False,
                name="reload",
                raw_name="ee.xlsx",
                kind="FILE",
            )
            business_process.import_file(
                data_source=data_source,
                file=f.name,
            )
            os.unlink(f.name)
        for virus__name, host__name, raw_response in self.private_data_source_of_user_mapped \
                .responseindatasource \
                .values_list('virus__name', 'host__name', 'raw_response') \
                .order_by('virus__name', 'host__name'):
            self.assertTrue(
                data_source.responseindatasource
                    .filter(virus__name=virus__name)
                    .filter(host__name=host__name)
                    .filter(raw_response=raw_response)
                    .exists()
            )


class DataSourceRangeMappingEditTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[self.public_data_source_of_user.pk])
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_submit_works(self):
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)
        self.assertFalse(self.public_data_source_of_user.is_mapping_done)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 1,
            "mapping_for_%i_starts_with" % self.lysis.pk: 2,
        }
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(self.public_data_source_of_user.is_mapping_done)

    def test_wrong_submit_fails(self):
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[self.public_data_source_of_user.pk])
        self.client.force_login(self.user)
        self.assertFalse(self.public_data_source_of_user.is_mapping_done)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 2,
            "mapping_for_%i_starts_with" % self.lysis.pk: 1,
        }
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.public_data_source_of_user.is_mapping_done)


class DataSourceContactTestCase(ViewTestCase):
    def test_works(self):
        for ds in [self.private_data_source_of_toto.pk, self.public_data_source_of_user.pk]:
            self.client.logout()
            url = reverse('viralhostrangedb:data-source-contact', args=[ds])
            response = self.client.get(url)
            self.assertRedirects(
                response,
                expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
            )
            self.client.force_login(self.toto)
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)

    def test_submit_works(self):
        self.client.force_login(self.toto)
        url = reverse('viralhostrangedb:data-source-contact', args=[self.public_data_source_of_user.pk])
        form_data = dict(
            subject='mysub',
            body='mymsg',
        )
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(mail.outbox), 2)
        self.assertTrue(form_data['subject'] in mail.outbox[0].subject)
        self.assertTrue(form_data['body'] in mail.outbox[0].body)
        self.assertTrue(self.user.email not in mail.outbox[0].body)
        self.assertTrue(self.toto.email in mail.outbox[0].body)
        self.assertEqual(mail.outbox[0].recipients(), [self.user.email, ])

        self.assertTrue(form_data['subject'] in mail.outbox[1].subject)
        self.assertTrue(form_data['body'] in mail.outbox[1].body)
        self.assertTrue(self.user.email not in mail.outbox[1].body)
        self.assertTrue(self.toto.email in mail.outbox[1].body)
        self.assertEqual(mail.outbox[1].recipients(), [self.toto.email, ])

    def test_wrong_submit_fails(self):
        self.client.force_login(self.toto)
        url = reverse('viralhostrangedb:data-source-contact', args=[self.public_data_source_of_user.pk])
        form_data = dict(
            subject='',
            body='mymsg',
        )
        self.client.post(url, form_data)
        form_data = dict(
            subject='mysub',
            body='',
        )
        self.client.post(url, form_data)
        self.assertEqual(len(mail.outbox), 0)
