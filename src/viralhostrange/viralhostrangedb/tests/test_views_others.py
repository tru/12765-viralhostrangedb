import logging
import os
from tempfile import NamedTemporaryFile

import pandas as pd
from basetheme_bootstrap.user_preferences import get_user_preferences_for_user
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Q, F
from django.http import QueryDict
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from viralhostrangedb import models, business_process

logger = logging.getLogger(__name__)


class ViewTestCase(TestCase):
    test_data = "./test_data"

    # __public_big_data_source_of_user_mapped = None

    def write_in_tmp_file(self, response):
        with NamedTemporaryFile(delete=False, suffix=".html") as f:
            f.write(response.content)
            print(response.content)
            print(f.name)

    def setUp(self):
        # self.startTime = time.time()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"

        ################################################################################
        get_user_model().objects.create(
            username="tata",
        )

        ################################################################################
        get_user_model().objects.create(
            username="titi",
        )

        ################################################################################
        self.user = get_user_model().objects.create(
            username="user",
            email="b@a.a",
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

        ################################################################################
        self.staff_user = get_user_model().objects.create(
            username="staff_user",
            email="staff@a.a",
            is_staff=True,
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

        ################################################################################
        self.toto = get_user_model().objects.create(
            username="toto",
            email="a@a.a",
        )
        self.toto.set_password(self.user_pwd)
        self.toto.save()

        ################################################################################
        self.lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection")
        self.weak = models.GlobalViralHostResponseValue.objects_mappable().get(name="Intermediate")
        self.no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection")
        self.not_mapped_yet = models.GlobalViralHostResponseValue.get_not_mapped_yet()

        ################################################################################
        # set the pk to different starting value for each model to avoid
        models.Virus.objects.create(name="won't last", pk=0).delete()
        models.Host.objects.create(name="won't last", pk=100).delete()
        models.DataSource.objects.create(
            owner=self.toto,
            name="won't last",
            raw_name="won't last",
            pk=200,
            creation_date=timezone.now(),
        ).delete()

        ################################################################################
        self.private_data_source_of_user = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="private user",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.private_data_source_of_user,
            file=filename,
        )

        ################################################################################
        self.public_data_source_of_user = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="public user",
            raw_name="ee",
            kind="FILE",
        )
        self.public_data_source_of_user.allowed_users.add(self.user)
        self.public_data_source_of_user.save()
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        business_process.import_file(
            data_source=self.public_data_source_of_user,
            file=filename,
        )

        ################################################################################
        self.public_data_source_of_user_mapped = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="public user mapped",
            raw_name="ee",
            kind="FILE",
        )
        self.public_data_source_of_user_mapped.allowed_users.add(self.user)
        self.public_data_source_of_user_mapped.save()
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        business_process.import_file(
            data_source=self.public_data_source_of_user_mapped,
            file=filename,
        )

        url = reverse('viralhostrangedb:data-source-mapping-label-edit',
                      args=[self.public_data_source_of_user_mapped.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.public_data_source_of_user_mapped.is_mapping_done)

        form_data = {
            "form-TOTAL_FORMS": 3,
            "form-INITIAL_FORMS": 3,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-raw_response": 0.0,
            "form-0-mapping": self.no_lysis.pk,
            "form-1-raw_response": 1.0,
            "form-1-mapping": self.no_lysis.pk,
            "form-2-raw_response": 2.0,
            "form-2-mapping": self.lysis.pk,
        }

        self.client.post(url, form_data)
        self.client.get(url)
        self.client.logout()

        ################################################################################
        self.private_data_source_of_user_mapped = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="private user mapped",
            raw_name="ee",
            kind="FILE",
        )
        self.private_data_source_of_user_mapped.allowed_users.add(self.user)
        self.private_data_source_of_user_mapped.save()
        filename = os.path.join(self.test_data, "three_reponse_simple_2.xlsx")
        business_process.import_file(
            data_source=self.private_data_source_of_user_mapped,
            file=filename,
        )

        url = reverse('viralhostrangedb:data-source-mapping-label-edit',
                      args=[self.private_data_source_of_user_mapped.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.private_data_source_of_user_mapped.is_mapping_done)

        form_data = {
            "form-TOTAL_FORMS": 3,
            "form-INITIAL_FORMS": 3,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-raw_response": 0.0,
            "form-0-mapping": self.no_lysis.pk,
            "form-1-raw_response": 1.0,
            "form-1-mapping": self.weak.pk,
            "form-2-raw_response": 2.0,
            "form-2-mapping": self.lysis.pk,
        }

        self.client.post(url, form_data)
        self.client.logout()

        ################################################################################

        self.private_data_source_of_toto = models.DataSource.objects.create(
            owner=self.toto,
            public=False,
            name="private toto",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.private_data_source_of_toto,
            file=filename,
        )

        ################################################################################

        self.public_data_source_with_ids_of_toto = models.DataSource.objects.create(
            owner=self.toto,
            public=True,
            name="public with_ids",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "simple-with-id.xlsx")
        business_process.import_file(
            data_source=self.public_data_source_with_ids_of_toto,
            file=filename,
        )

        ################################################################################
        self.three_reponse_simple = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="three_reponse_simple mapped",
            raw_name="ee",
            kind="FILE",
        )
        business_process.import_file(
            data_source=self.three_reponse_simple,
            file=os.path.join(self.test_data, "three_reponse_simple.xlsx"),
        )

        url = reverse('viralhostrangedb:data-source-mapping-label-edit', args=[self.three_reponse_simple.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.three_reponse_simple.is_mapping_done)

        form_data = {
            "form-TOTAL_FORMS": 3,
            "form-INITIAL_FORMS": 3,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-raw_response": 0.0,
            "form-0-mapping": self.no_lysis.pk,
            "form-1-raw_response": 1.0,
            "form-1-mapping": self.weak.pk,
            "form-2-raw_response": 2.0,
            "form-2-mapping": self.lysis.pk,
        }

        self.client.post(url, form_data)
        self.client.logout()
        self.assertTrue(self.three_reponse_simple.is_mapping_done)

        ################################################################################
        self.three_reponse_simple_2 = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="three_reponse_simple_2 mapped",
            raw_name="ee",
            kind="FILE",
        )
        business_process.import_file(
            data_source=self.three_reponse_simple_2,
            file=os.path.join(self.test_data, "three_reponse_simple_2.xlsx"),
        )

        url = reverse('viralhostrangedb:data-source-mapping-label-edit', args=[self.three_reponse_simple_2.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.three_reponse_simple_2.is_mapping_done)

        form_data = {
            "form-TOTAL_FORMS": 3,
            "form-INITIAL_FORMS": 3,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-raw_response": 0.0,
            "form-0-mapping": self.no_lysis.pk,
            "form-1-raw_response": 1.0,
            "form-1-mapping": self.weak.pk,
            "form-2-raw_response": 2.0,
            "form-2-mapping": self.lysis.pk,
        }

        self.client.post(url, form_data)
        self.client.logout()
        self.assertTrue(self.three_reponse_simple_2.is_mapping_done)

        ################################################################################
        self.four_reponse_simple = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="four_reponse_simple mapped",
            raw_name="ee",
            kind="FILE",
        )
        business_process.import_file(
            data_source=self.four_reponse_simple,
            file=os.path.join(self.test_data, "four_reponse_simple.xlsx"),
        )

        url = reverse('viralhostrangedb:data-source-mapping-label-edit', args=[self.four_reponse_simple.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.four_reponse_simple.is_mapping_done)

        form_data = {
            "form-TOTAL_FORMS": 4,
            "form-INITIAL_FORMS": 4,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-raw_response": 0.0,
            "form-0-mapping": self.no_lysis.pk,
            "form-1-raw_response": 1.0,
            "form-1-mapping": self.weak.pk,
            "form-2-raw_response": 3.2,
            "form-2-mapping": self.lysis.pk,
            "form-3-raw_response": 4.0,
            "form-3-mapping": self.lysis.pk,
        }

        self.client.post(url, form_data)
        self.client.logout()
        self.assertTrue(self.four_reponse_simple.is_mapping_done)

        ################################################################################
        self.four_reponse_simple_not_mapped = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="four_reponse_simple not mapped",
            raw_name="ee",
            kind="FILE",
        )
        business_process.import_file(
            data_source=self.four_reponse_simple_not_mapped,
            file=os.path.join(self.test_data, "four_reponse_simple.xlsx"),
        )

        self.assertFalse(self.four_reponse_simple_not_mapped.is_mapping_done)

        ################################################################################

        self.private_data_source_of_toto_with_no_virus_or_host_shared = models.DataSource.objects.create(
            owner=self.toto,
            public=False,
            name="private toto no_virus_or_host_shared",
            raw_name="ee",
            kind="FILE",
        )
        self.ds_no_shearing = self.private_data_source_of_toto_with_no_virus_or_host_shared
        filename = os.path.join(self.test_data, "rxCx.xlsx")
        business_process.import_file(
            data_source=self.private_data_source_of_toto_with_no_virus_or_host_shared,
            file=filename,
        )

        self.C1 = models.Host.objects.get(name="C1", identifier="")
        self.C2 = models.Host.objects.get(name="C2", identifier="")
        self.C3 = models.Host.objects.get(name="C3", identifier="")
        self.C4 = models.Host.objects.get(name="C4", identifier="")
        self.Cx = models.Host.objects.get(name="Cx", identifier="")

        self.r1 = models.Virus.objects.get(name="r1", identifier="")
        self.r2 = models.Virus.objects.get(name="r2", identifier="")
        self.r3 = models.Virus.objects.get(name="r3", identifier="")
        self.rx = models.Virus.objects.get(name="rx", identifier="")

        self.client.logout()


class HomePageTestCase(ViewTestCase):
    def test_works(self):
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(reverse('viralhostrangedb:home'))
        self.assertEqual(response.status_code, 200)


class FileImportTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:file-import-view')
        response = self.client.get(url)
        os.path.isfile(os.path.join(self.test_data, "empty.xlsx.json.not_needed")) or self.write_in_tmp_file(response)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_import_work(self):
        url = reverse('viralhostrangedb:file-import-view')
        self.client.force_login(self.toto)

        f = open(os.path.join(self.test_data, "three_reponse_simple.xlsx"), "rb")

        form_data = dict(
            name='imported',
            description="Nop",
            public=True,
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )

        response = self.client.post(url, form_data, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(self.toto.datasource_set.filter(name="imported").count(), 1)
        self.assertEqual(self.toto.datasource_set.get(name="imported").responseindatasource.count(), 12)
        # self.assertNotEqual(self.public_big_data_source_of_user_mapped.responseindatasource.count(), 0)
        # self.assertTrue(self.public_big_data_source_of_user_mapped.is_mapping_done)

    def test_import_skip_empty_cell(self):
        url = reverse('viralhostrangedb:file-import-view')
        self.client.force_login(self.toto)

        f = open(os.path.join(self.test_data, "empty_row.xlsx"), "rb")

        form_data = dict(
            name='imported',
            description="Nop",
            public=True,
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )

        response = self.client.post(url, form_data, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(self.toto.datasource_set.filter(name="imported").count(), 1)
        self.assertEqual(self.toto.datasource_set.get(name="imported").responseindatasource.count(), 8)
        self.assertTrue(models.Virus.objects.filter(name="r2", data_source__name="imported").exists())
        self.assertFalse(models.Virus.objects.filter(name="r3", data_source__name="imported").exists())

        f = open(os.path.join(self.test_data, "empty_col.xlsx"), "rb")
        form_data = dict(
            name='imported2',
            description="Nop",
            public=True,
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )
        self.client.post(url, form_data, follow=True)
        self.assertEqual(self.toto.datasource_set.get(name="imported2").responseindatasource.count(), 9)
        self.assertTrue(models.Host.objects.filter(name="C2", data_source__name="imported2").exists())
        self.assertFalse(models.Host.objects.filter(name="C3", data_source__name="imported2").exists())

        f = open(os.path.join(self.test_data, "empty_cell_random.xlsx"), "rb")
        form_data = dict(
            name='imported3',
            description="Nop",
            public=True,
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )
        self.client.post(url, form_data, follow=True)
        self.assertEqual(self.toto.datasource_set.get(name="imported3").responseindatasource.count(), 7)

    def test_works_messages_raised(self):
        url = reverse('viralhostrangedb:file-import-view')
        self.client.force_login(self.toto)

        f = open(os.path.join(self.test_data, "trigger_importation_error_message.xlsx"), "rb")

        form_data = dict(
            name='imported',
            description="Nop",
            public=True,
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )

        response = self.client.post(url, form_data, follow=True)
        self.assertEqual(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 3)
        error_codes = set()
        for m in messages:
            parts = str(m).split("]")
            if len(parts) > 1:
                error_codes.add(parts[0][1:])
        self.assertSetEqual(error_codes, {"ImportErr1", "ImportErr2"})

    def test_works_messages_raised_with_too_long_virus(self):
        url = reverse('viralhostrangedb:file-import-view')
        self.client.force_login(self.toto)

        f = open(os.path.join(self.test_data, "too_long_virus_name.xlsx"), "rb")

        form_data = dict(
            name='imported',
            description="Nop",
            public=True,
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )

        response = self.client.post(url, form_data, follow=True)
        self.assertEqual(response.status_code, 200)
        messages = list(response.context['messages'])
        error_codes = set()
        for m in messages:
            parts = str(m).split("]")
            if len(parts) > 1:
                error_codes.add(parts[0][1:])
        self.assertSetEqual(error_codes, {"ImportErr3"})

    def test_works_messages_raised_with_too_long_host(self):
        url = reverse('viralhostrangedb:file-import-view')
        self.client.force_login(self.toto)

        f = open(os.path.join(self.test_data, "too_long_host_name.xlsx"), "rb")

        form_data = dict(
            name='imported',
            description="Nop",
            public=True,
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )

        response = self.client.post(url, form_data, follow=True)
        self.assertEqual(response.status_code, 200)
        messages = list(response.context['messages'])
        error_codes = set()
        for m in messages:
            parts = str(m).split("]")
            if len(parts) > 1:
                error_codes.add(parts[0][1:])
        self.assertSetEqual(error_codes, {"ImportErr1"})


class BrowseTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:browse')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_allow_overflow_works(self):
        url = reverse('viralhostrangedb:browse') + \
              "?user_pref=False&only_host_ncbi_id=true&only_virus_ncbi_id=true&allow_overflow=on"
        response_1 = self.client.get(url)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.get(url, follow=True)

        target_2 = response_1.redirect_chain[0][0]
        qd = QueryDict(target_2[target_2.index("?") + 1:])

        expected_ds = set(models.ViralHostResponseValueInDataSource.objects.filter(
            ~Q(virus__identifier="")
            & ~Q(host__identifier="")
            & Q(data_source__public=True)).values_list("data_source__pk", flat=True))
        expected_ds = set([str(i) for i in expected_ds])

        self.assertEqual(set(qd.getlist("ds")), expected_ds)

    def test_allow_overflow_works_with_auth(self):
        self.client.force_login(self.user)
        self.private_data_source_of_toto.allowed_users.add(self.user)
        self.private_data_source_of_toto.save()
        self.private_data_source_of_toto.virus_set.update(identifier="i")
        self.private_data_source_of_toto.host_set.update(identifier="i")
        url = reverse('viralhostrangedb:browse') + \
              "?user_pref=False&only_host_ncbi_id=true&only_virus_ncbi_id=true&allow_overflow=on"
        response_1 = self.client.get(url)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.get(url, follow=True)

        target_2 = response_1.redirect_chain[0][0]
        qd = QueryDict(target_2[target_2.index("?") + 1:])

        expected_ds = set(models.ViralHostResponseValueInDataSource.objects.filter(
            ~Q(virus__identifier="")
            & ~Q(host__identifier="")
            & Q(
                Q(data_source__public=True)
                | Q(data_source__owner=self.user)
                | Q(data_source__allowed_users=self.user)
            )).values_list("data_source__pk", flat=True))
        expected_ds = set([str(i) for i in expected_ds])

        self.assertEqual(set(qd.get("ds").split(',')), expected_ds)


class HostListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:host-list')
        self.client.logout()
        self.assertEqual(self.client.get(url).status_code, 200)
        self.client.force_login(self.toto)
        self.assertEqual(self.client.get(url).status_code, 200)


class VirusListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:virus-list')
        self.client.logout()
        self.assertEqual(self.client.get(url).status_code, 200)
        self.client.force_login(self.toto)
        self.assertEqual(self.client.get(url).status_code, 200)

    def test_private_selectively_shawn(self):
        url = reverse('viralhostrangedb:virus-list')

        self.client.logout()
        response = self.client.get(url)
        link = self.private_data_source_of_toto_with_no_virus_or_host_shared.get_absolute_url()
        self.assertFalse(link in str(response.content))
        link = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first().get_absolute_url()
        self.assertFalse(link in str(response.content))

        self.client.force_login(self.toto)
        response = self.client.get(url)
        link = self.private_data_source_of_toto_with_no_virus_or_host_shared.get_absolute_url()
        self.assertTrue(link in str(response.content))
        link = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first().get_absolute_url()
        self.assertTrue(link in str(response.content))


class BrowseDataSetTestCase(ViewTestCase):
    def test_works(self):
        response = self.client.get(
            reverse('viralhostrangedb:data-source-browse', args=[self.public_data_source_of_user.pk]))

        url = "%(url)s?ds=%(ds)s&use_pref=True" % dict(
            url=reverse("viralhostrangedb:browse"),
            ds=str(self.public_data_source_of_user.pk),
        )
        self.assertRedirects(
            response,
            expected_url=url
        )

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = "%(url)s?ds=%(ds)s" % dict(
            url=reverse("viralhostrangedb:browse"),
            ds=str(self.public_data_source_of_user.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_works_by_virus_and_host(self):
        response = self.client.get(
            reverse('viralhostrangedb:data-source-browse-by-virus-and-host', args=[self.public_data_source_of_user.pk]))

        url = "%(url)s?host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse("viralhostrangedb:browse"),
            hosts=",".join(str(i) for i in self.public_data_source_of_user.host_set.values_list('pk', flat=True)),
            viruses=",".join(str(i) for i in self.public_data_source_of_user.virus_set.values_list('pk', flat=True)),
        )
        self.assertRedirects(
            response,
            expected_url=url
        )

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = "%(url)s?ds=%(ds)s" % dict(
            url=reverse("viralhostrangedb:browse"),
            ds=str(self.public_data_source_of_user.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class CustomCSSTestCase(ViewTestCase):
    def test_works(self):
        url_400 = reverse('viralhostrangedb:custom_css', args=[""])
        url_500 = reverse('viralhostrangedb:custom_css', args=["erty"])
        url_200 = reverse('viralhostrangedb:custom_css', args=["0.7812"])
        response = self.client.get(url_400)
        self.assertEqual(response.status_code, 400)
        self.assertRaises(ValueError, self.client.get, url_500)
        response = self.client.get(url_200)
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(len(response.content), 0)
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url_400)
        self.assertEqual(response.status_code, 400)
        self.assertRaises(ValueError, self.client.get, url_500)
        response = self.client.get(url_200)
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(len(response.content), 0)


class VirusDetailViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:virus-detail', args=[self.rx.pk])
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.rx.identifier = "ee"
        self.rx.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_cross_users(self):
        url = reverse('viralhostrangedb:virus-detail', args=[self.rx.pk])
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_staff_can_see_private(self):
        url = reverse('viralhostrangedb:virus-detail', args=[self.rx.pk])
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_public_can_be_seen(self):
        url = reverse('viralhostrangedb:virus-detail', args=[self.r1.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class HostDetailViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:host-detail', args=[self.Cx.pk])
        response = self.client.get(url)
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + url
        )
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.Cx.identifier = "ee"
        self.Cx.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_cross_users(self):
        url = reverse('viralhostrangedb:host-detail', args=[self.Cx.pk])
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_staff_can_see_private(self):
        url = reverse('viralhostrangedb:host-detail', args=[self.Cx.pk])
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_public_can_be_seen(self):
        url = reverse('viralhostrangedb:host-detail', args=[self.C1.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class DownloadXXXDataTestCase(ViewTestCase):

    def actual_test(
            self,
            *,
            filename=None,
            user=None,
            ds=None,
            virus=None,
            host=None,
            virus_infection_ratio=False,
            host_infection_ratio=False,
            infection_ratio=False,
            weak_infection=False,
            agreed_infection=False,
            row_order=None,
            col_order=None,
            only_virus_ncbi_id=False,
            only_host_ncbi_id=False,
            only_published_data=False,
            allow_overflow=False,
            life_domain="",
            empty_with_legend=False,
            url=None,
    ):
        url = (url or self.url) + "?"
        if ds:
            url += "&".join(["ds=%i" % pk for pk in ds.values_list('pk', flat=True)])
        if virus:
            url += "&" + "&".join(["virus=%i" % pk for pk in virus.values_list('pk', flat=True)])
        if host:
            url += "&" + "&".join(["host=%i" % pk for pk in host.values_list('pk', flat=True)])
        if virus_infection_ratio:
            url += "&virus_infection_ratio=true"
        if host_infection_ratio:
            url += "&host_infection_ratio=true"
        if infection_ratio:
            url += "&infection_ratio=true"
        if weak_infection:
            url += "&weak_infection=true"
        if agreed_infection:
            url += "&agreed_infection=true"
        if row_order:
            url += "&row_order=%s" % ", ".join([str(i) for i in row_order])
        if col_order:
            url += "&col_order=%s" % ", ".join([str(i) for i in col_order])
        if only_virus_ncbi_id:
            url += "&only_virus_ncbi_id=true"
        if only_host_ncbi_id:
            url += "&only_host_ncbi_id=true"
        if only_published_data:
            url += "&only_published_data=true"
        if len(life_domain) > 0:
            url += "&life_domain=" + life_domain
        if allow_overflow:
            url += "&allow_overflow=true"
        self.client.logout()
        if user:
            self.client.force_login(user)
        response = self.client.get(url)
        try:
            with NamedTemporaryFile(suffix=".xlsx", delete=False) as f:
                f.write(response.content)
                f.close()
                if filename:
                    self.assertTrue(response.status_code == 200)
                    data_xls = pd.read_excel(f.name, index_col=None)
                    filename = os.path.join(self.test_data, self.file_location, filename)
                    data_xls.to_csv(filename + ".candidate", encoding='utf-8', index=False)
                    with open(filename, 'r') as expected, open(filename + ".candidate", 'r') as computed:
                        computed_content = computed.read()
                        expected_content = expected.read() or ""
                        self.assertEqual(
                            computed_content,
                            expected_content,
                            "The file produced is different to the one expected, please see %s, %s and %s" % (
                                f.name, filename + ".candidate", filename
                            )
                        )
                    os.unlink(filename + ".candidate")
                elif empty_with_legend:
                    data_xls = pd.read_excel(f.name, index_col=None)
                    filename = os.path.join(self.test_data, "download_responses_empty.csv")
                    data_xls.to_csv(f.name + ".csv", encoding='utf-8', index=False)
                    with open(filename, 'r') as expected, open(f.name + ".csv", 'r') as computed:
                        computed_content = computed.read()
                        expected_content = expected.read() or ""
                        self.assertEqual(
                            computed_content,
                            expected_content,
                            "The file produced is not an \"empty\" file (i.e: with no data), please see %s, %s and %s" % (
                                f.name, f.name + ".csv", filename
                            )
                        )
                else:
                    self.assertTrue(
                        not response.status_code < 400 and response.status_code < 500 or os.path.getsize(f.name) == 0,
                        "Either an error 400 or an empty file was expected but we did not get that")
                os.unlink(f.name)
        except Exception as e:
            print(response.status_code)
            print(url)
            try:
                with NamedTemporaryFile(suffix=".xlsx", delete=False) as f:
                    f.write(response.content)
                    f.close()
                print(f.name)
            except Exception:
                pass
            raise e


class DownloadBrowsedDataTestCase(DownloadXXXDataTestCase):
    url = reverse('viralhostrangedb:download_responses')
    file_location = "download_responses"

    def test_private_protected_works(self):
        self.actual_test(
            ds=models.DataSource.objects.filter(pk=self.private_data_source_of_user_mapped.pk),
        )

    def test_works(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk=self.private_data_source_of_user_mapped.pk),
            filename="three_reponse_simple_2.csv",
        )

    def test_with_two_files(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            filename="both_three_reponse_simple.csv",
        )

    def test_with_two_files_on_virus_subset(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            virus=self.three_reponse_simple.virus_set.filter(~Q(pk=self.three_reponse_simple.virus_set.first().pk)),
            filename="both_three_reponse_simple_on_virus_subset.csv",
        )

    def test_with_two_files_on_host_subset(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            host=self.three_reponse_simple.host_set.filter(~Q(pk=self.three_reponse_simple.host_set.first().pk)),
            filename="both_three_reponse_simple_on_host_subset.csv",
        )

    def test_with_two_files_on_both_subset(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            virus=self.three_reponse_simple.virus_set.filter(~Q(pk=self.three_reponse_simple.virus_set.last().pk)),
            host=self.three_reponse_simple.host_set.filter(~Q(pk=self.three_reponse_simple.host_set.last().pk)),
            filename="both_three_reponse_simple_on_both_subset.csv",
        )

    def test_with_two_files_and_virus_ratio(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            filename="both_three_reponse_simple_virus_ratio.csv",
            virus_infection_ratio=True,
            host_infection_ratio=False,
            weak_infection=False,
            agreed_infection=False,
        )

    def test_with_two_files_and_host_ratio(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            filename="both_three_reponse_simple_host_ratio.csv",
            virus_infection_ratio=False,
            host_infection_ratio=True,
            weak_infection=False,
            agreed_infection=False,
        )

    def test_with_two_files_and_both_ratio(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            filename="both_three_reponse_simple_both_ratio.csv",
            virus_infection_ratio=True,
            host_infection_ratio=True,
            weak_infection=False,
            agreed_infection=False,
        )

    def test_with_two_files_and_both_ratio_agreed(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            filename="both_three_reponse_simple_both_ratio_agreed.csv",
            virus_infection_ratio=True,
            host_infection_ratio=True,
            weak_infection=False,
            agreed_infection=True,
        )

    def test_with_two_files_and_both_ratio_weak(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            filename="both_three_reponse_simple_both_ratio_weak.csv",
            virus_infection_ratio=True,
            host_infection_ratio=True,
            weak_infection=True,
            agreed_infection=False,
        )

    def test_with_two_files_and_both_ratio_weak_agreed(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk__in=[
                self.three_reponse_simple.pk,
                self.three_reponse_simple_2.pk,
            ]),
            filename="both_three_reponse_simple_both_ratio_weak_agreed.csv",
            virus_infection_ratio=True,
            host_infection_ratio=True,
            weak_infection=True,
            agreed_infection=True,
        )

    def test_order_kept(self):
        self.actual_test(
            user=self.user,
            ds=models.DataSource.objects.filter(pk=self.private_data_source_of_user_mapped.pk),
            virus=models.Virus.objects.filter(data_source__pk=self.private_data_source_of_user_mapped.pk)
                .order_by("-name"),
            host=models.Host.objects.filter(data_source__pk=self.private_data_source_of_user_mapped.pk)
                .order_by("-name"),
            filename="three_reponse_simple_2_order.csv",
        )


class DownloadHostDataTestCase(DownloadXXXDataTestCase):
    file_location = "download_responses_host_virus"

    def setUp(self):
        super().setUp()
        self.four_reponse_simple_not_mapped.delete()
        self.private_data_source_of_user.delete()
        self.private_data_source_of_toto.delete()
        self.public_data_source_of_user.delete()

    def test_private_protected_works(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[
                self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first().pk]),
        )
        self.actual_test(
            user=self.user,
            url=reverse('viralhostrangedb:host-download', args=[
                self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first().pk]),
        )

    def test_works_1(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[self.C1.pk]),
            user=self.user,
            filename="C1_user.csv",
        )

    def test_works_2(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[self.C1.pk]),
            user=self.toto,
            filename="C1_toto.csv",
        )

    def test_works_order(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[self.C1.pk]),
            user=self.user,
            filename="C1_user_both-name.csv",
            col_order=self.C1.data_source.order_by(
                "-name").values_list("pk", flat=True),
            row_order=self.private_data_source_of_user_mapped.virus_set.order_by(
                "-name").values_list("pk", flat=True),
        )

    def test_works_order_row(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[self.C1.pk]),
            user=self.user,
            filename="C1_user_row-name.csv",
            row_order=self.private_data_source_of_user_mapped.virus_set.order_by(
                "-name").values_list("pk", flat=True),
        )

    def test_works_order_col(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[self.C1.pk]),
            user=self.user,
            filename="C1_user_col-name.csv",
            col_order=self.C1.data_source.order_by(
                "-name").values_list("pk", flat=True),
        )

    def test_with_infection_ratio_where_only_weak(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[self.C1.pk]),
            user=self.user,
            filename="C1_infection_ratio.csv",
            infection_ratio=True,
        )

    def test_with_infection_ratio_with_lysis(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[self.C4.pk]),
            user=self.user,
            filename="C4_infection_ratio.csv",
            infection_ratio=True,
        )

    def test_with_infection_ratio_where_only_weak_and_weak_infection(self):
        self.actual_test(
            url=reverse('viralhostrangedb:host-download', args=[self.C1.pk]),
            user=self.user,
            filename="C1_infection_ratio_weak.csv",
            infection_ratio=True,
            weak_infection=True,
        )


class DownloadVirusDataTestCase(DownloadXXXDataTestCase):
    file_location = "download_responses_host_virus"

    def test_private_protected_works(self):
        self.actual_test(
            url=reverse('viralhostrangedb:virus-download', args=[
                self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first().pk]),
        )
        self.actual_test(
            user=self.user,
            url=reverse('viralhostrangedb:virus-download', args=[
                self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first().pk]),
        )

    def test_works_1(self):
        self.actual_test(
            url=reverse('viralhostrangedb:virus-download', args=[self.r1.pk]),
            user=self.user,
            filename="r1_user.csv",
        )

    def test_works_2(self):
        self.actual_test(
            url=reverse('viralhostrangedb:virus-download', args=[self.r3.pk]),
            user=self.user,
            filename="r3_user.csv",
        )

    def test_works_order(self):
        self.actual_test(
            url=reverse('viralhostrangedb:virus-download', args=[self.r3.pk]),
            user=self.user,
            filename="r3_user_both-name.csv",
            col_order=self.r3.data_source.order_by(
                "-name").values_list("pk", flat=True),
            row_order=self.private_data_source_of_user_mapped.host_set.order_by(
                "-name").values_list("pk", flat=True),
        )

    def test_works_order_row(self):
        self.actual_test(
            url=reverse('viralhostrangedb:virus-download', args=[self.r3.pk]),
            user=self.user,
            filename="r3_user_row-name.csv",
            row_order=self.private_data_source_of_user_mapped.host_set.order_by(
                "-name").values_list("pk", flat=True),
        )

    def test_works_order_col(self):
        self.actual_test(
            url=reverse('viralhostrangedb:virus-download', args=[self.r3.pk]),
            user=self.user,
            filename="r3_user_col-name.csv",
            col_order=self.r3.data_source.order_by(
                "-name").values_list("pk", flat=True),
        )


class DownloadWholeDataTestCase(DownloadXXXDataTestCase):
    file_location = "download_whole_responses"
    url = reverse('viralhostrangedb:download_responses')

    def setUp(self):
        super().setUp()
        models.DataSource.objects.filter(~Q(pk__in=[
            self.public_data_source_of_user_mapped.pk,
            self.private_data_source_of_user_mapped.pk,
        ])).delete()
        models.Virus.objects.filter(~Q(name="r2")).update(identifier=F("name"))
        models.Host.objects.filter(~Q(name="C4")).update(identifier=F("name"))

    def test_with_infection_ratio(self):
        self.actual_test(
            user=self.user,
            virus_infection_ratio=True,
            host_infection_ratio=True,
            only_published_data=False,
            allow_overflow=True,
            filename="test_with_infection_ratio.csv",
        )

    def test_only_virus_ncbi_id(self):
        self.actual_test(
            user=self.user,
            only_virus_ncbi_id=True,
            virus_infection_ratio=True,
            host_infection_ratio=True,
            allow_overflow=True,
            filename="test_only_virus_ncbi_id.csv",
        )

    def test_only_host_ncbi_id(self):
        self.actual_test(
            user=self.user,
            only_host_ncbi_id=True,
            virus_infection_ratio=True,
            host_infection_ratio=True,
            allow_overflow=True,
            filename="test_only_host_ncbi_id.csv",
        )

    def test_only_ncbi_id(self):
        self.actual_test(
            user=self.user,
            only_virus_ncbi_id=True,
            only_host_ncbi_id=True,
            virus_infection_ratio=True,
            host_infection_ratio=True,
            allow_overflow=True,
            filename="test_only_ncbi_id.csv",
        )

    def test_without_infection_ratio(self):
        self.actual_test(
            user=self.user,
            allow_overflow=True,
            filename="test_without_infection_ratio.csv",
        )

    def test_with_host_infection_ratio_and_pref(self):
        p = get_user_preferences_for_user(self.user)
        p.virus_infection_ratio = True
        p.save()
        self.actual_test(
            user=self.user,
            host_infection_ratio=True,
            allow_overflow=True,
            filename="test_with_host_infection_ratio_and_pref.csv",
        )

    def test_only_published_data(self):
        self.actual_test(
            user=self.user,
            only_published_data=True,
            allow_overflow=True,
            empty_with_legend=True,
        )
        self.public_data_source_of_user_mapped.publication_url = "http://a.a"
        self.public_data_source_of_user_mapped.save()
        self.actual_test(
            user=self.user,
            only_published_data=True,
            allow_overflow=True,
            filename="test_only_published_data.csv",
        )
        self.private_data_source_of_user_mapped.publication_url = "http://a.a"
        self.private_data_source_of_user_mapped.save()
        self.actual_test(
            user=self.user,
            only_published_data=True,
            allow_overflow=True,
            filename="test_without_infection_ratio.csv",
        )

    def test_life_domain(self):
        self.actual_test(
            user=self.user,
            allow_overflow=True,
            filename="test_without_infection_ratio.csv",
            life_domain="bacteria",
        )

    def test_fails(self):
        self.assertRaises(
            Exception,
            self.actual_test,
            user=self.user,
            allow_overflow=True,
            filename="test_without_infection_ratio.csv",
            life_domain="bacteriaAAAA",
        )


class ResponseUpdateTestCase(ViewTestCase):

    def test_works(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse(
            'viralhostrangedb:response-update',
            kwargs=dict(
                ds_pk=self.private_data_source_of_user_mapped.pk,
                virus_pk=self.private_data_source_of_user_mapped.virus_set.first().pk,
                host_pk=self.private_data_source_of_user_mapped.host_set.first().pk,
            ),
        ))
        self.assertEqual(response.status_code, 200)

    def test_cross_users(self):
        self.client.force_login(self.toto)
        url = reverse(
            'viralhostrangedb:response-update',
            kwargs=dict(
                ds_pk=self.private_data_source_of_user_mapped.pk,
                virus_pk=self.private_data_source_of_user_mapped.virus_set.first().pk,
                host_pk=self.private_data_source_of_user_mapped.host_set.first().pk,
            ),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
        response = self.client.post(url, dict(raw_response=2, ))
        self.assertEqual(response.status_code, 404)
        self.client.logout()
        self.assertEqual(response.status_code, 404)
        response = self.client.post(url, dict(raw_response=2, ))
        self.assertEqual(response.status_code, 404)

    def test_public_anon_users(self):
        self.client.logout()
        url = reverse(
            'viralhostrangedb:response-update',
            kwargs=dict(
                ds_pk=self.public_data_source_of_user_mapped.pk,
                virus_pk=self.private_data_source_of_user_mapped.virus_set.first().pk,
                host_pk=self.private_data_source_of_user_mapped.host_set.first().pk,
            ),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
        response = self.client.post(url, dict(raw_response=2, ))
        self.assertEqual(response.status_code, 404)
        self.client.logout()
        self.assertEqual(response.status_code, 404)
        response = self.client.post(url, dict(raw_response=2, ))
        self.assertEqual(response.status_code, 404)

    def test_save_go_to_browse_when_mapping_ok(self):
        self.client.force_login(self.user)
        virus_pk = self.private_data_source_of_user_mapped.virus_set.first().pk
        host_pk = self.private_data_source_of_user_mapped.host_set.first().pk
        ds_pk = self.public_data_source_of_user_mapped.pk
        url = reverse(
            'viralhostrangedb:response-update',
            kwargs=dict(
                ds_pk=ds_pk,
                virus_pk=virus_pk,
                host_pk=host_pk,
            ),
        )
        self.assertEqual(0, models.ViralHostResponseValueInDataSource.objects.get(
            virus__pk=virus_pk, host__pk=host_pk, data_source__pk=ds_pk, ).raw_response)
        form_data = dict(raw_response=2, )

        response = self.client.post(url, form_data)
        self.assertRedirects(response, expected_url=
        "%(url)s?host=%(hosts)s&virus=%(viruses)s&ds=%(data_source)s" % dict(
            url=reverse("viralhostrangedb:browse"),
            viruses=virus_pk,
            hosts=host_pk,
            data_source=ds_pk,
        ))

    def test_save_go_to_edit_when_mapping_ko(self):
        self.client.force_login(self.user)
        virus_pk = self.private_data_source_of_user_mapped.virus_set.first().pk
        host_pk = self.private_data_source_of_user_mapped.host_set.first().pk
        ds_pk = self.public_data_source_of_user_mapped.pk
        url = reverse(
            'viralhostrangedb:response-update',
            kwargs=dict(
                ds_pk=ds_pk,
                virus_pk=virus_pk,
                host_pk=host_pk,
            ),
        )
        self.assertEqual(0, models.ViralHostResponseValueInDataSource.objects.get(
            virus__pk=virus_pk, host__pk=host_pk, data_source__pk=ds_pk, ).raw_response)
        form_data = dict(raw_response=20, )

        response = self.client.post(url, form_data)
        self.assertRedirects(response, reverse("viralhostrangedb:data-source-mapping-edit", args=[ds_pk]))


class SearchTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb:search') + "?search=privateprivateprivate"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        url = reverse('viralhostrangedb:search') + "?search="
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        url = reverse('viralhostrangedb:search')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class ProbeTestCase(TestCase):
    def test_is_pk(self):
        self.assertEqual(200, self.client.get(reverse('viralhostrangedb:probe_ready')).status_code)
        self.assertEqual(200, self.client.get(reverse('viralhostrangedb:probe_alive')).status_code)
