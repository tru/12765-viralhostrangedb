from django.conf.urls import url
from django.urls import path
from django.views.i18n import JavaScriptCatalog

from viralhostrangedb import views

data_source_wizard = views.DataSourceWizard.as_view(url_name='viralhostrangedb:data-source-create-step')

app_name = 'viralhostrangedb'
urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^import/data-source/$', views.file_import, name='file-import-view'),
    # url(r'^data_source/(?P<pk>\d+)/edit/$', views.file_import, name='data-source-edit'),
    url(
        r'^data-source/pending-mapping/$',
        views.DataSourceMappingPendingListView.as_view(),
        name='data-source-pending-mapping-list',
    ),
    url(r'^explore/$', views.browse, name='browse'),
    url(r'^explore/css/(?P<slug>[^/]*)$', views.custom_css, name='custom_css'),
    url(r'^download/$', views.download_responses, name='download_responses'),
    url(r'^data-source/$', views.DataSourceListView.as_view(), name='data-source-list'),
    url(r'^data-source/(?P<pk>\d+)/download/$', views.data_source_download, name='data-source-download'),
    url(r'^data-source/(?P<pk>\d+)/update/$', views.data_source_data_update, name='data-source-data-update'),
    url(r'^data-source/(?P<pk>\d+)/edit/$', views.DataSourceUpdateView.as_view(), name='data-source-update'),
    url(r'^data-source/(?P<pk>\d+)/virus/update/$', views.data_source_virus_update, name='data-source-virus-update'),
    url(r'^data-source/(?P<pk>\d+)/host/update/$', views.data_source_host_update, name='data-source-host-update'),
    url(r'^data-source/(?P<pk>\d+)/virus/delete/$', views.data_source_virus_delete, name='data-source-virus-delete'),
    url(r'^data-source/(?P<pk>\d+)/host/delete/$', views.data_source_host_delete, name='data-source-host-delete'),
    url(r'^data-source/(?P<pk>\d+)/delete/$', views.DataSourceDeleteView.as_view(), name='data-source-delete'),
    url(r'^data-source/(?P<pk>\d+)/$', views.DataSourceDetailView.as_view(), name='data-source-detail'),
    url(r'^data-source/(?P<pk>\d+)/explore/$', views.SelectDataFromDataSourceView.as_view(), name='data-source-browse'),
    url(r'^data-source/(?P<pk>\d+)/explore_by_virus_and_host/$', views.SelectVirusAndHostFromDataSourceView.as_view(),
        name='data-source-browse-by-virus-and-host'),
    url(r'^data-source/(?P<pk>\d+)/mapping/edit-as-label/$', views.data_source_mapping_edit,
        name='data-source-mapping-label-edit'),
    url(r'^data-source/(?P<pk>\d+)/mapping/edit/$', views.data_source_mapping_range_edit,
        name='data-source-mapping-edit'),
    url(r'^data-source-contribution/(?P<step>.+)/$', data_source_wizard, name='data-source-create-step'),
    url(r'^data-source-contribution/$', data_source_wizard, name='data-source-create'),
    url(r'^data-source/(?P<pk>\d+)/contact/$', views.contact_data_source_owner, name='data-source-contact'),

    url(r'^virus/$', views.VirusListView.as_view(), name='virus-list'),
    url(r'^virus/(?P<pk>\d+)/$', views.VirusDetailView.as_view(), name='virus-detail'),
    url(r'^virus/(?P<pk>\d+)/download/$', views.virus_download, name='virus-download'),

    url(r'^host/$', views.HostListView.as_view(), name='host-list'),
    url(r'^host/(?P<pk>\d+)/$', views.HostDetailView.as_view(), name='host-detail'),
    url(r'^host/(?P<pk>\d+)/download/$', views.host_download, name='host-download'),

    url(r'^response/ds-(?P<ds_pk>\d+)-virus-(?P<virus_pk>\d+)-host-(?P<host_pk>\d+)/update/$',
        views.response_update, name='response-update'),
    url(r'^search/$', views.search, name='search'),

    url(r'^probe_alive/$', views.is_alive, name='probe_alive'),
    url(r'^probe_ready/$', views.is_ready, name='probe_ready'),

    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),

]
