from django.conf.urls import url, include
from rest_framework import routers

from viralhostrangedb import views_api

app_name = 'viralhostrangedb-api'
router = routers.DefaultRouter()
router.register(r'virus', views_api.VirusViewSet)
router.register(r'host', views_api.HostViewSet)
router.register(r'data-source', views_api.DataSourceViewSet)

urlpatterns = [
    url(r'^aggregated-responses/$', views_api.AggregatedResponseViewSet.as_view(), name='aggregated-responses'),
    url(r'^responses/$', views_api.CompleteResponseViewSet.as_view(), name='responses'),
    url(r'^', include(router.urls)),
    url(r'^aggregated-infection-ratio/(?P<slug>[^/]*)/$',
        views_api.VirusInfectionRatioViewSet.as_view(),
        name='aggregated-infection-ratio-list'),
    url(r'^aggregated-infection-ratio/(?P<slug>[^/]*)/(?P<slug_pk>\d+)/$',
        views_api.VirusInfectionRatioViewSet.as_view(),
        name='aggregated-infection-ratio-detail'),
    url(r'^infection-ratio/(?P<slug>[^/]*)/(?P<slug_pk>\d+)/$',
        views_api.VirusInfectionRatioViewSet.as_view(data_source_aggregated=False),
        name='infection-ratio-detail'),
    url(r'^search/$', views_api.search, name='search'),

]
