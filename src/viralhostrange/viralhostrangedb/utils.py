def order_queryset_specifically(*, queryset, actual_order: dict):
    """
    From https://stackoverflow.com/a/51291817/2144569
    :param queryset: the queryset of objects to apply a specific order
    :param actual_order: a dict where key is pk, and value is the rank of the associated object
    :type dict
    :return: the queryset annotate such as the order will follow actual_order
    """
    from django.db.models import Case, When, Value, IntegerField
    if len(actual_order) == 0:
        return queryset
    return queryset.annotate(
        rank=Case(
            *[When(pk=pk, then=Value(actual_order[pk])) for pk in actual_order.keys()],
            default=Value(len(actual_order)),
            output_field=IntegerField(),
        ),
    ).order_by('rank')
