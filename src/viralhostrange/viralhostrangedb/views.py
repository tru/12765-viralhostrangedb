import itertools
import json
import os
import re
import tempfile

import pandas as pd
from basetheme_bootstrap.user_preferences import get_user_preferences_for_user
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.cache import cache
from django.core.files import storage
from django.core.files.temp import NamedTemporaryFile
from django.core.mail import EmailMultiAlternatives
from django.db import transaction
from django.db.models import Count, Q
from django.db.models.functions import Upper
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.decorators import method_decorator
from django.utils.html import strip_tags, escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ugettext
from django.views.decorators.cache import cache_page
from django.views.generic import ListView, DetailView, UpdateView, DeleteView
from formtools.wizard import views as wizard_views

from viralhostrangedb import forms, business_process, views_api
from viralhostrangedb import mixins
from viralhostrangedb import models
from viralhostrangedb.business_process import MessageImportationObserver
from viralhostrangedb.utils import order_queryset_specifically


def index(request):
    data_sources = mixins.only_public_or_granted_or_owned_queryset_filter(
        self=None,
        request=request,
        queryset=models.DataSource.objects_annotated_with_pending.filter(mapping_done=True),
    ).filter(Q(public=True) | ~Q(description=""))
    top_data_sources = data_sources.order_by("-last_edition_date")[0:4]
    context = dict(
        n=range(3),
        data_sources=data_sources,
        top_data_sources=top_data_sources,
    )
    return render(
        request=request,
        template_name='viralhostrangedb/index.html',
        context=context,
    )


@login_required
def file_import(request):
    if request.method == 'POST':
        form = forms.ImportDataSourceForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            data_source = form.save(
                owner=request.user,
                importation_observer=MessageImportationObserver(request=request),
            )
            url = reverse("viralhostrangedb:data-source-mapping-edit", args=[data_source.pk])
            messages.success(
                request=request,
                message=mark_safe(_("success_import%(url_data_source_name)s%(data_source_name)s") % dict(
                    data_source_name=data_source.name,
                    url_data_source_name=data_source.get_absolute_url(),
                )),
            )
            return HttpResponseRedirect(url)
    else:
        form = forms.ImportDataSourceForm()
    context = dict(
        title=_("Import a file in the databases"),
        form=form,
    )
    return render(
        request=request,
        template_name='basetheme_bootstrap/small_form_host.html',
        context=context,
    )


@login_required
def data_source_data_update(request, pk):
    data_source = get_object_or_404(models.DataSource, pk=pk)
    if request.user != data_source.owner:
        return HttpResponseForbidden()
    if request.method == 'POST':
        form = forms.UploadToUpdateDataSourceForm(data=request.POST, files=request.FILES, instance=data_source)
        if form.is_valid():
            data_source = form.save(importation_observer=MessageImportationObserver(request=request))
            if data_source.is_mapping_done:
                url = reverse("viralhostrangedb:data-source-detail", args=[data_source.pk])
                messages.success(
                    request=request,
                    message=mark_safe(_("""Successfully updated data source """)),
                )
            else:
                url = reverse("viralhostrangedb:data-source-mapping-edit", args=[data_source.pk])
                messages.success(
                    request=request,
                    message=mark_safe(_("success_import%(url_data_source_name)s%(data_source_name)s") % dict(
                        data_source_name=data_source.name,
                        url_data_source_name=data_source.get_absolute_url(),
                    )),
                )
            return HttpResponseRedirect(url)
    else:
        form = forms.UploadToUpdateDataSourceForm(instance=data_source)
    context = dict(
        title=_("Update the data source content of %s" % data_source.name),
        submit_text=_("Upload and overwrite former data"),
        form=form,
    )
    return render(
        request=request,
        template_name='basetheme_bootstrap/small_form_host.html',
        context=context,
    )


@login_required
def data_source_mapping_edit(request, pk):
    data_source = get_object_or_404(models.DataSource, pk=pk)
    if request.user != data_source.owner:
        return HttpResponseForbidden()
    if request.method == 'POST':
        form = forms.MappingFormSet(data=request.POST, files=request.FILES)
        if form.is_valid():
            with transaction.atomic():
                for mapping_choice in form.forms:
                    models.ViralHostResponseValueInDataSource.objects.filter(
                        data_source=data_source,
                        raw_response=mapping_choice.cleaned_data["raw_response"]
                    ).update(response=mapping_choice.cleaned_data["mapping"])
                    data_source.save()
            return HttpResponseRedirect(reverse("viralhostrangedb:data-source-detail", args=[data_source.pk]))

    else:
        initial = []
        for raw_response, response_mapped in data_source.get_mapping(ordered=True):
            initial.append(
                dict(
                    raw_response=raw_response,
                    mapping=response_mapped
                )
            )
        form = forms.MappingFormSet(initial=initial)
    if data_source.is_mapping_absent:
        title = _("Defining the mapping of data source \"%s\"") % data_source.name
    else:
        title = _("Updating the mapping of data source \"%s\"") % data_source.name
    context = dict(
        title=title,
        form=form,
        medium_width=True,
        formset_css_classes="mb-4 col-12 col-md-6 col-lg-4 col-xl-3",
    )
    return render(
        request=request,
        template_name='basetheme_bootstrap/form_host.html',
        context=context,
    )


@login_required
def data_source_mapping_range_edit(request, pk):
    data_source = get_object_or_404(models.DataSource, pk=pk)
    if request.user != data_source.owner:
        return HttpResponseForbidden()
    if request.method == 'POST':
        form = forms.RangeMappingForm(data_source=data_source, data=request.POST)
        if form.is_valid():
            with transaction.atomic():
                form.save()
                data_source.save()
            return HttpResponseRedirect(reverse("viralhostrangedb:data-source-detail", args=[data_source.pk]))

    else:
        form = forms.RangeMappingForm(data_source=data_source)
    raw_responses = models.ViralHostResponseValueInDataSource.objects \
        .filter(data_source=data_source) \
        .values_list('raw_response') \
        .distinct() \
        .order_by("raw_response") \
        .values_list('raw_response', flat=True)
    if data_source.is_mapping_absent:
        title = _("Defining the mapping of data source \"%s\"") % data_source.name
    else:
        title = _("Updating the mapping of data source \"%s\"") % data_source.name
    context = dict(
        object=data_source,
        title=title,
        form=form,
        medium_width=True,
        formset_css_classes="mb-4 col-12 col-md-6 col-lg-4 col-xl-3",
        mapping=models.GlobalViralHostResponseValue.objects_mappable().order_by("value"),
        raw_responses=sorted(list(raw_responses)),
    )
    return render(
        request=request,
        template_name='viralhostrangedb/mapping_range_edit.html',
        context=context,
    )


class DataSourceDetailView(mixins.OnlyPublicOrGrantedOrStaffOrOwnedMixin, DetailView):
    model = models.DataSource

    def get_context_data(self, **kwargs):
        has_responses = self.object.responseindatasource.exists()
        example_table = DataSourceWizard.get_decorated_message_with_table_example(
            ugettext("DataSourceDetailView_how_to_fill_help%(scheme)s") % dict(scheme=DataSourceWizard.compute_scheme())
        )
        return super().get_context_data(
            has_responses=has_responses,
            example_table=example_table,
        )


class VirusListView(mixins.OnlyPublicOrGrantedOrStaffOrOwnedRelatedMixin, ListView):
    model = models.Virus
    template_name = "viralhostrangedb/vh_list.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["page_title"] = self.model._meta.verbose_name.title()
        context["title"] = self.model._meta.verbose_name.title()
        context["search_bar_value"] = self.request.GET.get("search", "")
        return context


class HostListView(VirusListView):
    model = models.Host


class VirusDetailView(mixins.OnlyPublicOrGrantedOrStaffOrOwnedRelatedMixin, DetailView):
    model = models.Virus
    template_name = "viralhostrangedb/vh_detail.html"

    def get_context_data(self, **kwargs):
        pref = get_user_preferences_for_user(self.request.user)
        context = super().get_context_data(**kwargs)
        data_sources = mixins.only_public_or_granted_or_owned_queryset_filter(
            self=None,
            request=self.request,
            queryset=self.object.data_source
        )
        context["data_sources"] = data_sources
        context["in_row_kind"] = _("Host")
        context["detailed_kind"] = _("Virus")
        context["focus_is_on_first_level_of_data"] = True
        context["get_detail_api_url"] = reverse("viralhostrangedb-api:host-detail", args=["000000"])
        context["get_detail_url"] = reverse("viralhostrangedb:host-detail", args=["000000"])
        context["download_url"] = reverse("viralhostrangedb:virus-download", args=[self.object.pk])
        context["schema"] = models.GlobalViralHostResponseValue.objects_mappable().order_by("value")
        # context["object_update"] = reverse("viralhostrangedb:virus-update", args=[self.object.pk])
        context["form"] = forms.VirusHostDetailViewForm(
            pref=pref,
            data=self.request.GET,
        )
        context["browse_url"] = reverse("viralhostrangedb:browse") + "?use_pref=True&virus=%i" % self.object.pk
        return context


class HostDetailView(mixins.OnlyPublicOrGrantedOrStaffOrOwnedRelatedMixin, DetailView):
    model = models.Host
    template_name = "viralhostrangedb/vh_detail.html"

    def get_context_data(self, **kwargs):
        pref = get_user_preferences_for_user(self.request.user)
        context = super().get_context_data(**kwargs)
        data_sources = mixins.only_public_or_granted_or_owned_queryset_filter(
            self=None,
            request=self.request,
            queryset=self.object.data_source
        )
        context["data_sources"] = data_sources
        context["in_row_kind"] = _("Virus")
        context["detailed_kind"] = _("Host")
        context["focus_is_on_first_level_of_data"] = False
        context["get_detail_api_url"] = reverse("viralhostrangedb-api:virus-detail", args=["000000"])
        context["get_detail_url"] = reverse("viralhostrangedb:virus-detail", args=["000000"])
        context["download_url"] = reverse("viralhostrangedb:host-download", args=[self.object.pk])
        context["schema"] = models.GlobalViralHostResponseValue.objects_mappable().order_by("value")
        # context["object_update"] = reverse("viralhostrangedb:host-update", args=[self.object.pk])
        context["form"] = forms.VirusHostDetailViewForm(
            pref=pref,
            data=self.request.GET,
        )
        context["browse_url"] = reverse("viralhostrangedb:browse") + "?use_pref=True&host=%i" % self.object.pk
        return context


class DataSourceUpdateView(mixins.OnlyOwnedMixin, UpdateView):
    model = models.DataSource
    form_class = forms.DataSourceUserCreateOrUpdateForm
    template_name = 'basetheme_bootstrap/small_form_host.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["page_title"] = _("Editing a data source")
        context["title"] = _("Editing a data source")
        context["submit_text"] = _("Update the data source")
        context["medium_width"] = True
        context["extra_js_file"] = "/js/data_source_update.js"
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(owner=self.request.user)
        return kwargs


# @method_decorator(login_required, name='dispatch')
# class DataSourceCreateView(mixins.OnlyOwnedMixin, CreateView):
#     login_required = True
#     model = models.DataSource
#     form_class = forms.DataSourceUserCreateOrUpdateForm
#     template_name = 'basetheme_bootstrap/small_form_host.html'
#
#     def get_form_kwargs(self):
#         kwargs = super().get_form_kwargs()
#         kwargs.update({'owner': self.request.user})
#         return kwargs
#
#     def get_context_data(self, *, object_list=None, **kwargs):
#         context = super().get_context_data(object_list=object_list, **kwargs)
#         context["page_title"] = _("Creating a data source")
#         context["title"] = _("Creating a data source")
#         context["submit_text"] = _("Create the data source")
#         return context


class DataSourceDeleteView(mixins.OnlyOwnedMixin, DeleteView):
    model = models.DataSource
    form_class = forms.DataSourceUserCreateOrUpdateForm
    template_name = 'basetheme_bootstrap/small_form_host.html'
    success_url = reverse_lazy('viralhostrangedb:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page_title = mark_safe('%s "%s" <small><i>created on %s</i></small>' % (
            _("Data source"),
            self.object.name,
            self.object.creation_date.strftime('%Y-%m-%d %H:%M:%S %Z')
        ))
        context.update(dict(
            page_title=page_title,
            title=_("Deleting a data source"),
            submit_text=_('Delete data source and all depending data'),
            medium_width=True,
            btn_classes='btn btn-lg btn-danger text-center',
            btn_cancel_classes='btn btn-lg btn-primary text-center',
            cancel_url=self.object.get_absolute_url(),
        ))
        return context

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        models.ViralHostResponseValueInDataSource.objects.filter(data_source=obj).delete()
        host_pk = list(obj.host_set.values_list('pk', flat=True))
        virus_pk = list(obj.virus_set.values_list('pk', flat=True))
        models.Virus.objects \
            .filter(pk__in=virus_pk) \
            .annotate(cpt=Count('data_source')) \
            .filter(cpt=1) \
            .delete()
        models.Host.objects \
            .filter(pk__in=host_pk) \
            .annotate(cpt=Count('data_source')) \
            .filter(cpt=1) \
            .delete()
        return super().delete(request=request, *args, **kwargs)


class DataSourceListView(mixins.OnlyPublicOrGrantedOrOwnedMixin, ListView):
    model = models.DataSource
    queryset = models.DataSource.objects_annotated_with_pending

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["page_title"] = _("Data sources")
        context["title"] = _("Data sources")
        context["search_bar_value"] = self.request.GET.get("search", "")
        return context


class DataSourceMappingPendingListView(LoginRequiredMixin, DataSourceListView):
    def get_queryset(self):
        return super().get_queryset().filter(mapping_done=False)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["page_title"] = _("Data sources with mapping not completed")
        context["title"] = _("Data sources with mapping not completed")
        return context


class SelectDataFromDataSourceView(mixins.OnlyPublicOrGrantedOrOwnedMixin, DetailView):
    model = models.DataSource

    def get(self, request, *args, **kwargs):
        data_source = self.get_object()
        return redirect("%(url)s?ds=%(data_sources)s&use_pref=True" % dict(
            url=reverse("viralhostrangedb:browse"),
            data_sources=str(data_source.pk)
        ), permanent=False, )


class SelectVirusAndHostFromDataSourceView(mixins.OnlyPublicOrGrantedOrOwnedMixin, DetailView):
    model = models.DataSource

    def get(self, request, *args, **kwargs):
        data_source = self.get_object()
        viruses = ",".join(str(i) for i in data_source.virus_set.values_list('pk', flat=True))
        hosts = ",".join(str(i) for i in data_source.host_set.values_list('pk', flat=True))
        return redirect("%(url)s?host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse("viralhostrangedb:browse"),
            viruses=viruses,
            hosts=hosts,
        ), permanent=False, )


def data_source_virus_update(request, pk):
    return data_source_entry_update(
        request,
        pk,
        entry_class=models.Virus,
        formset_class=forms.UpdateVirusFormSet,
        title=_("Update viruses present in a data source"),
        form_title=_("update_virus_sub_title"),
        submit_text=_("Save changes"),
    )


def data_source_host_update(request, pk):
    return data_source_entry_update(
        request,
        pk,
        entry_class=models.Host,
        formset_class=forms.UpdateHostFormSet,
        title=_("Update hosts present in a data source"),
        form_title=_("update_host_sub_title"),
        submit_text=_("Save changes"),
    )


@login_required
def data_source_entry_update(request, pk, entry_class, formset_class, title, form_title, submit_text):
    data_source = get_object_or_404(models.DataSource, pk=pk)
    if request.user != data_source.owner:
        return HttpResponseForbidden()
    if request.method == 'POST':
        formset = formset_class(
            data=request.POST,
            queryset=entry_class.objects.filter(data_source__pk=pk),
            data_source=data_source,
        )
        if formset.is_valid():
            try:
                with transaction.atomic():
                    formset.save()
                    data_source.save()
            except Exception as e:
                messages.error(request=request, message=e)
            else:
                return redirect(reverse("viralhostrangedb:data-source-detail", args=[pk]))
    else:
        formset = formset_class(
            queryset=entry_class.objects.filter(data_source__pk=pk)
        )
    context = dict(
        title=title,
        form_title=form_title,
        form=formset,
        medium_width=True,
        formset_css_classes="mb-4 col-12 col-md-6 col-lg-4 col-xl-3",
        submit_text=submit_text,
    )
    return render(
        request=request,
        template_name='basetheme_bootstrap/form_host.html',
        context=context,
    )


def data_source_virus_delete(request, pk):
    return data_source_entry_delete(
        request,
        pk,
        entry_class=models.Virus,
        formset_class=forms.DeleteVirusFormSet,
        entry_lower="virus",
        title=_("Delete viruses from a data source"),
        form_title=_("delete_virus_sub_title"),
        submit_text=_("Delete selected viruses"),
    )


def data_source_host_delete(request, pk):
    return data_source_entry_delete(
        request,
        pk,
        entry_class=models.Host,
        formset_class=forms.DeleteHostFormSet,
        entry_lower="host",
        title=_("Delete hosts from a data source"),
        form_title=_("delete_host_sub_title"),
        submit_text=_("Delete selected hosts"),
    )


@login_required
def data_source_entry_delete(request, pk, entry_class, formset_class, entry_lower, title, form_title, submit_text):
    data_source = get_object_or_404(models.DataSource, pk=pk)
    if request.user != data_source.owner:
        return HttpResponseForbidden()
    if request.method == 'POST':
        formset = formset_class(
            data=request.POST,
            queryset=entry_class.objects.filter(data_source__pk=pk)
        )
        if formset.is_valid():
            try:
                with transaction.atomic():
                    entries_to_delete = []
                    entries_to_remove_for_this_dataset = []
                    for form in formset:
                        if form.cleaned_data["DELETE"]:
                            entry = form.cleaned_data["id"]
                            if entry.data_source.count() == 1:
                                entries_to_delete.append(entry)
                            else:
                                entry.data_source.remove(data_source)
                                entry.save()
                            entries_to_remove_for_this_dataset.append(entry)

                    entries_pk = [v.pk for v in
                                  itertools.chain(entries_to_remove_for_this_dataset, entries_to_delete, )]
                    models.ViralHostResponseValueInDataSource.objects \
                        .filter(data_source__pk=pk) \
                        .filter(**{entry_lower + "__pk__in": entries_pk}) \
                        .delete()
                    entry_class.objects \
                        .filter(pk__in=[v.pk for v in entries_to_delete]) \
                        .delete()
                    data_source.save()
            except Exception as e:
                messages.error(request=request, message=e)
            else:
                return redirect(reverse("viralhostrangedb:data-source-detail", args=[pk]))
    else:
        formset = formset_class(
            queryset=entry_class.objects.filter(data_source__pk=pk)
        )
    context = dict(
        title=title,
        form_title=form_title,
        form=formset,
        medium_width=True,
        formset_css_classes="mb-4 col-12 col-md-6 col-lg-4 col-xl-3",
        submit_text=submit_text,
        btn_classes="btn btn-danger",
    )
    return render(
        request=request,
        template_name='basetheme_bootstrap/form_host.html',
        context=context,
    )


def browse(request):
    request.GET = request.GET.copy()
    if request.GET.get("allow_overflow") \
            and "virus" not in request.GET \
            and "host" not in request.GET \
            and "ds" not in request.GET:
        request.GET["format"] = "json"
        response = views_api.DataSourceViewSet.as_view(actions={'get': 'list'})(request)
        request.GET["ds"] = ",".join([str(d["id"]) for d in json.loads(response.rendered_content.decode('utf-8'))])
        del request.GET["format"]
        return HttpResponseRedirect(reverse('viralhostrangedb:browse') + "?" + request.GET.urlencode(safe=','))
    for k in ["virus", "host", "ds"]:
        try:
            request.GET.setlist(k, request.GET[k].split(","))
        except MultiValueDictKeyError:
            pass
    context = dict(
        form=forms.BrowseForm(
            user=request.user,
            data=request.GET if len(request.GET) > 0 else None,
        ),
        schema=models.GlobalViralHostResponseValue.objects_mappable().order_by("value"),
    )
    return render(
        request=request,
        template_name='viralhostrangedb/browse.html',
        context=context,
    )


@cache_page((1 if settings.DEBUG else 60 * 60 * 24))
def custom_css(request, slug):
    if slug == "":
        return HttpResponse(status=400)
    css = ""
    value = slug
    slug = slug.replace(".", "-")
    value = float(value)

    hue, sat, lgt = models.GlobalViralHostResponseValue.get_hsv_color_for(value)

    # default color
    s = 0.6
    css += """
.use-global-scheme .schema-%(slug)s,
.popover .schema-%(slug)s,
#grid_container:not(.only-disagree) .schema-%(slug)s,
#grid_container.only-disagree .disagree.schema-%(slug)s{
  background: hsla(%(hue)s, %(sat)s%%, %(lgt)s%%);vertical-align : middle;
} """ % dict(
        slug=slug,
        hue=hue,
        sat=sat,
        lgt=lgt * s + 100 * (1 - s),
    )

    # when focusing on disagreements
    s = 0.2
    css += """
#grid_container.only-disagree .schema-%(slug)s{
  background: hsla(%(hue)s, %(sat)s%%, %(lgt)s%%);;
} """ % dict(
        slug=slug,
        hue=hue,
        sat=sat,
        lgt=lgt * s + 100 * (1 - s),
    )

    # when hovering and NOT focusing on disagreements
    s = 1
    css += """
#grid_container:not(.only-disagree) tr:hover .schema-%(slug)s,
#grid_container:not(.only-disagree) .hover.schema-%(slug)s,
#grid_container.only-disagree tr:hover .disagree.schema-%(slug)s,
#grid_container.only-disagree .disagree.hover.schema-%(slug)s{
  background: hsla(%(hue)s, %(sat)s%%, %(lgt)s%%);;
} """ % dict(
        slug=slug,
        hue=hue,
        sat=sat,
        lgt=lgt * s + 100 * (1 - s),
    )

    # when hovering and focusing on disagreements
    s = 0.8
    css += """
#grid_container.only-disagree tr:hover .schema-%(slug)s,
#grid_container.only-disagree .hover.schema-%(slug)s{
  background: hsla(%(hue)s, %(sat)s%%, %(lgt)s%%);;
} """ % dict(
        slug=slug,
        hue=hue,
        sat=sat,
        lgt=lgt * s + 100 * (1 - s),
    )

    return HttpResponse(css, content_type="text/css")


def panda_color_mapping(v):
    key = 'html_color_%s' % str(v)
    color = cache.get(key)
    if color is not None:
        return color
    if v is not None and (isinstance(v, float) or isinstance(v, int)):
        try:
            color = 'background-color:' + models.GlobalViralHostResponseValue.get_html_color_for(v)
            cache.set(key, color, 60)
            return color
        except Exception:
            pass
    cache.set(key, '', 60)
    return ''


def panda_legend_color_mapping(v):
    # try:
    return panda_color_mapping(models.GlobalViralHostResponseValue.objects.get(name=v).value)
    # except ValueError: # No ValueError can be raised
    #     return ""


def download_responses(request):
    form = forms.BrowseForm(
        user=request.user,
        data=request.GET if request.method == 'GET' else None,
    )
    if not form.is_valid():
        return HttpResponseBadRequest(content=str(form.errors))
    request.GET = request.GET.copy()
    request.GET['format'] = 'json'

    # Get the aggregated data
    response = views_api.AggregatedResponseViewSet.as_view()(request)
    aggregated_responses = json.loads(response.rendered_content.decode('utf-8'))
    virus = form.cleaned_data["virus"]
    if virus.exists():
        virus = order_queryset_specifically(
            queryset=virus,
            actual_order=dict((int(o), i) for i, o in enumerate(request.GET.getlist("virus"))),
        )
    else:
        virus = models.Virus.objects.filter(
            pk__in=aggregated_responses.keys()
        ).order_by('pk')
    host = form.cleaned_data["host"]
    if host.exists():
        host = order_queryset_specifically(
            queryset=host,
            actual_order=dict((int(o), i) for i, o in enumerate(request.GET.getlist("host"))),
        )
    else:
        host = models.Host.objects.filter(
            pk__in=itertools.chain(*[d.keys() for d in aggregated_responses.values()])
        ).order_by('pk')

    # get if the data will be shifted because of the infection ratio
    host_infection_ratio_shift = 1 if form.cleaned_data["host_infection_ratio"] else 0
    virus_infection_ratio_shift = 1 if form.cleaned_data["virus_infection_ratio"] else 0

    col_header = [o.explicit_name for o in host]
    row_header = [o.explicit_name for o in virus]

    if host_infection_ratio_shift == 1:
        row_header.insert(0, _("Infection ratio"))
    if virus_infection_ratio_shift == 1:
        col_header.insert(0, _("Infection ratio"))

    # get at which position each host is, mandatory if data are sparse
    col_pos = dict([(pk, i) for i, pk in enumerate(
        host.values_list('pk', flat=True),
        start=virus_infection_ratio_shift,
    )])
    row_pos = dict([(pk, i) for i, pk in enumerate(
        virus.values_list('pk', flat=True),
        start=host_infection_ratio_shift,
    )])

    data = []
    for i in range(0, len(row_header)):
        data.append([""] * len(col_header))
    for virus_pk, host_dict in aggregated_responses.items():
        for host_pk, response_dict in host_dict.items():
            data[row_pos[int(virus_pk)]][col_pos[int(host_pk)]] = response_dict["val"]

    # Get the virus infection ratio if selected
    if virus_infection_ratio_shift == 1:
        response = views_api.VirusInfectionRatioViewSet.as_view()(request=request, slug="virus")
        virus_infection_ratio = json.loads(response.rendered_content.decode('utf-8'))
        for o in virus:
            data[row_pos[o.pk]][0] = virus_infection_ratio[str(o.pk)]['ratio'] * 100

    # Get the host infection ratio if selected
    if host_infection_ratio_shift == 1:
        response = views_api.VirusInfectionRatioViewSet.as_view()(request=request, slug="host")
        host_infection_ratio = json.loads(response.rendered_content.decode('utf-8'))
        for o in host:
            data[0][col_pos[o.pk]] = host_infection_ratio[str(o.pk)]['ratio'] * 100

    # Prepare the legend
    mapping = models.GlobalViralHostResponseValue.objects_mappable().order_by('value')
    legend_name = []
    legend_value = []
    legend = []
    for m in models.GlobalViralHostResponseValue.objects_mappable().order_by('value'):
        legend_name.append([m.name])
        legend_value.append(m.value)
        legend.append([m.name, m.value])
    df_legend = pd.DataFrame(legend_name, columns=[str(_("Legend"))], index=legend_value)

    # Prepare the setting help of the infection ratio
    options = []
    if form.cleaned_data["agreed_infection"]:
        options.append([form.fields["agreed_infection"].label, ])
    if form.cleaned_data["weak_infection"]:
        options.append([form.fields["weak_infection"].label, ])
    df_options = pd.DataFrame(options, columns=[str(_("Infection ratio settings"))], index=None)

    with NamedTemporaryFile(suffix=".xlsx") as f:
        # write in a temp file
        with pd.ExcelWriter(f.name) as writer:
            df_data = pd.DataFrame(data, columns=col_header, index=row_header)
            df_data = df_data.style.applymap(func=panda_color_mapping)
            df_data.to_excel(writer)

            df_legend = df_legend.style.applymap(func=panda_legend_color_mapping)
            df_legend.to_excel(writer, startcol=0, startrow=len(row_header) + 3)
            if len(options) > 0:
                df_options.to_excel(writer, startcol=0, startrow=len(row_header) + 3 + len(legend_name) + 3, index=None)

        # then read the temp file and send it to the client
        with open(f.name, "rb") as excel:
            response = HttpResponse(excel.read(),
                                    content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            # Translators: it is the beginning of a file name
            raw_filename = _("ViralHostRange browsed data on ") + timezone.now().strftime('%Y%m%d %Hh%M.%S') + ".xlsx"
            response['Content-Disposition'] = 'attachment; filename="%s"' % raw_filename
            return response


def data_source_download(request, pk):
    data_source = get_object_or_404(mixins.only_public_or_granted_or_owned_queryset_filter(
        self=None,
        request=request,
        queryset=models.DataSource.objects,
    ), pk=pk)
    mapping = dict([(raw, response.value) for raw, response in data_source.get_mapping(only_pk=False)])

    # define once and for all how we order the entry, to avoid bug in future refactoring
    def order_hv(qs, prefixes=None):
        prefixes = prefixes or [""]
        return qs.order_by(*[Upper(p + field) for p, field in itertools.product(prefixes, ['name', 'identifier', ])])

    # get virus and host, ordered by pk (i.e order of import, thus order liked by file owner)
    col_header = [o.explicit_name for o in order_hv(data_source.host_set)]
    row_header = [o.explicit_name for o in order_hv(data_source.virus_set)]
    # get at which position each host is, useful when data are sparse
    col_pos = dict([(pk, i) for i, pk in enumerate(order_hv(data_source.host_set).values_list('pk', flat=True))])
    row_pos = dict([(pk, i) for i, pk in enumerate(order_hv(data_source.virus_set).values_list('pk', flat=True))])

    old_virus_id = None
    row_raw_content = None
    row_mapped_content = None
    data_raw = []
    data_mapped = []
    # get all responses, ordered by virus_id and then host_id to fill the matrix ideally from left to right, and then
    # top to bottom.
    for virus_id, host_id, raw_response in order_hv(data_source.responseindatasource, prefixes=["virus__", "host__"]) \
            .values_list('virus_id', 'host_id', 'raw_response'):
        if old_virus_id != virus_id:
            # new virus, thus new line in the matrix
            old_virus_id = virus_id
            for i in range(len(data_raw), row_pos[virus_id] + 1):
                # if some row are empty, creating empty array to reflect it
                row_raw_content = [""] * len(col_header)
                data_raw.append(row_raw_content)
                row_mapped_content = [""] * len(col_header)
                data_mapped.append(row_mapped_content)

        col_id = col_pos[host_id]
        # set the response in the right column (useful when data are sparse)
        row_raw_content[col_id] = raw_response
        row_mapped_content[col_id] = mapping.get(raw_response, "")

    with NamedTemporaryFile(suffix=".xlsx") as f:
        # write in a temp file
        with pd.ExcelWriter(f.name) as writer:
            pd.DataFrame(data_raw,
                         index=row_header,
                         columns=col_header) \
                .to_excel(writer, sheet_name='raw_responses')

            df_data = pd.DataFrame(data_mapped, columns=col_header, index=row_header)
            df_data = df_data.style.applymap(func=panda_color_mapping)
            df_data.to_excel(writer, sheet_name='mapped_responses')
        # then read the temp file and send it to the client
        with open(f.name, "rb") as excel:
            response = HttpResponse(excel.read(),
                                    content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            raw_filename = data_source.raw_filename
            if raw_filename.endswith(".xlsx"):
                raw_filename = raw_filename[:-5]
            raw_filename += "." + timezone.now().strftime('%Y%m%d %Hh%M.%S') + ".xlsx"
            response['Content-Disposition'] = 'attachment; filename="%s"' % raw_filename
            return response


def virus_download(request, pk):
    instance = get_object_or_404(mixins.only_public_or_granted_or_staff_or_owned_queryset_filter(
        self=None,
        request=request,
        queryset=models.Virus.objects,
        path_to_data_source="data_source__",
    ), pk=pk)
    return host_or_virus_download(request=request, is_host=False, instance=instance)


def host_download(request, pk):
    instance = get_object_or_404(mixins.only_public_or_granted_or_staff_or_owned_queryset_filter(
        self=None,
        request=request,
        queryset=models.Host.objects,
        path_to_data_source="data_source__",
    ), pk=pk)
    return host_or_virus_download(request=request, is_host=True, instance=instance)


def host_or_virus_download(request, is_host, instance):
    request.GET = request.GET.copy()
    request.GET['format'] = 'json'
    form = forms.VirusHostDetailViewForm(
        user=request.user,
        data=request.GET if request.method == 'GET' else None,
    )
    if not form.is_valid():
        return HttpResponseBadRequest(content=str(form.errors))
    request.GET['host' if is_host else 'virus'] = str(instance.pk)

    # get if the data will be shifted because of the infection ratio
    infection_ratio_shift = 1 if form.cleaned_data["infection_ratio"] else 0

    # Get the data
    response = views_api.CompleteResponseViewSet.as_view()(request)
    complete_responses = json.loads(response.rendered_content.decode('utf-8'))
    # det the data source, i.e the columns
    data_sources = mixins.only_public_or_granted_or_owned_queryset_filter(
        self=None,
        request=request,
        queryset=instance.data_source
    )
    # order them following col_order
    data_sources = order_queryset_specifically(
        queryset=data_sources,
        actual_order=dict([(int(o.strip()), i) for i, o in enumerate(
            form.cleaned_data["col_order"].split(","),
            start=infection_ratio_shift,
        ) if len(o)]),
    )
    # keep only pk and name in a dict
    data_sources = dict(data_sources.values_list("pk", "name"))

    col_header = list(data_sources.values())
    row_header = []
    data = []

    if infection_ratio_shift == 1:
        col_header.insert(0, _("Infection ratio"))
        row_header.insert(0, _("Infection ratio"))
        data.append([""] * len(col_header))

    # get at which position each col is
    col_pos = dict([(pk, i) for i, pk in enumerate(
        data_sources.keys(),
        start=infection_ratio_shift,
    )])
    # get at which position each row is following the value of row_order
    row_pos = dict([(int(o.strip()), i) for i, o in enumerate(
        form.cleaned_data["row_order"].split(","),
        start=infection_ratio_shift,
    ) if len(o)])

    row = None
    for virus_pk, hosts_responses in complete_responses.items():
        if is_host:
            # as the host/virus are not in the order of row_pos, we extends data up to the pos of the row we are
            # about to work with, same for the row_header
            row_id = row_pos.setdefault(int(virus_pk), infection_ratio_shift + len(row_pos))
            for i in range(len(row_header), row_id + 1):
                row = [""] * len(col_header)
                data.append(row)
                row_header.append("")
            # ... then we set at the correct position the header
            row_header[row_id] = models.Virus.objects.get(pk=virus_pk).explicit_name
            # ... and the new row
            row = data[row_id]
        for host_pk, ds_responses in hosts_responses.items():
            if not is_host:
                # see here before
                row_id = row_pos.setdefault(int(host_pk), infection_ratio_shift + len(row_pos))
                for i in range(len(row_header), row_id + 1):
                    row = [""] * len(col_header)
                    data.append(row)
                    row_header.append("")
                row_header[row_id] = models.Host.objects.get(pk=host_pk).explicit_name
                row = data[row_id]

            for ds_pk, response in ds_responses.items():
                row[col_pos[int(ds_pk)]] = response

    # Get the virus infection ratio if selected
    if infection_ratio_shift == 1:
        response = views_api.VirusInfectionRatioViewSet. \
            as_view(data_source_aggregated=False)(request=request, slug="host" if is_host else "virus")
        infection_ratio = json.loads(response.rendered_content.decode('utf-8'))

        virus_infection_ratio = infection_ratio["virus" if is_host else "host"]
        actual_total = max(v["total"] for v in virus_infection_ratio.values())
        for pk, value in virus_infection_ratio.items():
            pos = row_pos.setdefault(int(pk), infection_ratio_shift + len(row_pos))
            data[pos][0] = value['ratio'] * value['total'] * 100 / actual_total

        ds_infection_ratio = infection_ratio["data_source"]
        actual_total = max(v["total"] for v in ds_infection_ratio.values())
        for pk, value in ds_infection_ratio.items():
            data[0][col_pos[int(pk)]] = value['ratio'] * value['total'] * 100 / actual_total

    # Prepare the legend
    legend_name = []
    legend_value = []
    legend = []
    for m in models.GlobalViralHostResponseValue.objects_mappable().order_by('value'):
        legend_name.append([m.name])
        legend_value.append(m.value)
        legend.append([m.name, m.value])
    df_legend = pd.DataFrame(legend_name, columns=[str(_("Legend"))], index=legend_value)

    # Prepare the setting help of the infection ratio
    options = []
    if form.cleaned_data["weak_infection"]:
        options.append([form.fields["weak_infection"].label, ])
    df_options = pd.DataFrame(options, columns=[str(_("Infection ratio settings"))], index=None)

    # Prepare the title
    title = _("%(kind)s \"%(name)s\"") % dict(
        kind=_("Host") if is_host else _("Virus"),
        name=instance.explicit_name,
    )
    df_title = pd.DataFrame([], columns=[str(title)], index=None)

    with NamedTemporaryFile(suffix=".xlsx") as f:
        # write in a temp file
        with pd.ExcelWriter(f.name) as writer:
            df_title.to_excel(writer)

            df_data = pd.DataFrame(data, columns=col_header, index=row_header)
            df_data = df_data.style.applymap(func=panda_color_mapping)
            df_data.to_excel(writer, startcol=0, startrow=2)

            df_legend = df_legend.style.applymap(func=panda_legend_color_mapping)
            df_legend.to_excel(writer, startcol=0, startrow=len(row_header) + 3 + 2)
            if len(options) > 0:
                df_options.to_excel(writer, startcol=0, startrow=len(row_header) + 3 + len(legend_name) + 3 + 2,
                                    index=None)

        # then read the temp file and send it to the client
        with open(f.name, "rb") as excel:
            response = HttpResponse(excel.read(),
                                    content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            # Translators: it is the beginning of a file name
            raw_filename = _("ViralHostRange data of %(host_or_virus_name)s on %(date)s") % dict(
                host_or_virus_name=re.sub(r'[^\w\d-]', '_', instance.explicit_name),
                date=timezone.now().strftime('%Y%m%d %Hh%M.%S') + ".xlsx"
            )
            response['Content-Disposition'] = 'attachment; filename="%s"' % raw_filename
            return response


@method_decorator(login_required, name='dispatch')
class DataSourceWizard(wizard_views.NamedUrlSessionWizardView):
    def get_upload_or_live_input(self):
        data = self.storage.get_step_data("UploadOrLiveInput")
        return data is not None and data.get('UploadOrLiveInput-upload_or_live_input', None)

    def show_visibility(self):
        data = self.storage.get_step_data("Information")
        return data is not None and not data.get('Information-public', False)

    def live_input(self):
        return self.get_upload_or_live_input() == 'live'

    def upload_data(self):
        return self.get_upload_or_live_input() == 'upload'

    def live_template_data(self):
        return self.get_upload_or_live_input() == 'live'

    instance = None
    form_list = [
        ("Intro", forms.EmptyForm),
        ("Information", forms.DataSourceNameModelForm),
        ("Visibility", forms.DataSourceVisibilityModelForm),
        ("Description", forms.DataSourceDescriptionModelForm),
        ("UploadOrLiveInput", forms.UploadOrLiveInput),
        ("Upload", forms.UploadDataSourceForm),
        ("LiveInput", forms.LiveInputVirusHostForm),
        ("UploadTemplate", forms.UploadDataSourceForm),
    ]
    condition_dict = {
        "Visibility": show_visibility,
        "LiveInput": live_input,
        "UploadTemplate": live_template_data,
        "Upload": upload_data,
    }
    template_name = 'viralhostrangedb/wizard_form.html'
    file_storage = storage.FileSystemStorage(location=os.path.join(tempfile.gettempdir(), 'viralhostrangedb_wizard'))

    def dispatch(self, request, *args, **kwargs):
        try:
            return super().dispatch(request, *args, **kwargs)
        except KeyError:
            self.storage.reset()
            return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, form, step=None, **kwargs):
        context = super().get_context_data(form=form, step=step, **kwargs)
        context["submit_text"] = _("Next")
        if step is None:
            step = self.steps.current
        if step == "Intro":
            context["title"] = _("Contribution")
        elif step == "Information":
            context["title"] = _("DataSourceWizard.title.Information")
        elif step == "Visibility":
            context["title"] = _("DataSourceWizard.title.Visibility")
        elif step == "Description":
            context["title"] = _("DataSourceWizard.title.Description")
        elif step == "UploadOrLiveInput":
            context["title"] = _("DataSourceWizard.title.UploadOrLiveInput")
        elif step == "Upload":
            context["title"] = _("DataSourceWizard.title.Upload")
        elif step == "LiveInput":
            context["title"] = _("DataSourceWizard.title.LiveInput")
        elif step == "UploadTemplate":
            context["title"] = _("DataSourceWizard.title.UploadTemplate")
        if step == "Upload" or step == "UploadTemplate":
            context["submit_text"] = _("Save")
        elif step == "UploadOrLiveInput":
            context["submit_text"] = _("Proceed")
        elif step == "Intro":
            context["submit_text"] = _("Begin your contribution")
        elif step == "Information":
            context["hide_step_button"] = True
        context["custom_css_width"] = "col-lg-10 offset-lg-1 "

        if step == "Intro":
            # Translators :scheme the the list of responses formated as a text
            context["step_message"] = mark_safe(
                ugettext("step_message_Intro_subtitle") +
                self.get_decorated_message_with_table_example(
                    "<div>" + ugettext("step_message_Intro%(scheme)s") % dict(
                        scheme=self.compute_scheme(),
                    ) + "</div>"
                )
            )
            context["custom_css_width"] = "col-12"
        elif step == "Information":
            context["step_message"] = _("step_message_Information")
        elif step == "Description":
            context["custom_css_width"] = "col-lg-10 offset-lg-1 "
        elif step == "Visibility":
            context["step_message"] = mark_safe(_("step_message_Visibility"))
        elif step == "UploadOrLiveInput":
            context["step_message"] = mark_safe(_("step_message_UploadOrLiveInput"))
        elif step == "Upload":
            context["step_message"] = self.get_decorated_message_with_table_example(
                ugettext("step_message_Upload%(scheme)s") % dict(scheme=self.compute_scheme())
            )
        elif step == "LiveInput":
            if form.is_bound:
                context["step_message"] = mark_safe(_("step_message_LiveInput when is_bound"))
            else:
                # Translators : when data is provided i.e: when we were not able to guess what is the separator,
                # so give more details
                context["step_message"] = mark_safe(_("step_message_LiveInput when not is_bound"))
            context["custom_css_width"] = "col-lg-10 offset-lg-1 "
        elif step == "UploadTemplate":
            f = self.get_form("LiveInput", self.storage.get_step_data("LiveInput"))
            f.is_valid()
            templates = f.get_template_files()
            context["step_message"] = self.get_decorated_message_with_table_example(
                ugettext("step_message_UploadTemplate%(scheme)s%(template_file_url)s") % dict(
                    scheme=self.compute_scheme(),
                    template_file_url=templates["file_url"],
                ),
                table=self.get_table_for_file(
                    file_path=templates["sample_path"],
                    file_url=templates["file_url"],
                    legend=ugettext("Fig 1: A (subset of) your template<br/>"),
                    download_message=ugettext('download complete template'),
                    use_color=False,
                ),
            )
        if context.get("step_message", None) == "_":
            del context["step_message"]
        return context

    @staticmethod
    def compute_scheme():
        scheme = ""
        mapping = list(models.GlobalViralHostResponseValue.objects_mappable().order_by("value"))
        for pos in range(len(mapping)):
            if pos > 0:
                if pos == len(mapping) - 1:
                    scheme += " " + ugettext("and") + " "
                else:
                    scheme += ", "
            scheme += '%g ( <i class="fa fa-stop" style="color:%s"></i> %s)' % (
                mapping[pos].value,
                mapping[pos].color,
                mapping[pos].name,
            )
        return scheme

    @staticmethod
    def get_table_for_file(file_path, file_url=None, legend="", download_message=None, use_color=True):
        table = None
        vhrs_dict = business_process.to_dict(
            business_process.parse_file(file_path),
            transposed=True,
        )
        hosts = []
        for v, h_r in vhrs_dict.items():
            for h, r in h_r.items():
                if h not in hosts:
                    hosts.append(h)
        for v, h_r in vhrs_dict.items():
            if table is None:
                table = ['<div class="text-center"><table class="table table-sm text-center', ]
                if use_color:
                    table += " use-global-scheme"
                else:
                    table += " table-bordered"
                table += "\">"
                table += "<tr><td></<td>"
                for h in hosts:
                    table += '<th>'
                    table += h.replace(" ", "&nbsp;").replace("&nbsp;(", " (")
                    table += '</th>'
                table += "</tr>"
            table += "<tr><th>"
            table += v.replace(" ", "&nbsp;").replace("&nbsp;(", " (")
            table += "</th>"
            for h in hosts:
                r = h_r.get(h, "")
                table += '<td class="schema-'
                table += escape(r)
                table += '">'
                table += escape(r)
                table += '</td>'
            table += "</tr>"
        table += "</table>"
        table += '<span>'
        table += legend
        table += '<a href="'
        table += file_url if file_url else static("/" + file_path)
        table += '"><i class="fa fa-download"></i> '
        table += download_message if download_message else ugettext('download associated file')
        table += '</a></span></div>'
        return table

    @staticmethod
    def get_decorated_message_with_table_example(msg, table=None):
        if not table:
            table = DataSourceWizard.get_table_for_file(
                file_path=os.path.join(settings.STATIC_ROOT, 'media/example.xlsx'),
            )
        return mark_safe('<div style="display:flex"><div class=\"mr-4\">' + msg + "</div>" + "".join(table) + '</div>')

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs(step=step)
        if step == "Information":
            kwargs.update({"owner": self.request.user})
        elif step == "Description":
            kwargs.update({"required": self.storage.get_step_data("Information").get('Information-public', False)})
            if self.storage.get_step_data("Information").get('Information-public', False):
                kwargs["msg"] = _("step_message_Description_data_source_public%s")
            else:
                kwargs["msg"] = _("step_message_Description_data_source_private%s")
            kwargs["msg"] = kwargs["msg"] % self.storage.get_step_data("Information").get('Information-name', False)
        return kwargs

    def get_form_instance(self, step):
        if self.instance is None:
            self.instance = models.DataSource()
        # if step in ["Information", "Visibility", "Upload"]:
        return self.instance
        # return super().get_form_instance(step)

    def done(self, form_list, form_dict=None, **kwargs):
        with transaction.atomic():
            if "Visibility" in form_dict:
                form_dict["Information"].save(commit=False)
                form_dict["Visibility"].save()
            else:
                form_dict["Information"].save()
            if self.upload_data():
                key = "Upload"
            else:  # if self.live_template_data():
                key = "UploadTemplate"
            form_dict[key].save(importation_observer=MessageImportationObserver(request=self.request))

            url = reverse("viralhostrangedb:data-source-mapping-edit", args=[self.instance.pk])
            messages.success(
                request=self.request,
                message=mark_safe(_("success_import%(url_data_source_name)s%(data_source_name)s") % dict(
                    data_source_name=self.instance.name,
                    url_data_source_name=self.instance.get_absolute_url(),
                )),
            )
            return HttpResponseRedirect(url)


def response_update(request, ds_pk, virus_pk, host_pk):
    data_source = get_object_or_404(mixins.only_owned_queryset_filter(
        self=None,
        request=request,
        queryset=models.DataSource.objects,
    ), pk=ds_pk)
    form = forms.ResponseUpdateForm(
        ds_pk=data_source.pk,
        virus_pk=virus_pk,
        host_pk=host_pk,
        data=request.POST if request.method == 'POST' else None,
    )
    if form.is_valid():
        response = form.save()
        if response.data_source.is_mapping_done:
            return redirect(
                "%(url)s?host=%(hosts)s&virus=%(viruses)s&ds=%(data_source)s" % dict(
                    url=reverse("viralhostrangedb:browse"),
                    viruses=virus_pk,
                    hosts=host_pk,
                    data_source=ds_pk,
                ),
                permanent=False,
            )
        return redirect(
            reverse("viralhostrangedb:data-source-mapping-edit", args=[ds_pk]),
            permanent=False,
        )

    context = dict(
        title=_("response_update__title"),
        # Translators: Also remind the user that they edit the raw response, not the mapped response, if they change
        # the raw_response to a value that is already map we will keep the mapping while if it is a completely new
        # value, they will be redirected to edit the mapping
        full_width_form_title=mark_safe(_("response_update__form_title%(ds_name)s"
                                          "%(virus_explicit_name_html)s%(host_explicit_name_html)s") % dict(
            ds_name=data_source.name,
            ds_author=data_source.owner.username,
            virus_explicit_name_html=models.Virus.objects.get(pk=virus_pk).explicit_name_html,
            host_explicit_name_html=models.Host.objects.get(pk=host_pk).explicit_name_html,
        )),
        form=form,
        medium_width=True,
        virus=models.Virus.objects.get(pk=virus_pk),
        host=models.Host.objects.get(pk=host_pk),
        ds=data_source,
    )
    return render(
        request=request,
        template_name='viralhostrangedb/response_update.html',
        context=context,
    )


@login_required
def contact_data_source_owner(request, pk):
    data_source = get_object_or_404(models.DataSource, pk=pk)
    if request.method == 'POST':
        form = forms.ContactOwnerForm(recipient=data_source.owner, data_source=data_source, data=request.POST)
        if form.is_valid():
            subject = _("contact_data_source_owner__subject%s") % escape(form.cleaned_data["subject"])
            body = escape(form.cleaned_data["body"]).split("\n")
            body = "<p>" + ("</p><p>".join(body)) + "</p>"
            body = _(
                "contact_data_source_owner__body"
                "%(body)s"
                "%(owner_first_name)s"
                "%(owner_last_name)s"
                "%(contacting_person_first_name)s"
                "%(contacting_person_last_name)s"
                "%(contacting_person_email)s"
                "%(data_source_name)s"
                "%(data_source_link)s"
            ) % dict(
                body=body,
                owner_first_name=escape(data_source.owner.first_name),
                owner_last_name=escape(data_source.owner.first_name),
                contacting_person_first_name=escape(request.user.first_name),
                contacting_person_last_name=escape(request.user.last_name),
                contacting_person_email=request.user.email,
                data_source_name=escape(str(data_source)),
                data_source_link=request.scheme + "://" + request.get_host() + data_source.get_absolute_url(),
            )
            for to in [data_source.owner.email, request.user.email]:
                email = EmailMultiAlternatives(
                    subject=subject,
                    body=strip_tags(body),
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=[
                        to,
                    ],
                    headers={'Reply-To': request.user.email},

                )
                email.attach_alternative(body, "text/html")
                email.send()
            return render(request, 'basetheme_bootstrap/simple_message_page.html', context=dict(
                title=_("contact_data_source_owner Email sent"),
                page_title=_("contact_data_source_owner Email sent"),
                sub_message=mark_safe(
                    _("contact_data_source_owner Email sent explanation%(back_url)s") % dict(
                        back_url=data_source.get_absolute_url(),
                    )
                ),
            ))
    else:
        form = forms.ContactOwnerForm(recipient=data_source.owner, data_source=data_source)
    context = dict(
        title=_("Contacting the owner of a data source"),
        form=form,
        medium_width=True,
        custom_css_width="col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3",
        # Translators: remind them that the email is sent on their behalf, but they can only get the owner email if
        # he/she answer.
        submit_text=_("Send"),
        full_width_form_title=_("contact_data_source_owner explanation"),
        full_width_form_title_classes="mb-4 col-md-10 offset-md-1 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3 "
                                      "text-justify",
    )
    return render(
        request=request,
        template_name='basetheme_bootstrap/small_form_host.html',
        context=context,
    )


def search(request):
    searched_text = request.POST.get("search", request.GET.get("search", ""))

    kind = request.POST.get("kind", request.GET.get("kind", "all"))
    initial = {}
    initial.update(request.POST)
    initial.update(request.GET)
    initial.update(
        dict(
            search=searched_text,
            sample_size=request.POST.get("sample_size", request.GET.get("sample_size", "0")),
            ui_help=True,
            kind=kind,
        )
    )
    form = forms.SearchForm(
        initial=initial,
        user=request.user,
    )
    form.is_valid()

    context = dict(
        form=form,
        term=searched_text,
        search_bar_value=searched_text,
    )

    return render(
        request=request,
        template_name='viralhostrangedb/search.html',
        context=context,
    )


def is_ready(request):
    from django.db import OperationalError
    import django.apps
    import django.apps
    try:
        for model_class in django.apps.apps.get_models():
            model_class.objects.first()
        return HttpResponse(status=200)
    except OperationalError:
        pass
    return HttpResponse(status=500)


def is_alive(request):
    from django.db import OperationalError
    try:
        get_user_model().objects.exists()
        return HttpResponse(status=200)
    except OperationalError:
        pass
    return HttpResponse(status=500)
