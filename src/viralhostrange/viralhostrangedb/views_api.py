from functools import reduce
from typing import List
from urllib.parse import urlencode

from django.db.models import Avg, Q, Count, Func, When, IntegerField, Case, Value, Max, Min
from django.http import JsonResponse, HttpResponseBadRequest
from django.urls import reverse
from rest_framework import views as drf_views, viewsets
from rest_framework.filters import BaseFilterBackend
from rest_framework.response import Response

from viralhostrangedb import models, mixins, serializers, forms


class MyAPIView(drf_views.APIView):
    queryset = None
    filter_backends = []

    def get_queryset(self):
        queryset = self.queryset
        return queryset

    def filter_queryset(self, queryset):
        """
        Given a queryset, filter it with whichever filter backend is in use.
        """
        for backend in list(self.filter_backends):
            queryset = backend().filter_queryset(self.request, queryset, self)
        return queryset

    def get(self, request, *args, **kwargs):
        raise NotImplementedError()


def to_int_array(input_list: List):
    for a in input_list:
        try:
            yield int(a)
        except ValueError:
            pass


class FromGetParamsFilterResponseBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        nop = 1 if "allow_overflow" in request.GET else 0
        if "ds" in request.GET:
            getlist = reduce(lambda x, y: x + y, [l.split(',') for l in request.GET.getlist("ds")])
            queryset = queryset.filter(data_source__pk__in=to_int_array(getlist))
            nop += 1
        if "host" in request.GET:
            getlist = reduce(lambda x, y: x + y, [l.split(',') for l in request.GET.getlist("host")])
            queryset = queryset.filter(host__pk__in=to_int_array(getlist))
            nop += 1
        if "virus" in request.GET:
            getlist = reduce(lambda x, y: x + y, [l.split(',') for l in request.GET.getlist("virus")])
            queryset = queryset.filter(virus__pk__in=to_int_array(getlist))
            nop += 1
        if "only_published_data" in request.GET:
            queryset = queryset.filter(~Q(data_source__publication_url=None))
        if "only_virus_ncbi_id" in request.GET:
            queryset = queryset.filter(~Q(virus__identifier=""))
        if "only_host_ncbi_id" in request.GET:
            queryset = queryset.filter(~Q(host__identifier=""))
        if "life_domain" in request.GET and len(request.GET["life_domain"]) > 0:
            queryset = queryset.filter(Q(data_source__life_domain=request.GET["life_domain"]))
        if nop == 0:
            return queryset.none()
        return queryset


class FromGetParamsFilterVirusHostBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "ds" in request.GET:
            getlist = request.GET.getlist("ds")
            if len(getlist) == 1:
                getlist = getlist[0].split(',')
            getlist = [x for x in getlist if len(x) > 0]
            if len(getlist) > 0:
                queryset = queryset.filter(data_source__pk__in=getlist)
        if "owner" in request.GET:
            getlist = request.GET.getlist("owner")
            if len(getlist) == 1:
                getlist = getlist[0].split(',')
            getlist = [x for x in getlist if len(x) > 0]
            if len(getlist) > 0:
                queryset = queryset.filter(data_source__owner__pk__in=getlist)
        if "only_published_data" in request.GET:
            queryset = queryset.filter(data_source__in=models.DataSource.objects.filter(~Q(publication_url=None)))
        if "only_virus_ncbi_id" in request.GET and queryset.model == models.Virus:
            queryset = queryset.filter(~Q(identifier=""))
        if "only_host_ncbi_id" in request.GET and queryset.model == models.Host:
            queryset = queryset.filter(~Q(identifier=""))
        if "life_domain" in request.GET and len(request.GET["life_domain"]) > 0:
            queryset = queryset.filter(Q(data_source__life_domain=request.GET["life_domain"]))
        return queryset

    pass


class FromGetParamsFilterDataSourceBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "owner" in request.GET:
            getlist = request.GET.getlist("owner")
            if len(getlist) == 1:
                getlist = getlist[0].split(',')
            getlist = [x for x in getlist if len(x) > 0]
            if len(getlist) > 0:
                queryset = queryset.filter(owner__pk__in=getlist)
        if "only_published_data" in request.GET:
            queryset = queryset.filter(~Q(publication_url=None))
        if "life_domain" in request.GET and len(request.GET["life_domain"]) > 0:
            queryset = queryset.filter(Q(life_domain=request.GET["life_domain"]))
        if "only_virus_ncbi_id" in request.GET:
            queryset = queryset.filter(virus__identifier__regex=r'.+')
        if "only_host_ncbi_id" in request.GET:
            queryset = queryset.filter(host__identifier__regex=r'.+')
        return queryset


class OnlyPublicOrOwnedFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return mixins.only_public_or_granted_or_owned_queryset_filter(
            self,
            request=request,
            queryset=queryset,
            path_to_data_source="data_source__",
        )


class OnlyMappedResponseFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(~Q(response__pk=models.GlobalViralHostResponseValue.get_not_mapped_yet_pk()))


class Round(Func):
    function = 'ROUND'
    arity = 2
    arg_joiner = '::numeric, '

    def as_sqlite(self, compiler, connection, **extra_context):
        return super().as_sqlite(compiler, connection, arg_joiner=", ", **extra_context)


class AggregatedResponseViewSet(MyAPIView):
    queryset = models.ViralHostResponseValueInDataSource.objects
    filter_backends = [OnlyMappedResponseFilterBackend, FromGetParamsFilterResponseBackend,
                       OnlyPublicOrOwnedFilterBackend]

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.values("virus__pk", "host__pk") \
            .annotate(val=Round(Avg('response__value'), 2)) \
            .annotate(diff=Count('response__value', distinct=True)) \
            .order_by('virus__pk')

        table = {}
        hosts = None
        last_virus = None
        for entry in queryset:
            virus_pk = entry['virus__pk']
            if last_virus != virus_pk:
                hosts = {}
                last_virus = virus_pk
                table[virus_pk] = hosts
            hosts[entry['host__pk']] = dict(val=entry['val'], diff=entry['diff'])

        return Response(table)


class CompleteResponseViewSet(MyAPIView):
    queryset = models.ViralHostResponseValueInDataSource.objects
    filter_backends = [OnlyMappedResponseFilterBackend, FromGetParamsFilterResponseBackend,
                       OnlyPublicOrOwnedFilterBackend]

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.values("virus__pk", "host__pk", "data_source__pk", "response__value") \
            .order_by('virus__pk', 'host__pk', 'data_source__pk', )

        table = {}
        hosts = None
        virus = None
        response = None
        last_virus = None
        last_host = None
        for entry in queryset:
            virus_pk = entry['virus__pk']
            if last_virus != virus_pk:
                hosts = {}
                last_virus = virus_pk
                table[virus_pk] = hosts
                last_host = None
            host_pk = entry['host__pk']
            if last_host != host_pk:
                response = {}
                last_host = host_pk
                hosts[host_pk] = response
            response[entry['data_source__pk']] = entry['response__value'];

        return Response(table)


class PKFromGetParamsFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "pks" in request.GET:
            queryset = queryset.filter(pk__in=request.GET["pks"].split(','))
        if "pk" in request.GET:
            queryset = queryset.filter(pk__in=request.GET["pk"].split(','))
        if "ids" in request.GET:
            queryset = queryset.filter(pk__in=request.GET["ids"].split(','))
        if "id" in request.GET:
            queryset = queryset.filter(pk__in=request.GET["id"].split(','))
        return queryset


class VirusViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Virus.objects
    serializer_class = serializers.VirusSerializer
    filter_backends = [
        OnlyPublicOrOwnedFilterBackend,
        PKFromGetParamsFilterBackend,
        FromGetParamsFilterVirusHostBackend,
    ]


class HostViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Host.objects
    serializer_class = serializers.HostSerializer
    filter_backends = [
        OnlyPublicOrOwnedFilterBackend,
        PKFromGetParamsFilterBackend,
        FromGetParamsFilterVirusHostBackend,
    ]


class OnlyPublicOrGrantedOrOwnedDataSourceFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return mixins.only_public_or_granted_or_owned_queryset_filter(
            self,
            request=request,
            queryset=queryset,
        )


class DataSourceViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.DataSource.objects
    serializer_class = serializers.DataSourceSerializer
    filter_backends = [
        OnlyPublicOrGrantedOrOwnedDataSourceFilterBackend,
        PKFromGetParamsFilterBackend,
        FromGetParamsFilterDataSourceBackend,
    ]


class VirusInfectionRatioViewSet(MyAPIView):
    data_source_aggregated = True
    queryset = models.ViralHostResponseValueInDataSource.objects
    filter_backends = [OnlyMappedResponseFilterBackend, FromGetParamsFilterResponseBackend,
                       OnlyPublicOrOwnedFilterBackend]

    def get(self, request, slug=None, slug_pk=None, *args, **kwargs):
        queryset = self.get_queryset()

        other_slug = None
        if slug == 'virus':
            other_slug = 'host'
        elif slug == 'host':
            other_slug = 'virus'
        else:
            return HttpResponseBadRequest("unknown slug '%s'" % slug)

        # filter only for the current slug
        if slug_pk != None:
            if slug == "virus":
                queryset = queryset.filter(virus__pk=int(str(slug_pk)))
            elif slug == "host":
                queryset = queryset.filter(host__pk=int(str(slug_pk)))
            # As the FromGetParamsFilterResponseBackend prevent to return too much data when no GET parameter is provided we
            # explicitly indicate that even if there is no GET parameter, we should not return an empty queryset.
            request.GET = request.GET.copy()
            request.GET.update({"allow_overflow": True})

        # filter with_backend
        queryset = self.filter_queryset(queryset)

        # picking the minimum global response value to consider that there is an infection
        if "weak_infection" in request.GET:
            # an infection is everything except the lower (i.e: No Lysis)
            min_level = models.GlobalViralHostResponseValue.objects_mappable().aggregate(v=Min("value"))['v']
            min_level = models.GlobalViralHostResponseValue.objects_mappable().filter(value__gt=min_level) \
                .aggregate(v=Min("value"))['v']
        else:
            # an infection is only the higher (i.e: Lysis)
            min_level = models.GlobalViralHostResponseValue.objects_mappable().aggregate(v=Max("value"))['v']

        # annotating the query such as infection is now 0/1
        queryset = queryset.annotate(infection=Case(
            When(response__value__gte=min_level, then=1),
            default=Value(0),
            output_field=IntegerField(),
        ))

        # determine how the response should be aggregated, do we enforce consensus or not
        if "agreed_infection" in request.GET:
            infection_aggregated = Min('infection')
        else:
            infection_aggregated = Max('infection')

        # aggregate per host/virus the infections
        if self.data_source_aggregated:
            queryset = queryset.values('host__pk', 'virus__pk')
        else:
            queryset = queryset.values('host__pk', 'virus__pk', 'data_source__pk')
        queryset = queryset.annotate(infection_aggregated=infection_aggregated)

        # Store infection in a dict where key in the pk, and value the count of infection and no infection
        infection_aggregated_counters = {}
        infection_aggregated_counters_on_data_source = None if self.data_source_aggregated else {}

        if self.data_source_aggregated:
            # we will get either virus__pk or host__pk, so suffixing the slug
            aggregation_key = slug + "__pk"
        else:
            # we will get the aggregation over the other slug: for a host, we get for each virus.
            aggregation_key = other_slug + "__pk"

        # run the query and fetch results
        for o in queryset:
            # infection is either yes(1) or no(0)
            infection_aggregated_counter = infection_aggregated_counters.setdefault(o[aggregation_key], [0, 0])
            # count infection or not an infection
            infection_aggregated_counter[o['infection_aggregated']] += 1
            if infection_aggregated_counters_on_data_source is not None:
                infection_aggregated_counter = infection_aggregated_counters_on_data_source \
                    .setdefault(o['data_source__pk'], [0, 0])
                # count infection or not an infection
                infection_aggregated_counter[o['infection_aggregated']] += 1

        # build the final ratio
        self.aggregate_infection_counters(infection_aggregated_counters)

        if self.data_source_aggregated:
            # return the ratios as a json
            return Response(infection_aggregated_counters)

        self.aggregate_infection_counters(infection_aggregated_counters_on_data_source)

        return Response({
            other_slug: infection_aggregated_counters,
            "data_source": infection_aggregated_counters_on_data_source
        })

    def aggregate_infection_counters(self, infection_aggregated_counters):
        for k, infection_aggregated_counter in infection_aggregated_counters.items():
            s = sum(infection_aggregated_counter)
            # compute the ratio as it is what we want, keep the total in case third part service need it
            infection_aggregated_counters[k] = dict(
                ratio=infection_aggregated_counter[1] / s,
                total=s,
            )


def search(request):
    form = forms.SearchForm(data=request.POST or request.GET, user=request.user)
    if not form.is_valid():
        return JsonResponse(dict(err="Invalid query", detail=form.errors))

    searched_text = form.cleaned_data["search"]
    sample_size = form.cleaned_data["sample_size"]
    kind = form.cleaned_data["kind"] or 'all'
    search_key = "__icontains"
    ui_help = form.cleaned_data["ui_help"]

    if sample_size == 0:
        sample_size = 7 if kind == "all" else 15

    q_in_vh = Q(Q(**{"name" + search_key: searched_text}) | Q(**{"identifier" + search_key: searched_text}))

    qs_virus = models.Virus.objects.filter(q_in_vh)
    qs_virus = FromGetParamsFilterVirusHostBackend().filter_queryset(request, qs_virus, None)
    qs_virus = mixins.only_public_or_granted_or_owned_queryset_filter(
        None,
        request=request,
        queryset=qs_virus,
        path_to_data_source="data_source__",
    )

    qs_host = models.Host.objects.filter(q_in_vh)
    qs_host = FromGetParamsFilterVirusHostBackend().filter_queryset(request, qs_host, None)
    qs_host = mixins.only_public_or_granted_or_owned_queryset_filter(
        None,
        request=request,
        queryset=qs_host,
        path_to_data_source="data_source__",
    )

    qs_data_source = models.DataSource.objects.filter(Q(
        Q(**{"name" + search_key: searched_text})
        | Q(**{"description" + search_key: searched_text})
        | Q(**{"owner__first_name" + search_key: searched_text})
        | Q(**{"owner__last_name" + search_key: searched_text})
    ))
    qs_data_source = FromGetParamsFilterDataSourceBackend().filter_queryset(request, qs_data_source, None)
    qs_data_source = mixins.only_public_or_granted_or_owned_queryset_filter(
        None,
        request=request,
        queryset=qs_data_source,
        path_to_data_source="",
    )

    responses = dict(
        query=dict(
            searched_text=searched_text,
            sample_size=sample_size,
            kind=kind,
        )
    )
    if ui_help:
        responses["ui_help"] = dict()

    get_param = dict(form.cleaned_data)
    get_param["owner"] = list(get_param["owner"].values_list("pk", flat=True))
    get_param["sample_size"] = -1
    get_param.pop("ui_help")
    get_param.pop("kind")
    for k, v in list(get_param.items()):
        if not v:
            get_param.pop(k)
    get_param = urlencode(get_param, doseq=True)

    if kind in ['virus', 'all']:
        if ui_help:
            responses["ui_help"]["virus"] = models.Virus._meta.verbose_name.title()
        responses["virus"] = dict(
            count=qs_virus.count(),
            sample=serializers.VirusSerializerWithURL(
                qs_virus[0:sample_size] if sample_size > 0 else qs_virus, many=True).data,
            subset_url="%s?kind=virus&%s" % (reverse("viralhostrangedb:search"), get_param)
        )
    if kind in ['host', 'all']:
        if ui_help:
            responses["ui_help"]["host"] = models.Host._meta.verbose_name.title()
        responses["host"] = dict(
            count=qs_host.count(),
            sample=serializers.HostSerializerWithURL(
                qs_host[0:sample_size] if sample_size > 0 else qs_host, many=True).data,
            subset_url="%s?kind=host&%s" % (reverse("viralhostrangedb:search"), get_param)
        )
    if kind in ['data_source', 'all']:
        if ui_help:
            responses["ui_help"]["data_source"] = models.DataSource._meta.verbose_name.title()
        responses["data_source"] = dict(
            count=qs_data_source.count(),
            sample=serializers.DataSourceSerializerForSearch(
                qs_data_source[0:sample_size] if sample_size > 0 else qs_data_source, many=True).data,
            subset_url="%s?kind=data_source&%s" % (reverse("viralhostrangedb:search"), get_param)
        )
    return JsonResponse(responses)
